# Backend

Para levantar el proyecto necesitan instalar:

- Java 8
- Eclipse (supongo que intellij funciona igual)
- MySQL 8

Despues tienen que hacer esto:

1. En MySQL crear una base de datos con el nombre glucome
1.  Abrir el eclipse e instalar el Spring boot tools (Help, install new software y en work with pegan esta URL(https://download.springsource.com/release/TOOLS/sts4/update/e4.9/) yo instale todo lo que me tiro ahi.
1. Importar el proyecto en Eclipse (Importar un proyecto Maven existente)
1.  Hacer click derecho en el proyecto, ir a Maven, Select Maven profiles y seleccionar el corresponda con su nombre
1. Abrir el archivo application-nombre.properties (src/main/resources) y modificar la clave del root para que hibernate pueda usar la bd
1. Despues tiene que abrir la clase "GlucomeApplication" (src/main/java/com.glucome) y poner "Run as Spring Boot App"


Configuracion para inicialización de Base de datos

1. En su mysql.ini (C:/ProgramData/MySQL/MySQL Server.../my.ini) e cambiar la linea que sea 'secure-file-priv=(ruta random)' a 'secure-file-priv=""' o sea vaciar eso y grabar
1. Correr en su mysql "SET GLOBAL local_infile='ON';"
1. Reiniciar el servicio MySQL