package com.glucome.api;

public class RespuestaApi {

    private String mensaje;

    private Response respuesta;

    public RespuestaApi(Response respuesta, String mensaje) {
        this.respuesta = respuesta;
        this.mensaje = mensaje;
    }

    public RespuestaApi(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Response getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Response respuesta) {
        this.respuesta = respuesta;
    }

}
