package com.glucome.api.request;

public class AlimentoIngeridoRequest {

	// Formato dd/MM/yyyy HH:mm
	private String fechaHora;

	private double porciones;

	private long alimento;

	private String observacion;

	private int momento;

	public AlimentoIngeridoRequest() {
	}

	public String getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}

	public long getAlimento() {
		return alimento;
	}

	public void setAlimento(long alimento) {
		this.alimento = alimento;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public AlimentoIngeridoRequest(String fechaHora, double porciones, long alimento, String observacion, int momento) {
		super();
		this.fechaHora = fechaHora;
		this.porciones = porciones;
		this.alimento = alimento;
		this.observacion = observacion;
		this.momento = momento;
	}

	public int getMomento() {
		return momento;
	}

	public void setMomento(int momento) {
		this.momento = momento;
	}

	public double getPorciones() {
		return porciones;
	}

	public void setPorciones(double porciones) {
		this.porciones = porciones;
	}

}
