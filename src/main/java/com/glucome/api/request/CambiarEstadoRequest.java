package com.glucome.api.request;

public class CambiarEstadoRequest {

    private Long idUsuario;
    private boolean online;

    public CambiarEstadoRequest() {
    }

    public CambiarEstadoRequest(Long idUsuario, boolean online) {
        this.idUsuario = idUsuario;
        this.online = online;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}
