package com.glucome.api.request;

public class ChatMedicoPacienteRequest {

    Long idUsuarioPaciente;
    Long idUsuarioMedico;

    public ChatMedicoPacienteRequest() { }

    public ChatMedicoPacienteRequest(Long idUsuarioPaciente, Long idUsuarioMedico) {
        this.idUsuarioPaciente = idUsuarioPaciente;
        this.idUsuarioMedico = idUsuarioMedico;
    }

    public Long getIdUsuarioPaciente() { return idUsuarioPaciente; }

    public void setIdUsuarioPaciente(Long idUsuarioPaciente) { this.idUsuarioPaciente = idUsuarioPaciente; }

    public Long getIdUsuarioMedico() { return idUsuarioMedico; }

    public void setIdUsuarioMedico(Long idUsuarioMedico) { this.idUsuarioMedico = idUsuarioMedico;  }
}
