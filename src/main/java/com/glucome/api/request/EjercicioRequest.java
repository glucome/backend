package com.glucome.api.request;

public class EjercicioRequest {

	// Formato dd/MM/yyyy HH:mm
	private String fechaHora;

	private double minutos;

	private int tipo;

	private String observacion;

	public EjercicioRequest() {
	}

	public EjercicioRequest(String fechaHora, double minutos, int tipo, String observacion) {
		super();
		this.fechaHora = fechaHora;
		this.minutos = minutos;
		this.tipo = tipo;
		this.observacion = observacion;
	}

	public String getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}

	public double getMinutos() {
		return minutos;
	}

	public void getMinutos(double minutos) {
		this.minutos = minutos;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public void setMinutos(double minutos) {
		this.minutos = minutos;
	}

}
