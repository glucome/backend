package com.glucome.api.request;

public class FactorCorreccionInsulinaRequest {

	private double factorCorreccionAzucar;
	
	private double factorCorreccionCHO;

	public double getFactorCorreccionAzucar() {
		return factorCorreccionAzucar;
	}

	public void setFactorCorreccionAzucar(double factorCorreccionAzucar) {
		this.factorCorreccionAzucar = factorCorreccionAzucar;
	}

	public double getFactorCorreccionCHO() {
		return factorCorreccionCHO;
	}

	public void setFactorCorreccionCHO(double factorCorreccionCHO) {
		this.factorCorreccionCHO = factorCorreccionCHO;
	}

	public FactorCorreccionInsulinaRequest() {
		super();
	}

	public FactorCorreccionInsulinaRequest(double factorCorreccionAzucar, double factorCorreccionCHO) {
		super();
		this.factorCorreccionAzucar = factorCorreccionAzucar;
		this.factorCorreccionCHO = factorCorreccionCHO;
	}
	
	
}
