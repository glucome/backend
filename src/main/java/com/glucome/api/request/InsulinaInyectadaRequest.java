package com.glucome.api.request;

public class InsulinaInyectadaRequest {

    // Formato dd/MM/yyyy HH:mm
    private String fechaHora;

    private double unidades;

    private int tipo;

    public InsulinaInyectadaRequest() {
    }

    public InsulinaInyectadaRequest(String fechaHora, double unidades, int tipo) {
        super();
        this.fechaHora = fechaHora;
        this.unidades = unidades;
        this.tipo = tipo;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public double getUnidades() {
        return unidades;
    }

    public void setUnidades(double unidades) {
        this.unidades = unidades;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

}
