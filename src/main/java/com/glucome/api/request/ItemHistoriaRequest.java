package com.glucome.api.request;

public class ItemHistoriaRequest {

	private long id;

	private int medico;

	private String fecha;

	private String descripcion;

	public int getMedico() {
		return medico;
	}

	public void setMedico(int medico) {
		this.medico = medico;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public ItemHistoriaRequest(int id, int medico, String fecha, String descripcion) {
		super();
		this.id = id;
		this.medico = medico;
		this.fecha = fecha;
		this.descripcion = descripcion;
	}

	public ItemHistoriaRequest() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
