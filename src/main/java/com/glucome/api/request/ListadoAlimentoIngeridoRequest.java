package com.glucome.api.request;

import com.glucome.utils.Constantes;

public class ListadoAlimentoIngeridoRequest {

	private String fechaHoraDesde;

	private String fechaHoraHasta;

	private Double porcionesDesde;

	private Double porcionesHasta;

	private Integer momento;

	private Integer alimento;

	private Integer numeroPagina;

	private Integer registrosPorPagina;

	public String getFechaHoraDesde() {
		return fechaHoraDesde;
	}

	public void setFechaHoraDesde(String fechaHoraDesde) {
		this.fechaHoraDesde = fechaHoraDesde;
	}

	public String getFechaHoraHasta() {
		return fechaHoraHasta;
	}

	public void setFechaHoraHasta(String fechaHoraHasta) {
		this.fechaHoraHasta = fechaHoraHasta;
	}

	public ListadoAlimentoIngeridoRequest() {
		super();
	}

	public Integer getAlimento() {
		return alimento;
	}

	public void setAlimento(Integer alimento) {
		this.alimento = alimento;
	}

	public Integer getNumeroPagina() {
		return numeroPagina != null ? numeroPagina : Constantes.PAGINA_INICIAL;
	}

	public void setNumeroPagina(Integer numeroPagina) {
		this.numeroPagina = numeroPagina;
	}

	public Integer getRegistrosPorPagina() {
		return registrosPorPagina != null ? registrosPorPagina : Constantes.REGISTROS_POR_PAGINA_POR_DEFECTO;
	}

	public void setRegistrosPorPagina(Integer registrosPorPagina) {
		this.registrosPorPagina = registrosPorPagina;
	}

	public Integer getMomento() {
		return momento;
	}

	public void setMomento(Integer momento) {
		this.momento = momento;
	}

	public Double getPorcionesDesde() {
		return porcionesDesde;
	}

	public void setPorcionesDesde(Double porcionesDesde) {
		this.porcionesDesde = porcionesDesde;
	}

	public Double getPorcionesHasta() {
		return porcionesHasta;
	}

	public void setPorcionesHasta(Double porcionesHasta) {
		this.porcionesHasta = porcionesHasta;
	}

}
