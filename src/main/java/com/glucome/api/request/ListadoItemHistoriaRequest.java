package com.glucome.api.request;

import com.glucome.utils.Constantes;

public class ListadoItemHistoriaRequest {

	private long medico;

	private String fechaDesde;

	private String fechaHasta;

	private String descripcion;

	private Integer numeroPagina;

	private Integer registrosPorPagina;

	public Integer getNumeroPagina() {
		return numeroPagina != null ? numeroPagina : Constantes.PAGINA_INICIAL;
	}

	public void setNumeroPagina(Integer numeroPagina) {
		this.numeroPagina = numeroPagina;
	}

	public Integer getRegistrosPorPagina() {
		return registrosPorPagina != null ? registrosPorPagina : Constantes.REGISTROS_POR_PAGINA_POR_DEFECTO;
	}

	public void setRegistrosPorPagina(Integer registrosPorPagina) {
		this.registrosPorPagina = registrosPorPagina;
	}

	public void setNumeroPagina(int numeroPagina) {
		this.numeroPagina = numeroPagina;
	}

	public String getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public String getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public ListadoItemHistoriaRequest(long medico, String fechaDesde, String fechaHasta, String descripcion, Integer numeroPagina, Integer registrosPorPagina) {
		super();
		this.medico = medico;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.descripcion = descripcion;
		this.numeroPagina = numeroPagina != null ? numeroPagina : Constantes.PAGINA_INICIAL;
		this.registrosPorPagina = registrosPorPagina != null ? registrosPorPagina : Constantes.REGISTROS_POR_PAGINA_POR_DEFECTO;
	}

	public ListadoItemHistoriaRequest() {
		super();
	}

	public long getMedico() {
		return medico;
	}

	public void setMedico(long idMedico) {
		this.medico = idMedico;
	}
}
