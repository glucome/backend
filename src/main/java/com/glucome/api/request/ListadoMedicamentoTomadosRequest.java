package com.glucome.api.request;

import com.glucome.utils.Constantes;

public class ListadoMedicamentoTomadosRequest {
	private String fechaHoraDesde;

	private String fechaHoraHasta;

	private String medicamento;

	private Double cantidadDesde;

	private Double cantidadHasta;

	private Integer numeroPagina;

	private Integer registrosPorPagina;

	public ListadoMedicamentoTomadosRequest(String fechaHoraDesde, String fechaHoraHasta, String medicamento, Double cantidadDesde, Double cantidadHasta, Integer numeroPagina, Integer registrosPorPagina) {
		super();
		this.fechaHoraDesde = fechaHoraDesde;
		this.fechaHoraHasta = fechaHoraHasta;
		this.medicamento = medicamento;
		this.cantidadDesde = cantidadDesde;
		this.cantidadHasta = cantidadHasta;
		this.numeroPagina = numeroPagina != null ? numeroPagina : Constantes.PAGINA_INICIAL;
		this.registrosPorPagina = registrosPorPagina != null ? registrosPorPagina : Constantes.REGISTROS_POR_PAGINA_POR_DEFECTO;
	}

	public String getFechaHoraDesde() {
		return fechaHoraDesde;
	}

	public void setFechaHoraDesde(String fechaHoraDesde) {
		this.fechaHoraDesde = fechaHoraDesde;
	}

	public String getFechaHoraHasta() {
		return fechaHoraHasta;
	}

	public void setFechaHoraHasta(String fechaHoraHasta) {
		this.fechaHoraHasta = fechaHoraHasta;
	}

	public String getMedicamento() {
		return medicamento;
	}

	public void setMedicamento(String medicamento) {
		this.medicamento = medicamento;
	}

	public Double getCantidadDesde() {
		return cantidadDesde;
	}

	public void setCantidadDesde(Double cantidadDesde) {
		this.cantidadDesde = cantidadDesde;
	}

	public Double getCantidadHasta() {
		return cantidadHasta;
	}

	public void setCantidadHasta(Double cantidadHasta) {
		this.cantidadHasta = cantidadHasta;
	}

	public Integer getNumeroPagina() {
		return numeroPagina != null ? numeroPagina : Constantes.PAGINA_INICIAL;
	}

	public void setNumeroPagina(Integer numeroPagina) {
		this.numeroPagina = numeroPagina;
	}

	public Integer getRegistrosPorPagina() {
		return registrosPorPagina != null ? registrosPorPagina : Constantes.REGISTROS_POR_PAGINA_POR_DEFECTO;
	}

	public void setRegistrosPorPagina(Integer registrosPorPagina) {
		this.registrosPorPagina = registrosPorPagina;
	}

	public void setNumeroPagina(int numeroPagina) {
		this.numeroPagina = numeroPagina;
	}

	public ListadoMedicamentoTomadosRequest() {
		super();
	}

}
