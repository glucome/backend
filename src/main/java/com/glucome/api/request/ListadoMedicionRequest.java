package com.glucome.api.request;

import com.glucome.utils.Constantes;

public class ListadoMedicionRequest {

	private String fechaHoraDesde;

    private String fechaHoraHasta;

    private Integer valorDesde;

    private Integer valorHasta;

    private Integer tipo;

    private Integer momento;

    private Integer numeroPagina;

    private Integer registrosPorPagina;

	public ListadoMedicionRequest(String fechaHoraDesde, String fechaHoraHasta, Integer valorDesde, Integer valorHasta, Integer tipo, Integer momento, Integer numeroPagina, Integer cantidadDeRegistrosPorPagina) {
		super();
		this.fechaHoraDesde = fechaHoraDesde;
		this.fechaHoraHasta = fechaHoraHasta;
		this.valorDesde = valorDesde;
		this.valorHasta = valorHasta;
		this.tipo = tipo;
		this.momento = momento;
		this.numeroPagina = numeroPagina != null ? numeroPagina : Constantes.PAGINA_INICIAL;
		this.registrosPorPagina = cantidadDeRegistrosPorPagina != null ? cantidadDeRegistrosPorPagina : Constantes.REGISTROS_POR_PAGINA_POR_DEFECTO;
	}

    public String getFechaHoraDesde() {
        return fechaHoraDesde;
    }

    public void setFechaHoraDesde(String fechaHoraDesde) {
        this.fechaHoraDesde = fechaHoraDesde;
    }

    public String getFechaHoraHasta() {
        return fechaHoraHasta;
    }

    public void setFechaHoraHasta(String fechaHoraHasta) {
        this.fechaHoraHasta = fechaHoraHasta;
    }

    public Integer getValorDesde() {
        return valorDesde;
    }

    public void setValorDesde(Integer valorDesde) {
        this.valorDesde = valorDesde;
    }

    public Integer getValorHasta() {
        return valorHasta;
    }

    public void setValorHasta(Integer valorHasta) {
        this.valorHasta = valorHasta;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getMomento() {
        return momento;
    }

    public void setMomento(Integer momento) {
        this.momento = momento;
    }

	public Integer getNumeroPagina() {
		return numeroPagina != null ? numeroPagina : Constantes.PAGINA_INICIAL;
	}

    public void setNumeroPagina(Integer numeroPagina) {
        this.numeroPagina = numeroPagina;
    }

	public Integer getRegistrosPorPagina() {
		return registrosPorPagina != null ? registrosPorPagina : Constantes.REGISTROS_POR_PAGINA_POR_DEFECTO;
	}

    public void setRegistrosPorPagina(Integer registrosPorPagina) {
        this.registrosPorPagina = registrosPorPagina;
    }

    public void setNumeroPagina(int numeroPagina) {
        this.numeroPagina = numeroPagina;
    }

    public ListadoMedicionRequest() {
        super();
    }

}