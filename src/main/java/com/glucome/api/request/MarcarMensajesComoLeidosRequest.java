package com.glucome.api.request;

public class MarcarMensajesComoLeidosRequest {

    private Long idChat;
    private Long idAutor;
    private String fechaHoraVisto;

    public MarcarMensajesComoLeidosRequest() {}

    public MarcarMensajesComoLeidosRequest(Long idChat, Long idAutor, String fechaHoraVisto) {
        this.idChat = idChat;
        this.idAutor = idAutor;
        this.fechaHoraVisto = fechaHoraVisto;
    }

    public Long getIdChat() { return idChat; }

    public void setIdChat(Long idChat) { this.idChat = idChat; }

    public Long getIdAutor() { return idAutor; }

    public void setIdAutor(Long idAutor) { this.idAutor = idAutor; }

    public String getFechaHoraVisto() { return fechaHoraVisto; }

    public void setFechaHoraVisto(String fechaHoraVisto) { this.fechaHoraVisto = fechaHoraVisto; }
}
