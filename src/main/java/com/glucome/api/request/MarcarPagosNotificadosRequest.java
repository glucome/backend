package com.glucome.api.request;

import java.util.List;

public class MarcarPagosNotificadosRequest {

    private List<Long> idsPagos;

    public MarcarPagosNotificadosRequest() { }

    public MarcarPagosNotificadosRequest(List<Long> idsPagos) {
        this.idsPagos = idsPagos;
    }

    public List<Long> getIdsPagos() {
        return idsPagos;
    }

    public void setIdsPagos(List<Long> idsPagos) {
        this.idsPagos = idsPagos;
    }
}
