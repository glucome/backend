package com.glucome.api.request;

public class MedicamentoTomadoPacienteRequest {

	private long id;

	private long idMedicamento;

	private double cantidad;

	private String fechaHora;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getIdMedicamento() {
		return idMedicamento;
	}

	public void setIdMedicamento(long idMedicamento) {
		this.idMedicamento = idMedicamento;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public String getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}

	public MedicamentoTomadoPacienteRequest(long id, long idMedicamento, double cantidad, String fechaHora) {
		super();
		this.id = id;
		this.idMedicamento = idMedicamento;
		this.cantidad = cantidad;
		this.fechaHora = fechaHora;
	}

	public MedicamentoTomadoPacienteRequest() {
	}

}
