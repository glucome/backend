package com.glucome.api.request;

public class MedicionRequest {

    //Formato dd/MM/yyyy HH:mm
    private String fechaHora;

    private int valor;

    private int momento;

    private int tipo;

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public MedicionRequest() {
        super();
    }

    public MedicionRequest(String fechaHora, int valor, int momento, int tipo) {
        super();
        this.fechaHora = fechaHora;
        this.valor = valor;
        this.momento = momento;
        this.tipo = tipo;
    }

    public int getMomento() {
        return momento;
    }

    public void setMomento(int momento) {
        this.momento = momento;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }


}
