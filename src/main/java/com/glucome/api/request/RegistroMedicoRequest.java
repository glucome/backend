package com.glucome.api.request;

import com.glucome.interfaces.RegistroUsuarioRequest;

public class RegistroMedicoRequest implements RegistroUsuarioRequest {

	private long id;

	private String nombreUsuario;

	private String mail;

	private String password;

	private String nombre;

	private String matricula;

	private String descripcion;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public RegistroMedicoRequest(long id, String nombreUsuario, String mail, String password, String nombre, String matricula) {
		super();
		this.id = id;
		this.nombreUsuario = nombreUsuario;
		this.mail = mail;
		this.password = password;
		this.nombre = nombre;
		this.matricula = matricula;
	}

	public RegistroMedicoRequest() {
		super();
	}

	public String getDescripcion() {
		return descripcion;
	}

}
