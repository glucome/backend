package com.glucome.api.request;

public class RegistroPacienteExternoRequest {

	private long id;

	private String nombre;

	private double altura;

	private String fechaNacimiento;

	private double peso;

	private int sexo;

	private int tipoDiabetes;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public int getSexo() {
		return sexo;
	}

	public void setSexo(int sexo) {
		this.sexo = sexo;
	}

	public int getTipoDiabetes() {
		return tipoDiabetes;
	}

	public void setTipoDiabetes(int tipoDiabetes) {
		this.tipoDiabetes = tipoDiabetes;
	}

	public RegistroPacienteExternoRequest(String nombre, double altura, String fechaNacimiento, double peso, int sexo, int tipoDiabetes) {
		super();

		this.nombre = nombre;
		this.altura = altura;
		this.fechaNacimiento = fechaNacimiento;
		this.peso = peso;
		this.sexo = sexo;
		this.tipoDiabetes = tipoDiabetes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public RegistroPacienteExternoRequest() {
	}

}
