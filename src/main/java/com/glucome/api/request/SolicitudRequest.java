package com.glucome.api.request;

public class SolicitudRequest {

    private long idSolicitante;

    private long idSolicitado;

    public long getIdSolicitante() {
        return idSolicitante;
    }

    public void setIdSolicitante(long idSolicitante) {
        this.idSolicitante = idSolicitante;
    }

    public long getIdSolicitado() {
        return idSolicitado;
    }

    public void setIdSolicitado(long idSolicitado) {
        this.idSolicitado = idSolicitado;
    }

}
