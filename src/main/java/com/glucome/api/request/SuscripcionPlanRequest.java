package com.glucome.api.request;

public class SuscripcionPlanRequest {
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public SuscripcionPlanRequest(long id) {
		super();
		this.id = id;
	}

	public SuscripcionPlanRequest() {
		super();
	}
}
