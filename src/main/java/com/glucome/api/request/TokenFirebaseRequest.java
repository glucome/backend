package com.glucome.api.request;

public class TokenFirebaseRequest {

    String tokenFirebase;

	public String getTokenFirebase() {
		return tokenFirebase;
	}

	public void setTokenFirebase(String tokenFirebase) {
		this.tokenFirebase = tokenFirebase;
	}

}
