package com.glucome.api.request;

public class UbicacionRequest {

	private String ubicacion;

	public UbicacionRequest() {}

	public UbicacionRequest(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getUbicacion() { return ubicacion; }

	public void setUbicacion(String ubicacion) { this.ubicacion = ubicacion; }
}
