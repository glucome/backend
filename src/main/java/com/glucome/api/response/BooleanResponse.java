package com.glucome.api.response;

import com.glucome.api.Response;

public class BooleanResponse implements Response {

    boolean valor;

    public BooleanResponse(boolean valor) {
        this.valor = valor;
    }

    public boolean isValor() {
        return valor;
    }

    public void setValor(boolean valor) {
        this.valor = valor;
    }
}
