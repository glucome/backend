package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Chat;

public class ChatResponse implements Response {

    private Chat chat;

    public ChatResponse(Chat chat) { this.chat = chat; }

    public Chat getChat() {  return chat; }

    public void setChat(Chat chat) { this.chat = chat; }
}
