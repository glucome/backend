package com.glucome.api.response;

import com.glucome.api.Response;

public class EstadoResponse implements Response {

    boolean valor;

    public EstadoResponse(boolean valor) {
        this.valor = valor;
    }

    public boolean isValor() {
        return valor;
    }

    public void setValor(boolean valor) {
        this.valor = valor;
    }
}
