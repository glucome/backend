package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.enumerador.EstadoUsuario;

public class EstadoUsuarioResponse implements Response {

    private EstadoUsuario estado;

    public EstadoUsuarioResponse(EstadoUsuario estado) {
        this.estado = estado;
    }

    public EstadoUsuario getEstado() {
        return estado;
    }

    public void setEstado(EstadoUsuario estado) {
        this.estado = estado;
    }
}
