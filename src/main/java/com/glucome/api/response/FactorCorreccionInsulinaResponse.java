package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.FactorCorreccionInsulina;

public class FactorCorreccionInsulinaResponse implements Response {

	private double factorCorreccionCHO;

	private double factorCorreccionAzucar;

	public double getFactorCorreccionAzucar() {
		return factorCorreccionAzucar;
	}

	public void setFactorCorreccionAzucar(double factorCorreccionAzucar) {
		this.factorCorreccionAzucar = factorCorreccionAzucar;
	}

	public double getFactorCorreccionCHO() {
		return factorCorreccionCHO;
	}

	public void setFactorCorreccionCHO(double factorCorreccionCHO) {
		this.factorCorreccionCHO = factorCorreccionCHO;
	}

	public FactorCorreccionInsulinaResponse() {
		super();
	}

	public FactorCorreccionInsulinaResponse(double factorCorreccionAzucar, double factorCorreccionCHO) {
		super();
		this.factorCorreccionAzucar = factorCorreccionAzucar;
		this.factorCorreccionCHO = factorCorreccionCHO;
	}

	public FactorCorreccionInsulinaResponse(FactorCorreccionInsulina factor) {
		this.factorCorreccionAzucar = factor.getFactorCorreccionAzucar();
		this.factorCorreccionCHO = factor.getFactorCorreccionCHO();
	}
}
