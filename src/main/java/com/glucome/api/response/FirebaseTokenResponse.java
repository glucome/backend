package com.glucome.api.response;

import com.glucome.api.Response;

public class FirebaseTokenResponse implements Response {

    String token;
    
	public FirebaseTokenResponse() {
		super();
	}

    public FirebaseTokenResponse(String token) {
    	super();
    	this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
