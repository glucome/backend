package com.glucome.api.response;

import com.glucome.api.Response;

public class IdResponse implements Response {
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public IdResponse(long id) {
		super();
		this.id = id;
	}

	public IdResponse() {
		super();
	}
}