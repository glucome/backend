package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Paciente;

public class InfoTarjetaResponse implements Response {

	Paciente paciente;

    public InfoTarjetaResponse(Paciente paciente) {
        this.paciente = paciente;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
    
}
