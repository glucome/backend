package com.glucome.api.response;

import com.glucome.api.Response;

public class InsulinaInyecadaResponse implements Response {

    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public InsulinaInyecadaResponse(long id) {
        super();
        this.id = id;
    }

    public InsulinaInyecadaResponse() {
        super();
    }

}
