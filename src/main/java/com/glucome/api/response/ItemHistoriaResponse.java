package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.ItemHistoriaClinica;

public class ItemHistoriaResponse implements Response {

	private long id;

	private ItemHistoriaClinica item;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ItemHistoriaResponse(long id) {
		super();
		this.id = id;
	}

	public ItemHistoriaResponse() {
		super();
	}

	public ItemHistoriaClinica getItem() {
		return item;
	}

	public void setItem(ItemHistoriaClinica item) {
		this.item = item;
	}

}
