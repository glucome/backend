package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.AlimentoIngerido;

import java.util.List;

public class ListadoAlimentoIngeridoResponse implements Response {

	private List<AlimentoIngerido> alimentoIngerido;

	private boolean ultimaPagina;

	public ListadoAlimentoIngeridoResponse(List<AlimentoIngerido> alimentoIngerido, boolean ultimaPagina) {
		this.alimentoIngerido = alimentoIngerido;
		this.ultimaPagina = ultimaPagina;
	}

	public List<AlimentoIngerido> getAlimentoIngerido() {
		return alimentoIngerido;
	}

	public void setAlimentoIngerido(List<AlimentoIngerido> alimentoIngerido) {
		this.alimentoIngerido = alimentoIngerido;
	}

	public boolean isUltimaPagina() {
		return ultimaPagina;
	}

	public void setUltimaPagina(boolean ultimaPagina) {
		this.ultimaPagina = ultimaPagina;
	}
}
