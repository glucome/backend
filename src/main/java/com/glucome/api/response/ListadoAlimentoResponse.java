package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Alimento;

import java.util.ArrayList;
import java.util.List;

public class ListadoAlimentoResponse implements Response {

    private List<Alimento> alimentos;

    public ListadoAlimentoResponse(List<Alimento> alimentos) {
        this.alimentos = alimentos;
    }
    
    public List<Alimento> getAlimentos() {
        return alimentos;
    }

    public void setAlimentos(List<Alimento> alimentos) {
        this.alimentos = alimentos;
    }

    public void agregarAlimentos(List<Alimento> alimentos) {
        if (this.alimentos == null) {
            this.alimentos = new ArrayList<>();
        }
        this.alimentos.addAll(alimentos);
    }

}
