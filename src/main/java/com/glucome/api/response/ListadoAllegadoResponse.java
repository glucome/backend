package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Allegado;

import java.util.List;

public class ListadoAllegadoResponse implements Response {

	private List<Allegado> allegados;

	public ListadoAllegadoResponse(List<Allegado> allegados) {
		this.allegados = allegados;
	}

	public List<Allegado> getAllegados() {
		return allegados;
	}

	public void setAllegados(List<Allegado> allegados) {
		this.allegados = allegados;
	}

}
