package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Contacto;

import java.util.List;

public class ListadoContactoResponse implements Response {

	private List<Contacto> contactos;

	public ListadoContactoResponse(List<Contacto> contactos) {
		this.contactos = contactos;
	}

	public List<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos(List<Contacto> contactos) {
		this.contactos = contactos;
	}

}
