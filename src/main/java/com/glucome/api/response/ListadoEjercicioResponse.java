package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Ejercicio;

import java.util.List;

public class ListadoEjercicioResponse implements Response {

	private List<Ejercicio> ejercicios;

	private boolean ultimaPagina;

	public ListadoEjercicioResponse(List<Ejercicio> ejercicios, boolean ultimaPagina) {
		this.ejercicios = ejercicios;
		this.ultimaPagina = ultimaPagina;
	}

	public List<Ejercicio> getEjercicios() {
		return ejercicios;
	}

	public void setEjercicios(List<Ejercicio> ejercicios) {
		this.ejercicios = ejercicios;
	}

	public boolean isUltimaPagina() {
		return ultimaPagina;
	}

	public void setUltimaPagina(boolean ultimaPagina) {
		this.ultimaPagina = ultimaPagina;
	}
}
