package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.InsulinaInyectada;

import java.util.List;

public class ListadoInyeccionInsulinaResponse implements Response {

    private List<InsulinaInyectada> InsulinaInyectadas;

    private boolean ultimaPagina;

    public ListadoInyeccionInsulinaResponse(List<InsulinaInyectada> InsulinaInyectadas, boolean ultimaPagina) {
        this.InsulinaInyectadas = InsulinaInyectadas;
        this.ultimaPagina = ultimaPagina;
    }

    public List<InsulinaInyectada> getInsulinaInyectadas() {
        return InsulinaInyectadas;
    }

    public void setInsulinaInyectadas(List<InsulinaInyectada> InsulinaInyectadas) {
        this.InsulinaInyectadas = InsulinaInyectadas;
    }

    public boolean isUltimaPagina() {
        return ultimaPagina;
    }

    public void setUltimaPagina(boolean ultimaPagina) {
        this.ultimaPagina = ultimaPagina;
    }
}
