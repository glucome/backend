package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.ItemHistoriaClinica;

import java.util.List;

public class ListadoItemHistoriaInsulinaResponse implements Response {

	private List<ItemHistoriaClinica> itemsHistoria;

	private boolean ultimaPagina;

	public List<ItemHistoriaClinica> getItemsHistoria() {
		return itemsHistoria;
	}

	public void setItemsHistoria(List<ItemHistoriaClinica> itemsHistoria) {
		this.itemsHistoria = itemsHistoria;
	}

	public boolean isUltimaPagina() {
		return ultimaPagina;
	}

	public void setUltimaPagina(boolean ultimaPagina) {
		this.ultimaPagina = ultimaPagina;
	}

	public ListadoItemHistoriaInsulinaResponse(List<ItemHistoriaClinica> itemsHistoria, boolean ultimaPagina) {
		super();
		this.itemsHistoria = itemsHistoria;
		this.ultimaPagina = ultimaPagina;
	}

	public ListadoItemHistoriaInsulinaResponse() {
		super();
	}

}
