package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Medicamento;

import java.util.List;

public class ListadoMedicamentoResponse implements Response {

    private List<Medicamento> medicamentos;

    public ListadoMedicamentoResponse(List<Medicamento> medicamentos) {
        this.medicamentos = medicamentos;
    }

    public List<Medicamento> getMedicamentos() {
        return medicamentos;
    }

    public void setMedicamentos(List<Medicamento> medicamentos) {
        this.medicamentos = medicamentos;
    }

}
