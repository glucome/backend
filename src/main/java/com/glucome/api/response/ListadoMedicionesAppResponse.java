package com.glucome.api.response;

import com.glucome.api.Response;

import java.util.List;

public class ListadoMedicionesAppResponse implements Response {

	private List<MedicionPacienteResponse> mediciones;

    public ListadoMedicionesAppResponse(List<MedicionPacienteResponse> mediciones) {
        this.mediciones = mediciones;
    }

    public List<MedicionPacienteResponse> getMediciones() {
        return mediciones;
    }

    public void setMediciones(List<MedicionPacienteResponse> mediciones) {
        this.mediciones = mediciones;
    }
}
