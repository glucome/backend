package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Medicion;

import java.util.List;

public class ListadoMedicionesResponse implements Response {

    private List<Medicion> mediciones;

    private boolean ultimaPagina;

    public ListadoMedicionesResponse(List<Medicion> mediciones, boolean ultimaPagina) {
        this.mediciones = mediciones;
        this.ultimaPagina = ultimaPagina;
    }

    public List<Medicion> getMediciones() {
        return mediciones;
    }

    public void setMediciones(List<Medicion> mediciones) {
        this.mediciones = mediciones;
    }

    public boolean isUltimaPagina() {
        return ultimaPagina;
    }

    public void setUltimaPagina(boolean ultimaPagina) {
        this.ultimaPagina = ultimaPagina;
    }
}
