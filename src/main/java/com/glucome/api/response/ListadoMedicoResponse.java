package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Medico;

import java.util.List;

public class ListadoMedicoResponse implements Response {

    private List<Medico> medicos;

    public ListadoMedicoResponse(List<Medico> medicos) {
        this.medicos = medicos;
    }

    public List<Medico> getMedicos() {
        return medicos;
    }

    public void setMedicos(List<Medico> medicos) {
        this.medicos = medicos;
    }

}
