package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Mensaje;

import java.util.List;

public class ListadoMensajeResponse implements Response {

    private List<Mensaje> mensajes;

    public ListadoMensajeResponse(List<Mensaje> mensajes) { this.mensajes = mensajes; }

    public List<Mensaje> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<Mensaje> mensajes) {  this.mensajes = mensajes;  }
}
