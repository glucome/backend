package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.wrapper.MensajeWrapper;

import java.util.List;

public class ListadoMensajeWrapperResponse implements Response {

    private List<MensajeWrapper> mensajes;

    public ListadoMensajeWrapperResponse(List<MensajeWrapper> mensajes) { this.mensajes = mensajes; }

    public List<MensajeWrapper> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<MensajeWrapper> mensajes) {
        this.mensajes = mensajes;
    }
}
