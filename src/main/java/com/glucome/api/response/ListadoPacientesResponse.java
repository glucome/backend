package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Paciente;

import java.util.List;

public class ListadoPacientesResponse implements Response {

    private List<Paciente> pacientes;

    public ListadoPacientesResponse(List<Paciente> pacientes) {
        this.pacientes = pacientes;
    }

    public List<Paciente> getPacientes() {
        return pacientes;
    }

    public void setPacientes(List<Paciente> pacientes) {
        this.pacientes = pacientes;
    }

}
