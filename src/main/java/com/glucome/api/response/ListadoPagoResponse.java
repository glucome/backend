package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Pago;

import java.util.List;

public class ListadoPagoResponse implements Response {

    private List<Pago> pagos;

    public ListadoPagoResponse(List<Pago> pagos) { this.pagos = pagos; }

    public List<Pago> getPagos() { return pagos; }

    public void setPagos(List<Pago> pagos) { this.pagos = pagos; }
}
