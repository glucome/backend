package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.enumerador.Plan;

import java.util.List;

public class ListadoPlanResponse implements Response {
	private List<Plan> planes;

	public List<Plan> getPlanes() {
		return planes;
	}

	public void setPlanes(List<Plan> planes) {
		this.planes = planes;
	}

	public ListadoPlanResponse(List<Plan> planes) {
		super();
		this.planes = planes;
	}

	public ListadoPlanResponse() {
		super();
	}
}
