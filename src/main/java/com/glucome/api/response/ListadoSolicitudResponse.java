package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.SolicitudVinculacion;

import java.util.List;

public class ListadoSolicitudResponse implements Response {

	private List<SolicitudVinculacion> solicitudes;

	public List<SolicitudVinculacion> getSolicitudes() {
		return solicitudes;
	}

	public void setSolicitudes(List<SolicitudVinculacion> solicitudes) {
		this.solicitudes = solicitudes;
	}

	public ListadoSolicitudResponse(List<SolicitudVinculacion> solicitudes) {
		super();
		this.solicitudes = solicitudes;
	}

	public ListadoSolicitudResponse() {
		super();
	}

}
