package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.wrapper.UsuarioChatWrapper;

import java.util.List;

public class ListadoUsuariosChatsResponse implements Response {

    private List<UsuarioChatWrapper> usuariosChats;

    public ListadoUsuariosChatsResponse(List<UsuarioChatWrapper> usuariosChats) {
        this.usuariosChats = usuariosChats;
    }

    public List<UsuarioChatWrapper> getUsuariosChats() {
        return usuariosChats;
    }

    public void setUsuariosChats(List<UsuarioChatWrapper> usuariosChats) {
        this.usuariosChats = usuariosChats;
    }
}
