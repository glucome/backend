package com.glucome.api.response;

import java.util.List;

import com.glucome.api.Response;
import com.glucome.wrapper.LogMedicionWrapper;

public class LogMedicionesResponse implements Response {

	private List<LogMedicionWrapper> logMedicion;

	public LogMedicionesResponse(List<LogMedicionWrapper> logMedicion) {
		this.logMedicion = logMedicion;
	}

	public List<LogMedicionWrapper> getLogMedicion() {
		return logMedicion;
	}

	public void setLogMedicion(List<LogMedicionWrapper> logMedicion) {
		this.logMedicion = logMedicion;
	}

}
