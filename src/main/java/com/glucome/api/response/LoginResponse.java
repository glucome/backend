package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.interfaces.Logueable;

import java.util.List;

public class LoginResponse implements Response {

    long id;
    String usuario;
    String token;
    Logueable asociado;
    List<String> roles;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public Logueable getAsociado() {
        return asociado;
    }

    public void setAsociado(Logueable asociado) {
        this.asociado = asociado;
    }

}
