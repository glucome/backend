package com.glucome.api.response;

import com.glucome.api.Response;

public class MedicamentoTomadoResponse implements Response {

	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public MedicamentoTomadoResponse(long id) {
		super();
		this.id = id;
	}

	public MedicamentoTomadoResponse() {
		super();
	}

}
