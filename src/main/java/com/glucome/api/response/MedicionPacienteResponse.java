package com.glucome.api.response;

import com.glucome.modelo.Medicion;
import com.glucome.utils.UtilesFecha;

import java.util.ArrayList;
import java.util.List;

public class MedicionPacienteResponse {

	private String valor;
	private String tipo;
	private String fecha;
	
	public MedicionPacienteResponse(String valor, String tipo, String fecha) {
		super();
		this.valor = valor;
		this.tipo = tipo;
		this.fecha = fecha;
	}
	
	public String getValor() {
		return valor;
	}
	
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getFecha() {
		return fecha;
	}
	
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	public static List<MedicionPacienteResponse> getFromMedicion(List<Medicion> mediciones){
		List<MedicionPacienteResponse> responses = new ArrayList<MedicionPacienteResponse>();
		for(Medicion medicion : mediciones) {
			MedicionPacienteResponse response = new MedicionPacienteResponse(String.valueOf(medicion.getValor()), String.valueOf(medicion.getMomento().getId()), UtilesFecha.getString(medicion.getFechaHora()));
			responses.add(response);
		}
		return responses;
	}
}
