package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Mensaje;

public class MensajeResponse implements Response {

    Mensaje mensaje;

    public MensajeResponse(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }
}
