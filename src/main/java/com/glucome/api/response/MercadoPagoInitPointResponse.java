package com.glucome.api.response;

import com.glucome.api.Response;

public class MercadoPagoInitPointResponse implements Response {

    private String initPoint;

    public MercadoPagoInitPointResponse(String initPoint) { this.initPoint = initPoint; }

    public String getInitPoint() { return initPoint; }

    public void setInitPoint(String initPoint) { this.initPoint = initPoint; }
}
