package com.glucome.api.response;

import com.glucome.api.Response;

public class PacienteExternoResponse implements Response {
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public PacienteExternoResponse(long id) {
		super();
		this.id = id;
	}

	public PacienteExternoResponse() {
		super();
	}
}
