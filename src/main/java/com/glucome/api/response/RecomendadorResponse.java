package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Alimento;

import java.util.ArrayList;
import java.util.List;

public class RecomendadorResponse implements Response {

    private List<Alimento> alimentosRecomendados;

    public List<Alimento> getAlimentosRecomendados() {
        return alimentosRecomendados;
    }

    public void setAlimentosRecomendados(List<Alimento> alimentosRecomendados) {
        this.alimentosRecomendados = alimentosRecomendados;
    }

    public void agregarAlimentosRecomendados(List<Alimento> alimentosRecomendados) {
        if (this.alimentosRecomendados == null) {
            this.alimentosRecomendados = new ArrayList<>();
        }
        this.alimentosRecomendados.addAll(alimentosRecomendados);
    }

}
