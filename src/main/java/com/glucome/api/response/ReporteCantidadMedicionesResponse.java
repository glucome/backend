package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.wrapper.ReporteCantidadMedicionesWrapper;

import java.util.List;

public class ReporteCantidadMedicionesResponse implements Response {

    private List<ReporteCantidadMedicionesWrapper> reporte;

    public ReporteCantidadMedicionesResponse(List<ReporteCantidadMedicionesWrapper> reporte) {
        this.reporte = reporte;
    }

    public List<ReporteCantidadMedicionesWrapper> getReporte() {
        return reporte;
    }

    public void setReporte(List<ReporteCantidadMedicionesWrapper> reporte) {
        this.reporte = reporte;
    }
}
