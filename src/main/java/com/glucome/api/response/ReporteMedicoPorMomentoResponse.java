package com.glucome.api.response;

import java.util.List;

import com.glucome.api.Response;
import com.glucome.wrapper.ReporteMedicoPorMomentoWrapper;

public class ReporteMedicoPorMomentoResponse implements Response {

    private List<ReporteMedicoPorMomentoWrapper> reporte;

    public ReporteMedicoPorMomentoResponse(List<ReporteMedicoPorMomentoWrapper> reporte) {
        this.reporte = reporte;
    }

    public List<ReporteMedicoPorMomentoWrapper> getReporte() {
        return reporte;
    }

    public void setReporte(List<ReporteMedicoPorMomentoWrapper> reporte) {
        this.reporte = reporte;
    }
}
