package com.glucome.api.response;

import java.util.List;

import com.glucome.api.Response;
import com.glucome.wrapper.ReporteMedicoPorRangoEdadWrapper;

public class ReporteMedicoPorRangoEdadResponse implements Response {

    private List<ReporteMedicoPorRangoEdadWrapper> reporte;

    public ReporteMedicoPorRangoEdadResponse(List<ReporteMedicoPorRangoEdadWrapper> reporte) {
        this.reporte = reporte;
    }

    public List<ReporteMedicoPorRangoEdadWrapper> getReporte() {
        return reporte;
    }

    public void setReporte(List<ReporteMedicoPorRangoEdadWrapper> reporte) {
        this.reporte = reporte;
    }
}
