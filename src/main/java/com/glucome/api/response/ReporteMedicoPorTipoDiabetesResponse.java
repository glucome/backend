package com.glucome.api.response;

import java.util.List;

import com.glucome.api.Response;
import com.glucome.wrapper.ReporteMedicoPorTipoDiabetesWrapper;

public class ReporteMedicoPorTipoDiabetesResponse implements Response {

    private List<ReporteMedicoPorTipoDiabetesWrapper> reporte;

    public ReporteMedicoPorTipoDiabetesResponse(List<ReporteMedicoPorTipoDiabetesWrapper> reporte) {
        this.reporte = reporte;
    }

    public List<ReporteMedicoPorTipoDiabetesWrapper> getReporte() {
        return reporte;
    }

    public void setReporte(List<ReporteMedicoPorTipoDiabetesWrapper> reporte) {
        this.reporte = reporte;
    }
}
