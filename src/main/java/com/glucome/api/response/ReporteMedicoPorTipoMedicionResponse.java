package com.glucome.api.response;

import java.util.List;

import com.glucome.api.Response;
import com.glucome.wrapper.ReporteMedicoPorTipoMedicionWrapper;

public class ReporteMedicoPorTipoMedicionResponse implements Response {

    private List<ReporteMedicoPorTipoMedicionWrapper> reporte;

    public ReporteMedicoPorTipoMedicionResponse(List<ReporteMedicoPorTipoMedicionWrapper> reporte) {
        this.reporte = reporte;
    }

    public List<ReporteMedicoPorTipoMedicionWrapper> getReporte() {
        return reporte;
    }

    public void setReporte(List<ReporteMedicoPorTipoMedicionWrapper> reporte) {
        this.reporte = reporte;
    }
}
