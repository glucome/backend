package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.wrapper.ReporteMedicoRankingAlimentosWrapper;

import java.util.List;

public class ReporteMedicoRankingAlimentosResponse implements Response {

    private List<ReporteMedicoRankingAlimentosWrapper> reporte;

    public ReporteMedicoRankingAlimentosResponse(List<ReporteMedicoRankingAlimentosWrapper> reporte) {
        this.reporte = reporte;
    }

    public List<ReporteMedicoRankingAlimentosWrapper> getReporte() {
        return reporte;
    }

    public void setReporte(List<ReporteMedicoRankingAlimentosWrapper> reporte) {
        this.reporte = reporte;
    }
}
