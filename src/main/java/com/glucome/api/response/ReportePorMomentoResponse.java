package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.wrapper.ReportePorMomentoWrapper;

import java.util.List;

public class ReportePorMomentoResponse implements Response {

    private List<ReportePorMomentoWrapper> reporte;

    public ReportePorMomentoResponse(List<ReportePorMomentoWrapper> reporte) {
        this.reporte = reporte;
    }

    public List<ReportePorMomentoWrapper> getReporte() {
        return reporte;
    }

    public void setReporte(List<ReportePorMomentoWrapper> reporte) {
        this.reporte = reporte;
    }
}
