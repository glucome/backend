package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.wrapper.ReportePorRangoEdadWrapper;

import java.util.List;

public class ReportePorRangoEdadResponse implements Response {

    private List<ReportePorRangoEdadWrapper> reporte;

    public ReportePorRangoEdadResponse(List<ReportePorRangoEdadWrapper> reporte) {
        this.reporte = reporte;
    }

    public List<ReportePorRangoEdadWrapper> getReporte() {
        return reporte;
    }

    public void setReporte(List<ReportePorRangoEdadWrapper> reporte) {
        this.reporte = reporte;
    }
}
