package com.glucome.api.response;

import java.util.List;

import com.glucome.api.Response;
import com.glucome.wrapper.ReportePorTipoDiabetesWrapper;

public class ReportePorTipoDiabetesResponse implements Response {

    private List<ReportePorTipoDiabetesWrapper> reporte;

    public ReportePorTipoDiabetesResponse(List<ReportePorTipoDiabetesWrapper> reporte) {
        this.reporte = reporte;
    }

    public List<ReportePorTipoDiabetesWrapper> getReporte() {
        return reporte;
    }

    public void setReporte(List<ReportePorTipoDiabetesWrapper> reporte) {
        this.reporte = reporte;
    }
}
