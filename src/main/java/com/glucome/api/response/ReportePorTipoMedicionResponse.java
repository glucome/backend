package com.glucome.api.response;

import java.util.List;

import com.glucome.api.Response;
import com.glucome.wrapper.ReportePorTipoMedicionWrapper;

public class ReportePorTipoMedicionResponse implements Response {

    private List<ReportePorTipoMedicionWrapper> reporte;

    public ReportePorTipoMedicionResponse(List<ReportePorTipoMedicionWrapper> reporte) {
        this.reporte = reporte;
    }

    public List<ReportePorTipoMedicionWrapper> getReporte() {
        return reporte;
    }

    public void setReporte(List<ReportePorTipoMedicionWrapper> reporte) {
        this.reporte = reporte;
    }
}
