package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.wrapper.ReporteRankingAlimentosWrapper;

import java.util.List;

public class ReporteRankingAlimentosResponse implements Response {

    private List<ReporteRankingAlimentosWrapper> reporte;

    public ReporteRankingAlimentosResponse(List<ReporteRankingAlimentosWrapper> reporte) {
        this.reporte = reporte;
    }

    public List<ReporteRankingAlimentosWrapper> getReporte() {
        return reporte;
    }

    public void setReporte(List<ReporteRankingAlimentosWrapper> reporte) {
        this.reporte = reporte;
    }
}
