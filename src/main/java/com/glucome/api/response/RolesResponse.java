package com.glucome.api.response;

import com.glucome.api.Response;

import java.util.List;

public class RolesResponse implements Response {

    private List<String> roles;

    public RolesResponse(List<String> roles) { this.roles = roles; }

    public List<String> getRoles() {  return roles; }

    public void setRoles(List<String> roles) { this.roles = roles; }
}
