package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.SolicitudVinculacion;

public class SolicitudResponse implements Response {

    private SolicitudVinculacion solicitud;

    public SolicitudResponse(SolicitudVinculacion solicitud) {
        super();
        this.solicitud = solicitud;
    }

    public SolicitudVinculacion getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(SolicitudVinculacion solicitud) {
        this.solicitud = solicitud;
    }

    public SolicitudResponse() {
    }

}
