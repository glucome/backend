package com.glucome.api.response;

import com.glucome.api.Response;

public class UbicacionResponse implements Response {

    String ubicacion;

    public UbicacionResponse(String ubicacion) {  this.ubicacion = ubicacion; }

    public String getUbicacion() { return ubicacion; }

    public void setUbicacion(String ubicacion) {  this.ubicacion = ubicacion;  }
}
