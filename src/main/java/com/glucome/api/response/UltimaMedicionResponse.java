package com.glucome.api.response;

import com.glucome.api.Response;
import com.glucome.modelo.Medicion;

public class UltimaMedicionResponse implements Response {

    private Medicion medicion;

    public UltimaMedicionResponse(Medicion medicion) {
        this.medicion = medicion;
    }

    public Medicion getMedicion() {
        return medicion;
    }

    public void setMedicion(Medicion medicion) {
        this.medicion = medicion;
    }

}
