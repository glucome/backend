package com.glucome.api.response;

import java.util.List;

import com.glucome.api.Response;
import com.glucome.modelo.Medicion;

public class UltimasMedicionesResponse implements Response {

    private List<Medicion> mediciones;

    public UltimasMedicionesResponse(List<Medicion> mediciones) {
        this.mediciones = mediciones;
    }

    public List<Medicion> getMediciones() {
        return mediciones;
    }

    public void setMediciones(List<Medicion> mediciones) {
        this.mediciones = mediciones;
    }
}
