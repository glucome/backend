package com.glucome.controladores;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.LoginRequest;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.UsuarioServicio;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin("*")
public class AuthControlador {

	@Autowired
	UsuarioServicio usuarioServicio;

	@PostMapping("/signin/")
	public ResponseEntity<RespuestaApi> signin(@RequestBody LoginRequest data) {

		try {
			return new ResponseEntity<RespuestaApi>(usuarioServicio.logIn(data), HttpStatus.OK);
		} catch (BadCredentialsException e) {
			RespuestaApi respuestaApi = new RespuestaApi(e.getMessage());
			return new ResponseEntity<RespuestaApi>(respuestaApi, HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/signin/medico/")
	public ResponseEntity<RespuestaApi> signinMedico(@RequestBody LoginRequest data) {

		try {
			return new ResponseEntity<RespuestaApi>(usuarioServicio.logInMedico(data), HttpStatus.OK);
		} catch (BadCredentialsException e) {
			RespuestaApi respuestaApi = new RespuestaApi(e.getMessage());
			return new ResponseEntity<RespuestaApi>(respuestaApi, HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/cambiarPass/")
	public ResponseEntity<RespuestaApi> cambiarPass(@RequestBody LoginRequest data) {

		try {
			return new ResponseEntity<RespuestaApi>(usuarioServicio.cambiarPass(data), HttpStatus.OK);
		} catch (PersistenciaException | ParametrosNoValidosException e) {
			RespuestaApi respuestaApi = new RespuestaApi(e.getMessage());
			return new ResponseEntity<RespuestaApi>(respuestaApi, HttpStatus.BAD_REQUEST);
		} catch (BadCredentialsException e) {
			RespuestaApi respuestaApi = new RespuestaApi(e.getMessage());
			return new ResponseEntity<RespuestaApi>(respuestaApi, HttpStatus.NOT_ACCEPTABLE);
		}
	}
}