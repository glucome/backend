package com.glucome.controladores;

import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.CambiarEstadoRequest;
import com.glucome.api.request.ChatMedicoPacienteRequest;
import com.glucome.api.request.MarcarMensajesComoLeidosRequest;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.ChatServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/chat")
@CrossOrigin("*")
public class ChatControlador {

	@Autowired
	ChatServicio chatServicio;

	@PostMapping("/cambiar_estado/")
	public ResponseEntity<RespuestaApi> cambiarEstado(@RequestBody CambiarEstadoRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(chatServicio.cambiarEstado(data.getIdUsuario(), data.isOnline()), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/crear_chat_medico_paciente/")
	public ResponseEntity<RespuestaApi> crearChatMedicoPaciente(@RequestBody ChatMedicoPacienteRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(chatServicio.crearChatMedicoPaciente(data.getIdUsuarioMedico(), data.getIdUsuarioPaciente()), HttpStatus.OK);
		} catch (BadCredentialsException | PersistenciaException e) {
			RespuestaApi respuestaApi = new RespuestaApi(e.getMessage());
			return new ResponseEntity<RespuestaApi>(respuestaApi, HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/mensajes/")
	public ResponseEntity<RespuestaApi> listadoMensajes(@RequestParam(required = true) Long idChat) {
		try {
			return new ResponseEntity<RespuestaApi>(chatServicio.getMensajes(idChat), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/pacientes/")
	public ResponseEntity<RespuestaApi> listadoPacientesMedico(@RequestParam(required = true) Long idUsuarioMedico) {
		try {
			return new ResponseEntity<RespuestaApi>(chatServicio.getPacientesMedico(idUsuarioMedico), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/medicos/")
	public ResponseEntity<RespuestaApi> listadoMedicosPaciente(@RequestParam(required = true) Long idUsuarioPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(chatServicio.getMedicosPaciente(idUsuarioPaciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/marcar_mensajes_como_leidos/")
	public ResponseEntity<RespuestaApi> marcarMensajesComoLeidos(@RequestBody MarcarMensajesComoLeidosRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(chatServicio.marcarMensajesComoLeidos(data.getIdChat(), data.getIdAutor(), data.getFechaHoraVisto()), HttpStatus.OK);
		} catch (PersistenciaException | DateTimeParseException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}