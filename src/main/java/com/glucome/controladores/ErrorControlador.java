package com.glucome.controladores;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.glucome.api.RespuestaApi;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorControlador {

    @ExceptionHandler({InvalidFormatException.class})
    public ResponseEntity<RespuestaApi> invalidFormatException(InvalidFormatException e) {
        return new ResponseEntity<RespuestaApi>(new RespuestaApi("Error en los datos envíados, reviselos"), HttpStatus.BAD_REQUEST);
    }

}