package com.glucome.controladores;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.FirebaseTokenRequest;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.FirebaseServicio;
import com.glucome.utils.UtilesFecha;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/v1/firebase")
@CrossOrigin("*")
public class FirebaseControlador {

	@Autowired
	FirebaseServicio firebaseServicio;

	@PostMapping("/token/")
	public ResponseEntity<RespuestaApi> setearTokenFirebaseDeUsuario(@RequestBody FirebaseTokenRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(firebaseServicio.setearTokenFirebaseDeUsuario(data.getUsuario(), data.getFirebaseToken()), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/token/{idUsuario}/")
	public ResponseEntity<RespuestaApi> obtenerTokenFirebaseDeUsuario(@PathVariable long idUsuario) {
		try {
			return new ResponseEntity<RespuestaApi>(firebaseServicio.obtenerTokenFirebaseDeUsuario(idUsuario), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/enviarNotificacionMedicion/{idPaciente}/{idAvisado}/{medicion}/{ubicacion}/")
	public ResponseEntity<String> enviarNotificacionMedicionAUsuario(@PathVariable long idPaciente, @PathVariable long idAvisado, @PathVariable String medicion, @PathVariable String ubicacion) {

		try {
			CompletableFuture<String> pushNotification = firebaseServicio.enviarAlertaMedicionAUsuario(idPaciente, idAvisado, medicion, ubicacion);

			CompletableFuture.allOf(pushNotification).join();

			String firebaseResponse = pushNotification.get();

			return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (PersistenciaException e) {
			e.printStackTrace();
		} catch (HttpClientErrorException e) {
			//e.printStackTrace();
		}

		return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
	}

	@GetMapping("/enviarNotificacionMedicacion/{idPaciente}/{idAvisado}/{horaMedicacion}/")
	public ResponseEntity<String> enviarNotificacionMedicacionAUsuario(@PathVariable long idPaciente, @PathVariable long idAvisado, @PathVariable long horaMedicacion) {

		String hora = UtilesFecha.getString(UtilesFecha.millsToLocalDateTime(horaMedicacion));

		try {
			CompletableFuture<String> pushNotification = firebaseServicio.enviarAlertaMedicacionAUsuario(idPaciente, idAvisado, hora);

			CompletableFuture.allOf(pushNotification).join();

			String firebaseResponse = pushNotification.get();

			return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (PersistenciaException e) {
			e.printStackTrace();
		} catch (HttpClientErrorException e) {
			//e.printStackTrace();
		}

		return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
	}
}