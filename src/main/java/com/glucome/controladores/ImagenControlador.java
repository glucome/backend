package com.glucome.controladores;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.glucome.api.RespuestaApi;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Alimento;
import com.glucome.servicios.AlimentoServicio;
import com.glucome.servicios.UsuarioServicio;
import com.glucome.utils.Constantes;
import com.glucome.utils.UtilesString;

@Controller
@RequestMapping("/api/v1/imagen/")
@CrossOrigin("*")
public class ImagenControlador {

	@Autowired
	AlimentoServicio alimentoServicio;

	@Autowired
	UsuarioServicio usuarioServicio;

	@Value("${file.upload-dir:'C:/Imagenes/'}")
	private String rutaImagenes;

	@RequestMapping(value = "alimento/{idAlimento}/", method = RequestMethod.GET, produces = MediaType.IMAGE_PNG_VALUE)
	public @ResponseBody byte[] getImagenAlimento(@PathVariable long idAlimento) throws IOException {
		Alimento alimento;
		try {
			alimento = alimentoServicio.get(idAlimento);
			if (alimento != null) {
				File file = new File(rutaImagenes + Constantes.RUTA_RELATIVA_IMAGENES_ALIMENTOS + alimento.getImagen());
				InputStream in = new FileInputStream(file);
				return IOUtils.toByteArray(in);
			} else {
				throw new PersistenciaException("Alimento inválido");
			}
		} catch (PersistenciaException e) {
			return null;
		}
	}

	@RequestMapping(value = "usuario/{idUsuario}/", method = RequestMethod.GET, produces = MediaType.IMAGE_PNG_VALUE)
	public @ResponseBody byte[] getImagenUsuario(@PathVariable long idUsuario) throws IOException {
		File file = null;
		InputStream in;
		file = usuarioServicio.getImagenUsuario(idUsuario);
		in = new FileInputStream(file);
		return IOUtils.toByteArray(in);
	}

	@RequestMapping(value = "alimento_ingerido/{imagen}/", method = RequestMethod.GET, produces = MediaType.IMAGE_PNG_VALUE)
	public @ResponseBody byte[] getImagenUsuario(@PathVariable String imagen) throws IOException, NumberFormatException, PersistenciaException {
		String rutaImagen = rutaImagenes;
		File file = null;
		InputStream in;
		if (UtilesString.esNumeroEntero(imagen)) {
			Alimento alimento = alimentoServicio.get(Long.parseLong(imagen));
			rutaImagen = rutaImagen.concat(Constantes.RUTA_RELATIVA_IMAGENES_ALIMENTOS).concat(alimento.getImagen());
		} else {
			rutaImagen = rutaImagen.concat(imagen);
		}
		file = new File(rutaImagen);
		in = new FileInputStream(file);
		return IOUtils.toByteArray(in);
	}

	@PostMapping("{idUsuario}/")
	private ResponseEntity<RespuestaApi> setearImagen(@PathVariable long idUsuario, @RequestParam(required = false) MultipartFile file) {
		try {

			return new ResponseEntity<RespuestaApi>(usuarioServicio.setImagen(idUsuario, file), HttpStatus.OK);
		} catch (IOException | PersistenciaException e) {
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("Ha ocurrido un error al subir el archivo"), HttpStatus.BAD_REQUEST);
		}
	}

}
