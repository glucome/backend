package com.glucome.controladores;

import com.glucome.api.RespuestaApi;
import com.glucome.enumerador.TipoUsuario;
import com.glucome.servicios.MercadoPagoServicio;
import com.mercadopago.exceptions.MPException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/mercadopago")
@CrossOrigin("*")
public class MercadoPagoControlador {

	@Autowired
	private MercadoPagoServicio mercadoPagoServicio;

	@GetMapping("/init_point/medico/{idUsuario}")
	public ResponseEntity<RespuestaApi> getInitPointMedico(@PathVariable long idUsuario) {
		try {
			return new ResponseEntity<RespuestaApi>(mercadoPagoServicio.getInitPoint(idUsuario, TipoUsuario.MEDICO), HttpStatus.OK);
		} catch (BadCredentialsException | MPException e) {
			RespuestaApi respuestaApi = new RespuestaApi(e.getMessage());
			return new ResponseEntity<RespuestaApi>(respuestaApi, HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/init_point/paciente/{idUsuario}")
	public ResponseEntity<RespuestaApi> getInitPointPaciente(@PathVariable long idUsuario) {
		try {
			return new ResponseEntity<RespuestaApi>(mercadoPagoServicio.getInitPoint(idUsuario, TipoUsuario.PACIENTE), HttpStatus.OK);
		} catch (BadCredentialsException | MPException e) {
			RespuestaApi respuestaApi = new RespuestaApi(e.getMessage());
			return new ResponseEntity<RespuestaApi>(respuestaApi, HttpStatus.BAD_REQUEST);
		}
	}

	/*@GetMapping("/notificacion/{idUsuario}")
	public ResponseEntity<RespuestaApi> getInitPointPaciente(@PathVariable long idUsuario) {
		try {
			return new ResponseEntity<RespuestaApi>(mercadoPagoServicio.getInitPoint(idUsuario, TipoUsuario.PACIENTE), HttpStatus.OK);
		} catch (BadCredentialsException | MPException | PersistenciaException e) {
			RespuestaApi respuestaApi = new RespuestaApi(e.getMessage());
			return new ResponseEntity<RespuestaApi>(respuestaApi, HttpStatus.BAD_REQUEST);
		}
	}*/
}