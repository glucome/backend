package com.glucome.controladores;

import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.MercadoPagoIPNServicio;
import com.mercadopago.exceptions.MPException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mercadopago_ipn")
@CrossOrigin("*")
public class MercadoPagoIPNControlador {

	@Autowired
	private MercadoPagoIPNServicio mercadoPagoIPNServicio;

	@PostMapping("")
	public HttpStatus ipn(@RequestParam String topic, @RequestParam String id) throws MPException, PersistenciaException, APIException, PacienteException, ParametrosNoValidosException {
		return mercadoPagoIPNServicio.ipn(topic, id);
	}
}