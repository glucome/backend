package com.glucome.controladores;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.glucome.api.request.ListadoItemHistoriaRequest;
import com.glucome.servicios.PDFServicio;
import com.google.zxing.WriterException;

@Controller
public class PDFControlador {

	@Autowired
	PDFServicio pdfServicio;

	@RequestMapping(value = "/api/v1/descargar_historia_clinica/{idMedico}/{idPaciente}/", method = RequestMethod.GET)
	public void exportToPDF(final HttpServletRequest request, final HttpServletResponse response, @PathVariable long idMedico, @PathVariable long idPaciente, @RequestParam(required = false) String fechaDesde, @RequestParam(required = false) String fechaHasta, @RequestParam(required = false) String descripcion) {

		ListadoItemHistoriaRequest filtro = new ListadoItemHistoriaRequest();
		filtro.setDescripcion(descripcion);
		filtro.setFechaDesde(fechaDesde);
		filtro.setFechaHasta(fechaHasta);
		filtro.setMedico(idMedico);

		try {
			pdfServicio.descargarPDFHistoriaClinica(request, response, idMedico, idPaciente, filtro);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}

	@RequestMapping(value = "/api/v1/descargar_tarjeta_aumentada/{idPaciente}/", method = RequestMethod.GET)
	public void exportarTarjetaAumentada(final HttpServletRequest request, final HttpServletResponse response, @PathVariable long idPaciente) {
		try {
			pdfServicio.descargarTarjetaAumentada(request, response, idPaciente);
		} catch (IOException | WriterException e) {
			e.printStackTrace();
			return;
		}
	}

}
