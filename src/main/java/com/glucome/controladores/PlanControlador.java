package com.glucome.controladores;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.SuscripcionPlanRequest;
import com.glucome.enumerador.TipoUsuario;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.PlanServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLIntegrityConstraintViolationException;

@Controller
@RequestMapping("/api/v1/suscripcion")
public class PlanControlador {

	@Autowired
	PlanServicio planServicio;

	@GetMapping("/medico/")
	public ResponseEntity<RespuestaApi> getPlanesMedico() throws PersistenciaException, SQLIntegrityConstraintViolationException {
		return new ResponseEntity<RespuestaApi>(planServicio.getPlanes(TipoUsuario.MEDICO), HttpStatus.OK);
	}

	@GetMapping("/paciente/")
	public ResponseEntity<RespuestaApi> getPlanesPaciente() throws PersistenciaException, SQLIntegrityConstraintViolationException {
		return new ResponseEntity<RespuestaApi>(planServicio.getPlanes(TipoUsuario.PACIENTE), HttpStatus.OK);
	}

	@PostMapping("/medico/{idMedico}/")
	public ResponseEntity<RespuestaApi> setPlanMedico(@RequestBody SuscripcionPlanRequest data, @PathVariable long idMedico) throws PersistenciaException, SQLIntegrityConstraintViolationException {
		try {
			return new ResponseEntity<RespuestaApi>(planServicio.setSuscripcionMedico(data, idMedico), HttpStatus.OK);
		} catch (APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/paciente/{idPaciente}/")
	public ResponseEntity<RespuestaApi> setPlanPaciente(@RequestBody SuscripcionPlanRequest data, @PathVariable long idPaciente) throws PersistenciaException, SQLIntegrityConstraintViolationException {
		try {
			return new ResponseEntity<RespuestaApi>(planServicio.setSuscripcionPaciente(data, idPaciente), HttpStatus.OK);
		} catch (APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

}
