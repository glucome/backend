package com.glucome.controladores;

import com.glucome.api.RespuestaApi;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.RecomendadorServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLIntegrityConstraintViolationException;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin("*")
public class RecomendadorControlador {

	@Autowired
	private RecomendadorServicio recomendadorServicio;

	@GetMapping("/recomendador/{idPaciente}/")
	public ResponseEntity<RespuestaApi> recomendador(@PathVariable long idPaciente) throws PersistenciaException, SQLIntegrityConstraintViolationException {
		try {
			return new ResponseEntity<RespuestaApi>(recomendadorServicio.getRecomendacion(idPaciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			RespuestaApi respuestaApi = new RespuestaApi("Error generar la recomendación de comidas, revise que el paciente sea válido");
			return new ResponseEntity<RespuestaApi>(respuestaApi, HttpStatus.BAD_REQUEST);
		} catch (APIException e) {
			RespuestaApi respuestaApi = new RespuestaApi(e.getMessage());
			return new ResponseEntity<RespuestaApi>(respuestaApi, HttpStatus.BAD_REQUEST);
		}

	}

}
