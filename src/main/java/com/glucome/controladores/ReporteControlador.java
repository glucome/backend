package com.glucome.controladores;

import com.glucome.api.RespuestaApi;
import com.glucome.servicios.ReporteServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/reporte")
@CrossOrigin("*")
public class ReporteControlador {

	@Autowired
	private ReporteServicio reporteServicio;

	@GetMapping("/cantidad_mediciones/")
	public ResponseEntity<RespuestaApi> reporteCantidadMediciones() {
		return new ResponseEntity<RespuestaApi>(reporteServicio.getReporteCantidadMediciones(), HttpStatus.OK);
	}

	@GetMapping("/por_momento/")
	public ResponseEntity<RespuestaApi> reportePorMomento() { return new ResponseEntity<RespuestaApi>(reporteServicio.getReportePorMomento(), HttpStatus.OK); }

	@GetMapping("/por_rango_edad/")
	public ResponseEntity<RespuestaApi> reportePorEdad() {
		return new ResponseEntity<RespuestaApi>(reporteServicio.getReportePorRangoEdad(), HttpStatus.OK);
	}

	@GetMapping("/por_tipo_diabetes/")
	public ResponseEntity<RespuestaApi> reportePorTipoDiabetes() {
		return new ResponseEntity<RespuestaApi>(reporteServicio.getReportePorTipoDiabetes(), HttpStatus.OK);
	}

	@GetMapping("/por_tipo_medicion/")
	public ResponseEntity<RespuestaApi> reportePorTipoMedicion() { return new ResponseEntity<RespuestaApi>(reporteServicio.getReportePorTipoMedicion(), HttpStatus.OK); }

	@GetMapping("/ranking_alimentos/")
	public ResponseEntity<RespuestaApi> reporteRankingAlimentos() { return new ResponseEntity<RespuestaApi>(reporteServicio.getReporteRankingAlimentos(), HttpStatus.OK); }
}
