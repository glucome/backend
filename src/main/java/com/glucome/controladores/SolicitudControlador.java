package com.glucome.controladores;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.SolicitudRequest;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.SolicitudServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/solicitud")
@CrossOrigin("*")
public class SolicitudControlador {

	@Autowired
	private SolicitudServicio solicitudServicio;

	@GetMapping("/{idSolicitante}/{idSolicitado}/")
	private ResponseEntity<RespuestaApi> getSolicitud(@PathVariable long idSolicitante, @PathVariable long idSolicitado, @RequestParam(required = true) int tipoSolicitud) {
		try {
			return new ResponseEntity<RespuestaApi>(solicitudServicio.getSolicitud(idSolicitante, idSolicitado, tipoSolicitud), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/")
	private ResponseEntity<RespuestaApi> agregarSolicitud(@RequestBody SolicitudRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(solicitudServicio.agregarSolicitud(data), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/allegado/")
	private ResponseEntity<RespuestaApi> agregarSolicitudAllegado(@RequestBody SolicitudRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(solicitudServicio.agregarSolicitudAllegado(data), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/{id}/")
	private ResponseEntity<RespuestaApi> aceptarSolicitud(@PathVariable long id) {
		try {
			return new ResponseEntity<RespuestaApi>(solicitudServicio.aceptarSolicitud(id), HttpStatus.OK);
		} catch (PersistenciaException | APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/{id}/")
	private ResponseEntity<RespuestaApi> rechazarSolicitud(@PathVariable long id) {
		try {
			return new ResponseEntity<RespuestaApi>(solicitudServicio.rechazarSolicitud(id), HttpStatus.OK);
		} catch (PersistenciaException | APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

}
