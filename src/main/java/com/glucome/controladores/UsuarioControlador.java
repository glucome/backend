package com.glucome.controladores;

import com.glucome.api.RespuestaApi;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.UsuarioServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/usuario")
@CrossOrigin("*")
public class UsuarioControlador {

    @Autowired
    UsuarioServicio usuarioServicio;

    @GetMapping("/{idUsuario}/roles/")
    public ResponseEntity<RespuestaApi> getRoles(@PathVariable long idUsuario) {
        try {
            return new ResponseEntity<RespuestaApi>(usuarioServicio.getRoles(idUsuario), HttpStatus.OK);
        } catch (PersistenciaException e) {
            RespuestaApi respuestaApi = new RespuestaApi(e.getMessage());
            return new ResponseEntity<RespuestaApi>(respuestaApi, HttpStatus.BAD_REQUEST);
        }
    }
}
