package com.glucome.controladores;

import com.glucome.api.RespuestaApi;
import com.glucome.servicios.UsuarioServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin("*")
public class VerificacionUsuarioControlador {
	@Autowired
	public UsuarioServicio usuarioServicio;
	
	@GetMapping("/existe_mail/{mail}/")
	public RespuestaApi existeMail(@PathVariable String mail) {
		return usuarioServicio.existeMail(mail);
	}

	@GetMapping("/existe_nombre_usuario/")
	public RespuestaApi existeNombreUsuario(@RequestParam String nombreUsuario) {
		return usuarioServicio.existeNombreUsuario(nombreUsuario);
	}

}
