package com.glucome.controladores.allegado;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.RegistroAllegadoRequest;
import com.glucome.enumerador.TipoSolicitud;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.AllegadoServicio;
import com.glucome.servicios.PacienteServicio;
import com.glucome.servicios.SolicitudServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1/allegado")
@CrossOrigin("*")
public class AllegadoControlador {

	@Autowired
	AllegadoServicio allegadoServicio;

	@Autowired
	SolicitudServicio solicitudServicio;

	@Autowired
	PacienteServicio pacienteServicio;

	@PostMapping("/")
	private ResponseEntity<RespuestaApi> registrarAllegado(@RequestBody RegistroAllegadoRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(allegadoServicio.registrar(data), HttpStatus.OK);
		} catch (PacienteException | ParametrosNoValidosException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		} catch (IOException e) {
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("Ha ocurrido un error al subir el archivo"), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/")
	private ResponseEntity<RespuestaApi> listadoMedico(@RequestParam(required = false) String nombre) {
		try {
			return new ResponseEntity<RespuestaApi>(allegadoServicio.get(nombre), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idAllegado}/solicitudes/")
	private ResponseEntity<RespuestaApi> getSolicitud(@PathVariable long idAllegado) {
		try {
			return new ResponseEntity<RespuestaApi>(solicitudServicio.getListado(idAllegado, TipoSolicitud.ALLEGADO_PACIENTE), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idAllegado}/pacientes/")
	private ResponseEntity<RespuestaApi> getPacientes(@PathVariable long idAllegado) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.getListadoDelAllegado(idAllegado), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

}
