package com.glucome.controladores.dev;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.glucome.dao.AlimentoDAO;
import com.glucome.dao.BaseDAO;
import com.glucome.dao.ChatDAO;
import com.glucome.dao.MedicamentoDAO;
import com.glucome.dao.PacienteDAO;
import com.glucome.dao.UserRepository;
import com.glucome.enumerador.EstadoSolicitud;
import com.glucome.enumerador.MomentoAlimento;
import com.glucome.enumerador.MomentoMedicion;
import com.glucome.enumerador.Plan;
import com.glucome.enumerador.Rol;
import com.glucome.enumerador.Sexo;
import com.glucome.enumerador.TipoDiabetes;
import com.glucome.enumerador.TipoEjercicio;
import com.glucome.enumerador.TipoInsulina;
import com.glucome.enumerador.TipoMedicion;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Alimento;
import com.glucome.modelo.AlimentoIngerido;
import com.glucome.modelo.Allegado;
import com.glucome.modelo.Ejercicio;
import com.glucome.modelo.InsulinaInyectada;
import com.glucome.modelo.Medicamento;
import com.glucome.modelo.MedicamentoIngerido;
import com.glucome.modelo.Medicion;
import com.glucome.modelo.Medico;
import com.glucome.modelo.Paciente;
import com.glucome.modelo.SolicitudVinculacionAllegadoPaciente;
import com.glucome.modelo.SolicitudVinculacionMedicoPaciente;
import com.glucome.modelo.Usuario;

@RestController
@CrossOrigin("*")
public class Inicializar {

	private static final int CANTIDAD_PACIENTES = 20;

	private static final int CANTIDAD_MEDICOS = 10;

	private static final int CANTIDAD_ALLEGADOS = 50;

	private static final int CANTIDAD_MEDICIONES = 50;

	private static final int CANTIDAD_EJERCICIOS = 10;

	private static final int CANTIDAD_ALIMENTOS = 20;

	private static final int CANTIDAD_MEDICAMENTOS = 10;

	private static final int CANTIDAD_INSULINA = 10;

	private static final int CANTIDAD_MEDICOS_POR_PACIENTE = 1;

	private static final int CANTIDAD_ALLEGADOS_PACIENTE = 5;

	private static final String ARCHIVO_MEDICIONES_REALES = "mediciones/mediciones";

	@Autowired
	UserRepository users;

	@Autowired
	PacienteDAO pacienteDAO;

	@Autowired
	BaseDAO baseDAO;

	@Autowired
	ChatDAO chatDAO;

	@Autowired
	AlimentoDAO alimentoDAO;

	@Autowired
	MedicamentoDAO medicamentoDAO;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Value("${file.inicializacion}")
	private String rutaArchivos;

	private static String ARCHIVO_MEDICAMENTOS = "entidades.csv";

	private static String ARCHIVO_ALIMENTOS = "alimentos.csv";

	private static String ARCHIVO_ALIMENTOS_MOMENTOS = "alimento_momento.csv";

	private List<Paciente> pacientes = new ArrayList<>();

	private List<Medico> medicos = new ArrayList<>();

	private List<Allegado> allegados = new ArrayList<>();

	@GetMapping("/api/v1/inicializar_prod/")
	public String inicializarProd() throws PersistenciaException {

		// USUARIOS
		Usuario martin = new Usuario();
		martin.setNombreUsuario("martime");
		martin.setClave(this.passwordEncoder.encode("martime"));
		martin.setRoles(Arrays.asList(Rol.PACIENTE_PREMIUM));
		martin.setPlan(Plan.PACIENTE_PREMIUM);
		martin.setMail("martin.artime@gmail.com");
		this.users.save(martin);

		Paciente martinPaciente = new Paciente();
		martinPaciente.setAltura(1.66);
		martinPaciente.setFechaNacimiento(LocalDate.of(1994, 1, 28));
		martinPaciente.setNombre("Martin Artime");
		martinPaciente.setPeso(68);
		martinPaciente.setSexo(Sexo.MASCULINO);
		martinPaciente.setTipoDiabetes(TipoDiabetes.TIPO_1);
		martinPaciente.setUsuario(martin);
		pacienteDAO.grabar(martinPaciente);
		this.pacientes.add(martinPaciente);

		Usuario pablo = new Usuario();
		pablo.setNombreUsuario("pbecerra");
		pablo.setClave(this.passwordEncoder.encode("pbecerra"));
		pablo.setRoles(Arrays.asList(Rol.MEDICO_PREMIUM));
		pablo.setPlan(Plan.MEDICO_PREMIUM);
		pablo.setMail("pablo.d.becerra94@gmail.com");
		this.users.save(pablo);

		Medico pabloMedico = new Medico();
		pabloMedico.setMatricula("MN 19050403");
		pabloMedico.setNombre("Pablo Becerra");
		pabloMedico.setUsuario(pablo);
		pacienteDAO.grabar(pabloMedico);
		this.medicos.add(pabloMedico);

		Usuario leonel = new Usuario();
		leonel.setNombreUsuario("lmenendez");
		leonel.setClave(this.passwordEncoder.encode("lmenendez"));
		leonel.setRoles(Arrays.asList(Rol.ALLEGADO));
		leonel.setPlan(Plan.ALLEGADO_BASICO);
		leonel.setMail("leo.dr@live.com.ar");
		this.users.save(leonel);

		Allegado leonelAllegado = new Allegado();
		leonelAllegado.setNombre("Leonel Menendez");
		leonelAllegado.setUsuario(leonel);
		pacienteDAO.grabar(leonelAllegado);
		this.allegados.add(leonelAllegado);

		Usuario claudio = new Usuario();
		claudio.setNombreUsuario("csaccella");
		claudio.setClave(this.passwordEncoder.encode("csaccella"));
		claudio.setRoles(Arrays.asList(Rol.MEDICO_PREMIUM));
		claudio.setPlan(Plan.MEDICO_PREMIUM);
		claudio.setMail("cdsaccella@gmail.com");
		this.users.save(claudio);

		Medico claudioMedico = new Medico();
		claudioMedico.setMatricula("MN 39214529");
		claudioMedico.setNombre("Claudio Saccella");
		claudioMedico.setUsuario(claudio);
		pacienteDAO.grabar(claudioMedico);
		this.medicos.add(claudioMedico);

		Usuario lautaro = new Usuario();
		lautaro.setNombreUsuario("lscarpione");
		lautaro.setClave(this.passwordEncoder.encode("lscarpione"));
		lautaro.setRoles(Arrays.asList(Rol.PACIENTE_PREMIUM));
		lautaro.setPlan(Plan.PACIENTE_PREMIUM);
		lautaro.setMail("lautaro.scarpione@gmail.com");
		this.users.save(lautaro);

		Paciente lautaroPaciente = new Paciente();
		lautaroPaciente.setAltura(1.72);
		lautaroPaciente.setFechaNacimiento(LocalDate.of(1994, 8, 22));
		lautaroPaciente.setNombre("Lautaro Scarpione");
		lautaroPaciente.setPeso(76);
		lautaroPaciente.setSexo(Sexo.MASCULINO);
		lautaroPaciente.setTipoDiabetes(TipoDiabetes.TIPO_2);
		lautaroPaciente.setUsuario(lautaro);
		pacienteDAO.grabar(lautaroPaciente);
		this.pacientes.add(lautaroPaciente);

		Usuario hernan = new Usuario();
		hernan.setNombreUsuario("hruttimann");
		hernan.setClave(this.passwordEncoder.encode("hruttimann"));
		hernan.setRoles(Arrays.asList(Rol.PACIENTE_PREMIUM));
		hernan.setPlan(Plan.PACIENTE_PREMIUM);
		hernan.setMail("ruttimannh@gmail.com");
		this.users.save(hernan);

		Paciente hernanPaciente = new Paciente();
		hernanPaciente.setAltura(1.79);
		hernanPaciente.setFechaNacimiento(LocalDate.of(1994, 10, 31));
		hernanPaciente.setNombre("Hernan Ruttimann");
		hernanPaciente.setPeso(70);
		hernanPaciente.setSexo(Sexo.MASCULINO);
		hernanPaciente.setTipoDiabetes(TipoDiabetes.TIPO_1);
		hernanPaciente.setUsuario(hernan);
		pacienteDAO.grabar(hernanPaciente);
		this.pacientes.add(hernanPaciente);

		Usuario nicolas = new Usuario();
		nicolas.setNombreUsuario("nvespoli");
		nicolas.setClave(this.passwordEncoder.encode("nvespoli"));
		nicolas.setRoles(Arrays.asList(Rol.PACIENTE));
		nicolas.setPlan(Plan.PACIENTE_BASICO);
		nicolas.setMail("vespoli.nicolas94@gmail.com");
		this.users.save(nicolas);

		Paciente nicolasPaciente = new Paciente();
		nicolasPaciente.setAltura(1.79);
		nicolasPaciente.setFechaNacimiento(LocalDate.of(1994, 7, 6));
		nicolasPaciente.setNombre("Nicolas Vespoli");
		nicolasPaciente.setPeso(68);
		nicolasPaciente.setSexo(Sexo.MASCULINO);
		nicolasPaciente.setTipoDiabetes(TipoDiabetes.TIPO_2);
		nicolasPaciente.setUsuario(nicolas);
		pacienteDAO.grabar(nicolasPaciente);
		this.pacientes.add(nicolasPaciente);

		// VINCULACIONES PACIENTE/ALLEGADO
		SolicitudVinculacionAllegadoPaciente solicitud1 = new SolicitudVinculacionAllegadoPaciente();
		solicitud1.setEstado(EstadoSolicitud.ACEPTADA);
		solicitud1.setAllegado(leonelAllegado);
		solicitud1.setPaciente(martinPaciente);
		solicitud1.aceptarSolicitud();
		pacienteDAO.grabar(solicitud1);
		pacienteDAO.grabar(martinPaciente);

		SolicitudVinculacionAllegadoPaciente solicitud2 = new SolicitudVinculacionAllegadoPaciente();
		solicitud2.setEstado(EstadoSolicitud.ACEPTADA);
		solicitud2.setAllegado(leonelAllegado);
		solicitud2.setPaciente(lautaroPaciente);
		solicitud2.aceptarSolicitud();
		pacienteDAO.grabar(solicitud2);
		pacienteDAO.grabar(lautaroPaciente);

		SolicitudVinculacionAllegadoPaciente solicitud3 = new SolicitudVinculacionAllegadoPaciente();
		solicitud3.setEstado(EstadoSolicitud.ACEPTADA);
		solicitud3.setAllegado(leonelAllegado);
		solicitud3.setPaciente(hernanPaciente);
		solicitud3.aceptarSolicitud();
		pacienteDAO.grabar(solicitud3);
		pacienteDAO.grabar(hernanPaciente);

		// VINCULACIONES PACIENTE/MEDICO
		SolicitudVinculacionMedicoPaciente solicitud4 = new SolicitudVinculacionMedicoPaciente();
		solicitud4.setEstado(EstadoSolicitud.ACEPTADA);
		solicitud4.setMedico(pabloMedico);
		solicitud4.setPaciente(lautaroPaciente);
		solicitud4.aceptarSolicitud();
		pacienteDAO.grabar(solicitud4);
		pacienteDAO.grabar(lautaroPaciente);
		chatDAO.crearChatMedicoPaciente(pabloMedico, lautaroPaciente);

		SolicitudVinculacionMedicoPaciente solicitud5 = new SolicitudVinculacionMedicoPaciente();
		solicitud5.setEstado(EstadoSolicitud.ACEPTADA);
		solicitud5.setMedico(claudioMedico);
		solicitud5.setPaciente(martinPaciente);
		solicitud5.aceptarSolicitud();
		pacienteDAO.grabar(solicitud5);
		pacienteDAO.grabar(martinPaciente);
		chatDAO.crearChatMedicoPaciente(claudioMedico, martinPaciente);

		SolicitudVinculacionMedicoPaciente solicitud6 = new SolicitudVinculacionMedicoPaciente();
		solicitud6.setEstado(EstadoSolicitud.ACEPTADA);
		solicitud6.setMedico(pabloMedico);
		solicitud6.setPaciente(hernanPaciente);
		solicitud6.aceptarSolicitud();
		pacienteDAO.grabar(solicitud6);
		pacienteDAO.grabar(hernanPaciente);
		chatDAO.crearChatMedicoPaciente(pabloMedico, hernanPaciente);

		// VINCULACIONES PACIENTE/MEDICO
		SolicitudVinculacionMedicoPaciente solicitud7 = new SolicitudVinculacionMedicoPaciente();
		solicitud7.setEstado(EstadoSolicitud.ACEPTADA);
		solicitud7.setMedico(pabloMedico);
		solicitud7.setPaciente(martinPaciente);
		solicitud7.aceptarSolicitud();
		pacienteDAO.grabar(solicitud7);
		pacienteDAO.grabar(martinPaciente);
		chatDAO.crearChatMedicoPaciente(pabloMedico, martinPaciente);

		SolicitudVinculacionMedicoPaciente solicitud8 = new SolicitudVinculacionMedicoPaciente();
		solicitud8.setEstado(EstadoSolicitud.ACEPTADA);
		solicitud8.setMedico(pabloMedico);
		solicitud8.setPaciente(nicolasPaciente);
		solicitud8.aceptarSolicitud();
		pacienteDAO.grabar(solicitud8);
		pacienteDAO.grabar(nicolasPaciente);
		chatDAO.crearChatMedicoPaciente(pabloMedico, nicolasPaciente);

		// ALIMENTOS
		cargarArchivosAlimentos();

		// MEDICAMENTOS
		cargarArchivoMedicamentos();

		// MEDICIONES
		cargarMedicionesReal(martinPaciente, 1);
		cargarMedicionesReal(lautaroPaciente, 2);
		cargarMedicionesReal(hernanPaciente, 3);
		cargarMedicionesReal(nicolasPaciente, 4);

		Random random = new Random();
		int tiempoEjercicioMinimo = 15;
		int tiempoEjercicioMaximo = 120;

		int unidadesMinimo = 1;
		int unidadesMaximo = 10;

		List<Alimento> alimentos = alimentoDAO.getListado("");
		List<Medicamento> medicamentos = medicamentoDAO.getListado("Metformina", 0);

		// DATOS DE MARTIN
		cargarEjerciciosPaciente(random, tiempoEjercicioMinimo, tiempoEjercicioMaximo, martinPaciente);
		cargarAlimentosPaciente(random, unidadesMinimo, unidadesMaximo, alimentos, martinPaciente);
		cargarMedicamentosPaciente(random, unidadesMinimo, unidadesMaximo, medicamentos, martinPaciente);
		cargarInsulinaPaciente(random, unidadesMinimo, unidadesMaximo, martinPaciente);

		// DATOS DE LAUTARO
		cargarEjerciciosPaciente(random, tiempoEjercicioMinimo, tiempoEjercicioMaximo, lautaroPaciente);
		cargarAlimentosPaciente(random, unidadesMinimo, unidadesMaximo, alimentos, lautaroPaciente);
		cargarMedicamentosPaciente(random, unidadesMinimo, unidadesMaximo, medicamentos, lautaroPaciente);
		cargarInsulinaPaciente(random, unidadesMinimo, unidadesMaximo, lautaroPaciente);

		// DATOS DE HERNAN
		cargarEjerciciosPaciente(random, tiempoEjercicioMinimo, tiempoEjercicioMaximo, hernanPaciente);
		cargarAlimentosPaciente(random, unidadesMinimo, unidadesMaximo, alimentos, hernanPaciente);
		cargarMedicamentosPaciente(random, unidadesMinimo, unidadesMaximo, medicamentos, hernanPaciente);
		cargarInsulinaPaciente(random, unidadesMinimo, unidadesMaximo, hernanPaciente);

		// DATOS DE RELLENO
		int medicionMinima = 50;
		int medicionMaxima = 250;
		Sexo sexo = null;
		String nombre;
		for (int i = 0; i < CANTIDAD_PACIENTES; i++) {
			if (i % 2 == 0) {
				sexo = Sexo.MASCULINO;
				nombre = generarNombresAleatoriosMasculino();
			} else {
				sexo = Sexo.FEMENINO;
				nombre = generarNombresAleatoriosFemenino();
			}
			Usuario user = new Usuario();

			String nombreUsuario = nombre.toLowerCase().replace(" ", "_") + i;
			user.setNombreUsuario(nombreUsuario);
			user.setClave(this.passwordEncoder.encode(nombreUsuario));
			user.setRoles(Arrays.asList(i % 2 == 0 ? Rol.PACIENTE : Rol.PACIENTE_PREMIUM));
			user.setPlan(i % 2 == 0 ? Plan.PACIENTE_BASICO : Plan.PACIENTE_PREMIUM);
			user.setMail(nombre + "-" + i + "@gmail.com");
			this.users.save(user);

			Paciente paciente = new Paciente();
			paciente.setAltura(1.72);
			paciente.setFechaNacimiento(LocalDate.now().minusYears(40));
			paciente.setNombre(nombre);
			paciente.setPeso(89.3);
			paciente.setSexo(sexo);
			paciente.setTipoDiabetes(i % 5 == 0 ? TipoDiabetes.TIPO_2 : TipoDiabetes.TIPO_1);
			paciente.setUsuario(user);
			pacienteDAO.grabar(paciente);
			this.pacientes.add(paciente);

			cargarMedicionPaciente(random, medicionMinima, medicionMaxima, i, paciente);
			cargarEjerciciosPaciente(random, tiempoEjercicioMinimo, tiempoEjercicioMaximo, paciente);
			cargarAlimentosPaciente(random, unidadesMinimo, unidadesMaximo, alimentos, paciente);
			cargarMedicamentosPaciente(random, unidadesMinimo, unidadesMaximo, medicamentos, paciente);
			cargarInsulinaPaciente(random, unidadesMinimo, unidadesMaximo, paciente);
			cargarMedicosDelPaciente(random, i, paciente);
		}

		return "Fin inicialización de producción";

	}

	@GetMapping("/api/v1/inicializar/")
	public String inicializar() throws PersistenciaException {

		Usuario user = new Usuario();
		user.setNombreUsuario("paciente");
		user.setClave(this.passwordEncoder.encode("paciente"));
		user.setRoles(Arrays.asList(Rol.PACIENTE));
		user.setPlan(Plan.PACIENTE_BASICO);
		user.setMail("paciente@glucome.com.ar");
		this.users.save(user);

		Usuario admin = new Usuario();
		admin.setNombreUsuario("admin");
		admin.setClave(this.passwordEncoder.encode("admin"));
		admin.setRoles(Arrays.asList(Rol.PACIENTE));
		admin.setPlan(Plan.PACIENTE_BASICO);
		admin.setMail("admin@glucome.com.ar");
		this.users.save(admin);

		Usuario medicoUser = new Usuario();
		medicoUser.setNombreUsuario("medico");
		medicoUser.setPlan(Plan.MEDICO_BASICO);
		medicoUser.setClave(this.passwordEncoder.encode("medico"));
		medicoUser.setRoles(Arrays.asList(Rol.MEDICO));
		medicoUser.setMail("medico@glucome.com.ar");
		this.users.save(medicoUser);

		Usuario allegadoUser = new Usuario();
		allegadoUser.setNombreUsuario("allegado");
		allegadoUser.setPlan(Plan.ALLEGADO_BASICO);
		allegadoUser.setClave(this.passwordEncoder.encode("allegado"));
		allegadoUser.setRoles(Arrays.asList(Rol.ALLEGADO));
		allegadoUser.setMail("allegado@glucome.com.ar");
		this.users.save(allegadoUser);

		Paciente paciente = new Paciente();
		paciente.setAltura(1.72);
		paciente.setFechaNacimiento(LocalDate.now());
		paciente.setNombre("Paciente");
		paciente.setPeso(89.3);
		paciente.setSexo(Sexo.MASCULINO);
		paciente.setTipoDiabetes(TipoDiabetes.TIPO_2);
		paciente.setUsuario(user);
		pacienteDAO.grabar(paciente);
		this.pacientes.add(paciente);

		paciente = new Paciente();
		paciente.setAltura(1.72);
		paciente.setFechaNacimiento(LocalDate.now());
		paciente.setNombre("Admin");
		paciente.setPeso(89.3);
		paciente.setSexo(Sexo.MASCULINO);
		paciente.setTipoDiabetes(TipoDiabetes.TIPO_2);
		paciente.setUsuario(admin);
		pacienteDAO.grabar(paciente);
		this.pacientes.add(paciente);

		Medico medico = new Medico();
		medico.setMatricula("MATRICULA");
		medico.setNombre("Juan Perez");
		medico.setUsuario(medicoUser);
		pacienteDAO.grabar(medico);
		this.medicos.add(medico);

		Allegado allegado = new Allegado();
		allegado.setNombre("Allegado");
		allegado.setUsuario(allegadoUser);
		pacienteDAO.grabar(allegado);
		this.allegados.add(allegado);

		cargarArchivosAlimentos();

		cargarArchivoMedicamentos();

		return "Fin de inicialización";

	}

	@GetMapping("/api/v1/poblar/")
	public String poblar() throws PersistenciaException {

		cargarMedicos();
		cargarAllegados();
		cargarPacientes();

		return "Fin de poblado de datos";

	}

	@GetMapping("/api/v1/poblar_real/{idPaciente}/{numeroArchivo}")
	public String poblarReal(@PathVariable long idPaciente, @PathVariable int numeroArchivo) throws PersistenciaException {

		Paciente paciente = pacienteDAO.get(idPaciente);
		cargarMedicionesReal(paciente, numeroArchivo);

		return "Fin de poblado de datos";

	}

	private void cargarAllegados() throws PersistenciaException {
		for (int i = 0; i < CANTIDAD_ALLEGADOS; i++) {
			Usuario user = new Usuario();
			user.setNombreUsuario("allegado-" + i);
			user.setClave(this.passwordEncoder.encode("allegado-" + i));
			user.setRoles(Arrays.asList(Rol.ALLEGADO));
			user.setPlan(Plan.ALLEGADO_BASICO);
			user.setMail("allegado-" + i + "@glucome.com.ar");
			this.users.save(user);

			Allegado allegado = new Allegado();
			allegado.setNombre("Allegado " + i);
			allegado.setUsuario(user);
			pacienteDAO.grabar(allegado);
			this.allegados.add(allegado);
		}

	}

	private void cargarMedicos() throws PersistenciaException {
		for (int i = 0; i < CANTIDAD_MEDICOS; i++) {
			Usuario user = new Usuario();
			user.setNombreUsuario("medico-" + i);
			user.setClave(this.passwordEncoder.encode("medico-" + i));
			user.setRoles(Arrays.asList(i % 2 == 0 ? Rol.MEDICO : Rol.MEDICO_PREMIUM));
			user.setPlan(i % 2 == 0 ? Plan.MEDICO_BASICO : Plan.MEDICO_PREMIUM);
			user.setMail("medico-" + i + "@glucome.com.ar");
			this.users.save(user);

			Medico medico = new Medico();
			medico.setMatricula("MATRICULA-" + i);
			medico.setNombre("Médico " + i);
			medico.setUsuario(user);
			pacienteDAO.grabar(medico);
			this.medicos.add(medico);
		}

	}

	private void cargarPacientes() throws PersistenciaException {

		Random random = new Random();
		int medicionMinima = 50;
		int medicionMaxima = 250;

		int tiempoEjercicioMinimo = 15;
		int tiempoEjercicioMaximo = 120;

		int unidadesMinimo = 1;
		int unidadesMaximo = 10;

		List<Alimento> alimentos = alimentoDAO.getListado("");
		List<Medicamento> medicamentos = medicamentoDAO.getListado("Metformina", 0);

		for (int i = 0; i < CANTIDAD_PACIENTES; i++) {
			Usuario user = new Usuario();
			user.setNombreUsuario("paciente-" + i);
			user.setClave(this.passwordEncoder.encode("paciente-" + i));
			user.setRoles(Arrays.asList(i % 2 == 0 ? Rol.PACIENTE : Rol.PACIENTE_PREMIUM));
			user.setPlan(i % 2 == 0 ? Plan.PACIENTE_BASICO : Plan.PACIENTE_PREMIUM);
			user.setMail("paciente-" + i + "@glucome.com.ar");
			this.users.save(user);

			Paciente paciente = new Paciente();
			paciente.setAltura(1.72);
			paciente.setFechaNacimiento(LocalDate.now());
			paciente.setNombre("Paciente " + i);
			paciente.setPeso(89.3);
			paciente.setSexo(i % 3 == 0 ? Sexo.MASCULINO : Sexo.FEMENINO);
			paciente.setTipoDiabetes(i % 5 == 0 ? TipoDiabetes.TIPO_2 : TipoDiabetes.TIPO_1);
			paciente.setUsuario(user);
			pacienteDAO.grabar(paciente);
			this.pacientes.add(paciente);

			cargarMedicionPaciente(random, medicionMinima, medicionMaxima, i, paciente);
			cargarEjerciciosPaciente(random, tiempoEjercicioMinimo, tiempoEjercicioMaximo, paciente);
			cargarAlimentosPaciente(random, unidadesMinimo, unidadesMaximo, alimentos, paciente);
			cargarMedicamentosPaciente(random, unidadesMinimo, unidadesMaximo, medicamentos, paciente);
			cargarInsulinaPaciente(random, unidadesMinimo, unidadesMaximo, paciente);
			cargarMedicosDelPaciente(random, i, paciente);
			cargarAllegadosDelPaciente(random, i, paciente);
		}
	}

	private void cargarAllegadosDelPaciente(Random random, int i, Paciente paciente) throws PersistenciaException {
		for (int j = 0; j < CANTIDAD_ALLEGADOS_PACIENTE; j++) {
			SolicitudVinculacionAllegadoPaciente solicitud = new SolicitudVinculacionAllegadoPaciente();
			solicitud.setEstado(EstadoSolicitud.ACEPTADA);
			solicitud.setAllegado(allegados.get((i + j) % allegados.size()));
			solicitud.setPaciente(paciente);
			solicitud.aceptarSolicitud();
			pacienteDAO.grabar(solicitud);
			pacienteDAO.grabar(paciente);
		}
	}

	private void cargarMedicosDelPaciente(Random random, int i, Paciente paciente) throws PersistenciaException {
		for (int j = 0; j < CANTIDAD_MEDICOS_POR_PACIENTE; j++) {
			SolicitudVinculacionMedicoPaciente solicitud = new SolicitudVinculacionMedicoPaciente();
			solicitud.setEstado(EstadoSolicitud.ACEPTADA);
			Medico medico = medicos.get((i + j) % medicos.size());
			solicitud.setMedico(medico);
			solicitud.setPaciente(paciente);
			solicitud.aceptarSolicitud();
			pacienteDAO.grabar(solicitud);
			pacienteDAO.grabar(paciente);
			chatDAO.crearChatMedicoPaciente(medico, paciente);
		}

	}

	private void cargarInsulinaPaciente(Random random, int unidadesMinimo, int unidadesMaximo, Paciente paciente) throws PersistenciaException {
		for (int j = 0; j < CANTIDAD_INSULINA; j++) {
			InsulinaInyectada insulina = new InsulinaInyectada();
			insulina.setFechaHora(LocalDateTime.now().minusDays(j).minusHours(j + 1));
			insulina.setTipo(TipoInsulina.getById(j % TipoInsulina.values().length));
			insulina.setUnidades(random.nextInt(unidadesMaximo - unidadesMinimo) + unidadesMinimo);
			insulina.setPaciente(paciente);
			pacienteDAO.grabar(insulina);
		}

	}

	private void cargarMedicamentosPaciente(Random random, int unidadesMinimo, int unidadesMaximo, List<Medicamento> medicamentos, Paciente paciente) throws PersistenciaException {

		Set<Medicamento> medicamentosRecetados = new HashSet<>();

		for (int j = 0; j < CANTIDAD_MEDICAMENTOS; j++) {
			MedicamentoIngerido medicamentoIngerido = new MedicamentoIngerido();
			medicamentoIngerido.setFechaHora(LocalDateTime.now().minusDays(j).minusHours(j + 2));
			medicamentoIngerido.setCantidadIngerida(random.nextInt(unidadesMaximo - unidadesMinimo) + unidadesMinimo);
			Medicamento medicamento = medicamentos.get(j % medicamentos.size());
			medicamentosRecetados.add(medicamento);
			medicamentoIngerido.setMedicamento(medicamento);
			medicamentoIngerido.setPaciente(paciente);
			pacienteDAO.grabar(medicamentoIngerido);
		}

		paciente.setMedicamentos(new ArrayList<Medicamento>(medicamentosRecetados));
		pacienteDAO.grabar(paciente);

	}

	private void cargarAlimentosPaciente(Random random, int unidadesMinimo, int unidadesMaximo, List<Alimento> alimentos, Paciente paciente) throws PersistenciaException {
		for (int j = 0; j < CANTIDAD_ALIMENTOS; j++) {
			AlimentoIngerido alimentoIngerido = new AlimentoIngerido();
			Alimento alimento = alimentos.get(j % alimentos.size());
			alimentoIngerido.setAlimento(alimento);
			alimentoIngerido.setFechaHora(LocalDateTime.now().minusDays(j).minusHours(j + 2));
			alimentoIngerido.setPorciones(random.nextInt(unidadesMaximo - unidadesMinimo) + unidadesMinimo);
			int size = alimento.getMomentos().size();
			alimentoIngerido.setMomento(size != 0 ? alimento.getMomentos().get(j % size) : MomentoAlimento.get(j % MomentoAlimento.values().length));
			alimentoIngerido.setPaciente(paciente);
			pacienteDAO.grabar(alimentoIngerido);
		}
	}

	private void cargarEjerciciosPaciente(Random random, int tiempoEjercicioMinimo, int tiempoEjercicioMaximo, Paciente paciente) throws PersistenciaException {
		for (int j = 0; j < CANTIDAD_EJERCICIOS; j++) {
			Ejercicio ejercicio = new Ejercicio();
			ejercicio.setFechaHora(LocalDateTime.now().minusDays(j).minusHours(j + 1));
			ejercicio.setMinutos(random.nextInt(tiempoEjercicioMaximo - tiempoEjercicioMinimo) + tiempoEjercicioMinimo);
			ejercicio.setObservacion(j % 4 == 0 ? "Correr en el Hospital Italiano de San Justo" : "Caminar");
			ejercicio.setPaciente(paciente);
			ejercicio.setTipo(j % 4 == 0 ? TipoEjercicio.CORRER : TipoEjercicio.CAMINAR);
			pacienteDAO.grabar(ejercicio);
		}
	}

	private void cargarMedicionPaciente(Random random, int medicionMinima, int medicionMaxima, int i, Paciente paciente) throws PersistenciaException {
		for (int j = 0; j < CANTIDAD_MEDICIONES; j++) {
			Medicion medicion = new Medicion();
			medicion.setFechaHora(LocalDateTime.now().minusDays(j).minusHours(j));
			medicion.setMomento(MomentoMedicion.getById(i % MomentoMedicion.values().length));
			medicion.setPaciente(paciente);
			medicion.setTipo(TipoMedicion.getById(j % TipoMedicion.values().length));
			medicion.setValor(random.nextInt(medicionMaxima - medicionMinima) + medicionMinima);
			pacienteDAO.grabar(medicion);
		}
	}

	private void cargarArchivosAlimentos() throws PersistenciaException {
		baseDAO.correrScript("LOAD DATA INFILE '" + rutaArchivos + ARCHIVO_ALIMENTOS + "' " + "INTO TABLE alimento CHARACTER SET UTF8 FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '" + System.lineSeparator() + "' IGNORE 1 ROWS " + "(@col1, @col2, @col3, @col4, @col5, @col6, @col7) "
				+ "SET id = @col1, nombre = @col2, unidad_referencia = @col3, carbohidratos_por_unidad = @col4, indice_glucemico = @col5, descripcion=@col6, imagen=@col7;");

		baseDAO.correrScript("LOAD DATA INFILE '" + rutaArchivos + ARCHIVO_ALIMENTOS_MOMENTOS + "' " + "INTO TABLE alimento_momento CHARACTER SET UTF8 FIELDS TERMINATED BY ',' LINES TERMINATED BY '" + System.lineSeparator() + "' IGNORE 1 ROWS " + "(@col1, @col2) " + "SET alimento_id = @col1, momento = @col2;");

	}

	private void cargarArchivoMedicamentos() throws PersistenciaException {
		baseDAO.correrScript("LOAD DATA INFILE '" + rutaArchivos + ARCHIVO_MEDICAMENTOS + "' " + "INTO TABLE medicamento CHARACTER SET UTF8 FIELDS TERMINATED BY ',' LINES TERMINATED BY '" + System.lineSeparator() + "' IGNORE 1 ROWS " + "(@col1, @col2, @col3, @col4, @col5, @col6, @col7, @col8, @col9) " + "SET droga = @col2, laboratorio = @col5, marca = @col3, presentacion = @col4;");
	}

	private void cargarMedicionesReal(Paciente paciente, int numeroArchivo) throws PersistenciaException {
		baseDAO.correrScript(
				"LOAD DATA INFILE '" + rutaArchivos + ARCHIVO_MEDICIONES_REALES + numeroArchivo + ".csv' " + "INTO TABLE medicion CHARACTER SET UTF8 FIELDS TERMINATED BY ';' LINES TERMINATED BY '" + System.lineSeparator() + "' IGNORE 1 ROWS " + "(@col1, @col2, @col3, @col4) " + "SET fecha_hora = @col1, momento = @col2, valor = @col3, tipo = @col4, paciente_id=" + paciente.getId() + ";");

	}

	public static String generarNombresAleatoriosMasculino() {

		String[] nombres = { "David", "Jose", "Roberto", "Edwin", "Alberto", "Marcelo", "Matias", "Oscar", "Sebastian", "Ricardo", "Carlos", "Salvador", "Diego", "Luis", "Cristian", "Hernán", "Martin", "Claudio", "Leonel", "Lautaro", "Ruben", "Nicolás", "Pablo", "Alan" };
		String[] apellidos = { "Gomez", "Guerrero", "Cardenas", "Cardiel", "Cardona", "Cardoso", "Cariaga", "Carillo", "Carion", "Castiyo", "Castorena", "Castro", "Grande", "Grangenal", "Grano", "Grasia", "Griego", "Grigalva" };

		return nombres[(int) (Math.floor(Math.random() * ((nombres.length - 1) - 0 + 1) + 0))] + " " + apellidos[(int) (Math.floor(Math.random() * ((apellidos.length - 1) - 0 + 1) + 0))];
	}

	public static String generarNombresAleatoriosFemenino() {

		String[] nombres = { "Andrea", "Agustina", "Marcela", "Lucía", "Candela", "Karina", "Julieta", "Julia", "Victoria", "Sofia", "Verónica", "Candelaria", "Florencia", "Canela", "Mariana", "Carina", "Analía", "Belén", "Carlota" };
		String[] apellidos = { "Gomez", "Guerrero", "Cardenas", "Cardiel", "Cardona", "Cardoso", "Cariaga", "Carillo", "Carion", "Castiyo", "Castorena", "Castro", "Grande", "Grangenal", "Grano", "Grasia", "Griego", "Grigalva" };

		return nombres[(int) (Math.floor(Math.random() * ((nombres.length - 1) - 0 + 1) + 0))] + " " + apellidos[(int) (Math.floor(Math.random() * ((apellidos.length - 1) - 0 + 1) + 0))];
	}
}
