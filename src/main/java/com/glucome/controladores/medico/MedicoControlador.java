package com.glucome.controladores.medico;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.RegistroMedicoRequest;
import com.glucome.api.request.UbicacionRequest;
import com.glucome.enumerador.TipoSolicitud;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.MedicoServicio;
import com.glucome.servicios.PacienteServicio;
import com.glucome.servicios.SolicitudServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/medico")
@CrossOrigin("*")
public class MedicoControlador {

	@Autowired
	MedicoServicio medicoServicio;

	@Autowired
	PacienteServicio pacienteServicio;

	@Autowired
	private SolicitudServicio solicitudServicio;

	@PostMapping("/")
	private ResponseEntity<RespuestaApi> registrarMedico(@RequestBody RegistroMedicoRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(medicoServicio.registrar(data), HttpStatus.OK);
		} catch (PacienteException | ParametrosNoValidosException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/")
	private ResponseEntity<RespuestaApi> listadoMedico(@RequestParam(required = false) String nombre) {
		try {
			return new ResponseEntity<RespuestaApi>(medicoServicio.get(nombre), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idMedico}/solicitudes/")
	private ResponseEntity<RespuestaApi> getSolicitud(@PathVariable long idMedico) {
		try {
			return new ResponseEntity<RespuestaApi>(solicitudServicio.getListado(idMedico, TipoSolicitud.MEDICO_PACIENTE), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idMedico}/pacientes/")
	private ResponseEntity<RespuestaApi> getPacientes(@PathVariable long idMedico) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.getListado(idMedico), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idMedico}/pacientes/log_mediciones/")
	private ResponseEntity<RespuestaApi> getLogMedicionesPacientes(@PathVariable long idMedico) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.getLogMediciones(idMedico), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/")
	private ResponseEntity<RespuestaApi> editarMedico(@RequestBody RegistroMedicoRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(medicoServicio.editarMedico(data), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("Error al editar el médico, intente nuevamente más tarde"), HttpStatus.BAD_REQUEST);
		} catch (APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idMedico}/ubicacion/")
	private ResponseEntity<RespuestaApi> getUbicacion(@PathVariable long idMedico) {
		try {
			return new ResponseEntity<RespuestaApi>(medicoServicio.getUbicacion(idMedico), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("Error al recuperar su ubicación, intente nuevamente más tarde"), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/{idMedico}/ubicacion/")
	private ResponseEntity<RespuestaApi> guardarUbicacion(@PathVariable long idMedico, @RequestBody UbicacionRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(medicoServicio.guardarUbicacion(idMedico, data.getUbicacion()), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("Error al guardar su ubicación, intente nuevamente más tarde"), HttpStatus.BAD_REQUEST);
		} catch (APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/{idMedico}/ubicacion/")
	private ResponseEntity<RespuestaApi> borrarUbicacion(@PathVariable long idMedico) {
		try {
			return new ResponseEntity<RespuestaApi>(medicoServicio.borrarUbicacion(idMedico), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("Error al borrar su ubicación, intente nuevamente más tarde"), HttpStatus.BAD_REQUEST);
		} catch (APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
