package com.glucome.controladores.medico;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.RegistroPacienteExternoRequest;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.servicios.PacienteServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/medico")
@CrossOrigin("*")
public class PacienteExternoControlador {

	@Autowired
	PacienteServicio pacienteServicio;

	@PostMapping("/{idMedico}/paciente/")
	private ResponseEntity<RespuestaApi> registrarPacienteExterno(@PathVariable long idMedico, @RequestBody RegistroPacienteExternoRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.registrar(idMedico, data), HttpStatus.OK);
		} catch (ParametrosNoValidosException | APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/{idMedico}/paciente/")
	private ResponseEntity<RespuestaApi> editarPacienteExterno(@PathVariable long idMedico, @RequestBody RegistroPacienteExternoRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.registrar(idMedico, data), HttpStatus.OK);
		} catch (ParametrosNoValidosException | APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/{idMedico}/paciente/{idPaciente}")
	private ResponseEntity<RespuestaApi> borrarPacienteExterno(@PathVariable long idMedico, @PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.borrar(idPaciente), HttpStatus.OK);
		} catch (APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

}
