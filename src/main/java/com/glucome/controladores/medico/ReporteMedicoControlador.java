package com.glucome.controladores.medico;

import com.glucome.api.RespuestaApi;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.ReporteMedicoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/medico/reporte")
@CrossOrigin("*")
public class ReporteMedicoControlador {

	@Autowired
	ReporteMedicoServicio reporteMedicoServicio;

	@GetMapping("/{idMedico}/por_momento/")
	private ResponseEntity<RespuestaApi> getReportePorMomento(@PathVariable long idMedico) {
		try {
			return new ResponseEntity<RespuestaApi>(reporteMedicoServicio.getReportePorMomento(idMedico), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idMedico}/por_rango_edad/")
	private ResponseEntity<RespuestaApi> getReportePorRangoEdad(@PathVariable long idMedico) {
		try {
			return new ResponseEntity<RespuestaApi>(reporteMedicoServicio.getReportePorRangoEdad(idMedico), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idMedico}/por_tipo_diabetes/")
	private ResponseEntity<RespuestaApi> getReportePorTipoDiabetes(@PathVariable long idMedico) {
		try {
			return new ResponseEntity<RespuestaApi>(reporteMedicoServicio.getReportePorTipoDiabetes(idMedico), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idMedico}/por_tipo_medicion/")
	private ResponseEntity<RespuestaApi> getReportePorTipoMedicion(@PathVariable long idMedico) {
		try {
			return new ResponseEntity<RespuestaApi>(reporteMedicoServicio.getReportePorTipoMedicion(idMedico), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idMedico}/ranking_alimentos/")
	private ResponseEntity<RespuestaApi> getReporteRankingAlimentos(@PathVariable long idMedico) {
		try {
			return new ResponseEntity<RespuestaApi>(reporteMedicoServicio.getReporteRankingAlimentos(idMedico), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
