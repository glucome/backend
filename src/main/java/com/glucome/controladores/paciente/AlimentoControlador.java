package com.glucome.controladores.paciente;

import com.glucome.api.RespuestaApi;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.AlimentoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/alimento")
@CrossOrigin("*")
public class AlimentoControlador {

	@Autowired
	private AlimentoServicio alimentoServicio;

	@GetMapping("/")
	private ResponseEntity<RespuestaApi> listadoAlimentos(@RequestParam(required = false) String nombre) {
		try {
			return new ResponseEntity<RespuestaApi>(alimentoServicio.get(nombre), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
