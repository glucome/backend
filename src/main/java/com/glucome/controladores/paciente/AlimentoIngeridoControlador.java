package com.glucome.controladores.paciente;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.AlimentoIngeridoRequest;
import com.glucome.api.request.ListadoAlimentoIngeridoRequest;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.servicios.AlimentosIngeridosServicio;
import com.google.gson.Gson;

@RestController
@RequestMapping("/api/v1/paciente")
@CrossOrigin("*")
public class AlimentoIngeridoControlador {

	@Autowired
	private AlimentosIngeridosServicio alimentoIngeridoServicio;

	@PostMapping(path = "/{idPaciente}/alimento_ingerido/")
	private ResponseEntity<RespuestaApi> registrarAlimentoIngerido(@PathVariable long idPaciente, @RequestParam(name = "data", required = false) String data, @RequestParam(name = "file", required = false) MultipartFile file) {

		AlimentoIngeridoRequest request = new Gson().fromJson(data, AlimentoIngeridoRequest.class);
		try {
			return new ResponseEntity<>(alimentoIngeridoServicio.registrarAlimentoIngerido(idPaciente, request, file), HttpStatus.OK);
		} catch (PacienteException | ParametrosNoValidosException | APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/{idPaciente}/alimento_ingerido/listado/")
	private ResponseEntity<RespuestaApi> getListadoAlimentoIngerido(@PathVariable long idPaciente, @RequestBody ListadoAlimentoIngeridoRequest data) {
		try {
			return new ResponseEntity<>(alimentoIngeridoServicio.getListado(data, idPaciente), HttpStatus.OK);
		} catch (APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
