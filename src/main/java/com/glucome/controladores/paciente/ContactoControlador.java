package com.glucome.controladores.paciente;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.ContactoRequest;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.ContactoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/")
@CrossOrigin("*")
public class ContactoControlador {

	@Autowired
	ContactoServicio contactoServicio;

	@PostMapping("paciente/{idPaciente}/contacto/")
	private ResponseEntity<RespuestaApi> registrarContacto(@PathVariable long idPaciente, @RequestBody ContactoRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(contactoServicio.registrar(idPaciente, data), HttpStatus.OK);
		} catch (ParametrosNoValidosException | PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("contacto/{idContacto}/")
	private ResponseEntity<RespuestaApi> borrarContacto(@PathVariable long idContacto) {
		try {
			return new ResponseEntity<RespuestaApi>(contactoServicio.borrar(idContacto), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("paciente/{idPaciente}/contacto/")
	private ResponseEntity<RespuestaApi> getContactos(@PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(contactoServicio.get(idPaciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

}
