package com.glucome.controladores.paciente;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.EjercicioRequest;
import com.glucome.api.request.ListadoEjercicioRequest;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.servicios.EjercicioServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/paciente")
@CrossOrigin("*")
public class EjercicioControlador {

	@Autowired
	private EjercicioServicio ejercicioServicio;

	@PostMapping("/{idPaciente}/ejercicio/")
	private ResponseEntity<RespuestaApi> registrarEjercicio(@PathVariable long idPaciente, @RequestBody EjercicioRequest data) {

		try {
			return new ResponseEntity<>(ejercicioServicio.registrarEjercicio(idPaciente, data), HttpStatus.OK);
		} catch (PacienteException | ParametrosNoValidosException | APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/{idPaciente}/ejercicio/listado/")
	private ResponseEntity<RespuestaApi> getListadoEjercicio(@PathVariable long idPaciente, @RequestBody ListadoEjercicioRequest data) {
		try {
			return new ResponseEntity<>(ejercicioServicio.getListado(data, idPaciente), HttpStatus.OK);
		} catch (APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
