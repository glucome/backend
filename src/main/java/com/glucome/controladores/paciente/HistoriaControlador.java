package com.glucome.controladores.paciente;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.ItemHistoriaRequest;
import com.glucome.api.request.ListadoItemHistoriaRequest;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.ItemHistoriaServicio;

@RestController
@RequestMapping("/api/v1/paciente")
@CrossOrigin("*")
public class HistoriaControlador {

	@Autowired
	private ItemHistoriaServicio itemHistoriaServicio;

	@PostMapping("/{idPaciente}/historia/")
	private ResponseEntity<RespuestaApi> registrarHistoria(@PathVariable long idPaciente, @RequestBody ItemHistoriaRequest data) {

		try {
			return new ResponseEntity<>(itemHistoriaServicio.registrarItemHistoria(idPaciente, data), HttpStatus.OK);
		} catch (PacienteException | ParametrosNoValidosException | APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/{idPaciente}/historia/")
	private ResponseEntity<RespuestaApi> editarItemHistoria(@PathVariable long idPaciente, @RequestBody ItemHistoriaRequest data) {

		try {
			return new ResponseEntity<>(itemHistoriaServicio.registrarItemHistoria(idPaciente, data), HttpStatus.OK);
		} catch (PacienteException | ParametrosNoValidosException | APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/historia/{itemId}/")
	private ResponseEntity<RespuestaApi> eliminarItem(@PathVariable long itemId) {

		try {
			return new ResponseEntity<>(itemHistoriaServicio.borrarItemHistoria(itemId), HttpStatus.OK);
		} catch (APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/{idPaciente}/historia/listado/")
	private ResponseEntity<RespuestaApi> getListadoHistorias(@PathVariable long idPaciente, @RequestBody ListadoItemHistoriaRequest data) {
		try {
			return new ResponseEntity<>(itemHistoriaServicio.getListado(data, idPaciente), HttpStatus.OK);
		} catch (APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/historia/{itemId}/")
	private ResponseEntity<RespuestaApi> getItem(@PathVariable long itemId) {

		try {
			return new ResponseEntity<>(itemHistoriaServicio.getItemHistoria(itemId), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
