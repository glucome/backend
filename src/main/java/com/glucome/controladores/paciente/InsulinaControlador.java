package com.glucome.controladores.paciente;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.InsulinaInyectadaRequest;
import com.glucome.api.request.ListadoInyeccionInsulinaRequest;
import com.glucome.excepciones.InsulinaException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.servicios.InsulinaServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/paciente")
@CrossOrigin("*")
public class InsulinaControlador {

	@Autowired
	private InsulinaServicio insulinaServicio;

	@PostMapping("/{idPaciente}/insulina/")
	private ResponseEntity<RespuestaApi> registrarInyeccion(@PathVariable long idPaciente, @RequestBody InsulinaInyectadaRequest data) {

		try {
			return new ResponseEntity<>(insulinaServicio.registrarInyeccion(idPaciente, data), HttpStatus.OK);
		} catch (PacienteException | ParametrosNoValidosException | InsulinaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/{idPaciente}/insulina/listado/")
	private ResponseEntity<RespuestaApi> getListadoInyecciones(@PathVariable long idPaciente, @RequestBody ListadoInyeccionInsulinaRequest data) {
		try {
			return new ResponseEntity<>(insulinaServicio.getListado(data, idPaciente), HttpStatus.OK);
		} catch (InsulinaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
