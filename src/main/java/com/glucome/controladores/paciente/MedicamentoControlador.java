package com.glucome.controladores.paciente;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.MedicamentoPacienteRequest;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.MedicamentoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/medicamento")
@CrossOrigin("*")
public class MedicamentoControlador {

	@Autowired
	MedicamentoServicio medicacionServicio;

	@GetMapping("/")
	private ResponseEntity<RespuestaApi> listadoMedico(@RequestParam(required = false) String nombre, @RequestParam(required = true) long paciente) {
		try {
			return new ResponseEntity<RespuestaApi>(medicacionServicio.get(nombre, paciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/")
	private ResponseEntity<RespuestaApi> agregarMedicamento(@RequestBody MedicamentoPacienteRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(medicacionServicio.agregarMedicamento(data), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
