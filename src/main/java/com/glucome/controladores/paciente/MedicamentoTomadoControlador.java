package com.glucome.controladores.paciente;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.ListadoMedicamentoTomadosRequest;
import com.glucome.api.request.MedicamentoTomadoPacienteRequest;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.MedicamentoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/v1/paciente/{idPaciente}/medicamento")
@CrossOrigin("*")
public class MedicamentoTomadoControlador {

	@Autowired
	MedicamentoServicio medicacionServicio;

	@PostMapping("/")
	private ResponseEntity<RespuestaApi> agregarMedicamentoTomado(@RequestBody MedicamentoTomadoPacienteRequest data, @PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(medicacionServicio.agregarMedicamentoTomado(data, idPaciente), HttpStatus.OK);
		} catch (PersistenciaException | ParametrosNoValidosException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/{idMedicamento}/")
	private ResponseEntity<RespuestaApi> borrarMedicamentoTomado(@PathVariable long idMedicamento, @PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(medicacionServicio.borradoMedicamento(idMedicamento), HttpStatus.OK);
		} catch (APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/listado/")
	private ResponseEntity<RespuestaApi> getListadoMedicion(@PathVariable long idPaciente, @RequestBody ListadoMedicamentoTomadosRequest data) {
		try {
			return new ResponseEntity<>(medicacionServicio.getListadoIngeridos(data, idPaciente), HttpStatus.OK);
		} catch (APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

}
