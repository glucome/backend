package com.glucome.controladores.paciente;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.ListadoMedicionRequest;
import com.glucome.api.request.MedicionRequest;
import com.glucome.excepciones.MedicionException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.servicios.MedicionServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/api/v1/paciente")
@CrossOrigin("*")
public class MedicionControlador {
	@Autowired
	MedicionServicio medicionServicio;

	@PostMapping("/{idPaciente}/medicion/")
	private ResponseEntity<RespuestaApi> registrarMedicion(@PathVariable long idPaciente, @RequestBody MedicionRequest data) {

		try {
			return new ResponseEntity<>(medicionServicio.registrarMedicion(idPaciente, data), HttpStatus.OK);
		} catch (PacienteException | MedicionException | ParametrosNoValidosException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/{idPaciente}/medicion/listado/")
	private ResponseEntity<RespuestaApi> getListadoMedicion(@PathVariable long idPaciente, @RequestBody ListadoMedicionRequest data) {
		try {
			return new ResponseEntity<>(medicionServicio.getListado(data, idPaciente), HttpStatus.OK);
		} catch (MedicionException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/{idPaciente}/medicion/listadoSinFiltro/")
	private ResponseEntity<RespuestaApi> getListadoMedicionSinFiltro(@PathVariable long idPaciente) {
		try {
			return new ResponseEntity<>(medicionServicio.getListadoSinFiltro(idPaciente), HttpStatus.OK);
		} catch (MedicionException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/{idPaciente}/mediciones/exportar/", method = RequestMethod.GET)
	public void doGet(HttpServletRequest request, HttpServletResponse response, @PathVariable long idPaciente, @RequestParam(required = false) String fechaHoraDesde, @RequestParam(required = false) String fechaHoraHasta, @RequestParam(required = false) Integer valorDesde, @RequestParam(required = false) Integer valorHasta, @RequestParam(required = false) Integer tipo,
			@RequestParam(required = false) Integer momento, @RequestParam(required = false) Integer numeroPagina, @RequestParam(required = false) Integer registrosPorPagina) throws IOException {
		try {

			ListadoMedicionRequest filtro = new ListadoMedicionRequest(fechaHoraDesde, fechaHoraHasta, valorDesde, valorHasta, tipo, momento, numeroPagina, registrosPorPagina);
			medicionServicio.descargarMediciones(filtro, response, idPaciente);
		} catch (IOException | MedicionException e) {
			e.printStackTrace();
			return;
		}

	}
}
