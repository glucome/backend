package com.glucome.controladores.paciente;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.FactorCorreccionInsulinaRequest;
import com.glucome.api.request.RegistroPacienteRequest;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.AllegadoServicio;
import com.glucome.servicios.MedicamentoServicio;
import com.glucome.servicios.MedicoServicio;
import com.glucome.servicios.PacienteServicio;

@RestController
@RequestMapping("/api/v1/paciente")
@CrossOrigin("*")
public class PacienteControlador {

	@Autowired
	PacienteServicio pacienteServicio;
	@Autowired
	MedicoServicio medicoServicio;
	@Autowired
	AllegadoServicio allegadoServicio;
	@Autowired
	MedicamentoServicio medicamentoServicio;

	@PostMapping("/")
	private ResponseEntity<RespuestaApi> registrarPaciente(@RequestBody RegistroPacienteRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.registrar(data), HttpStatus.OK);
		} catch (PacienteException | ParametrosNoValidosException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		} catch (IOException e) {
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("Ha ocurrido un error al subir el archivo"), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idPaciente}/")
	private ResponseEntity<RespuestaApi> getPaciente(@PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.get(idPaciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idPaciente}/ultimaMedicion/")
	private ResponseEntity<RespuestaApi> registrarPaciente(@PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.getUltimaMedicion(idPaciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idPaciente}/ultimas_mediciones/")
	private ResponseEntity<RespuestaApi> getUltimasMediciones(@PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.getUltimasMediciones(idPaciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idPaciente}/hemoglobina_glicosilada/")
	private ResponseEntity<RespuestaApi> getHemoglobinaGlicosilada(@PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.getHemoglobinaGlicosilada(idPaciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idPaciente}/hemoglobina_glicosilada_anual/")
	private ResponseEntity<RespuestaApi> getHemoglobinaGlicosiladaAnual(@PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.getHemoglobinaGlicosiladaAnual(idPaciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idPaciente}/medicos/")
	private ResponseEntity<RespuestaApi> listadoMedicosDeUnPaciente(@PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(medicoServicio.getListado(idPaciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idPaciente}/allegados/")
	private ResponseEntity<RespuestaApi> listadoAllegadosDeUnPaciente(@PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(allegadoServicio.getListado(idPaciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idPaciente}/medicamentos/")
	private ResponseEntity<RespuestaApi> listadoMedicamentosDeUnPaciente(@PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(medicamentoServicio.getListado(idPaciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/medico/")
	private ResponseEntity<RespuestaApi> borrarMedico(@RequestParam long idPaciente, @RequestParam long idMedico) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.borrarMedico(idPaciente, idMedico), HttpStatus.OK);
		} catch (PersistenciaException | APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/allegado/")
	private ResponseEntity<RespuestaApi> borrarAllegado(@RequestParam long idPaciente, @RequestParam long idAllegado) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.borrarAllegado(idPaciente, idAllegado), HttpStatus.OK);
		} catch (PersistenciaException | APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/{idPaciente}/factor_correccion_insulina/")
	private ResponseEntity<RespuestaApi> configurarFactores(@PathVariable long idPaciente, @RequestBody FactorCorreccionInsulinaRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.setFactorCorreccionInsulina(idPaciente, data), HttpStatus.OK);
		} catch (ParametrosNoValidosException | PacienteException | APIException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idPaciente}/factor_correccion_insulina/")
	private ResponseEntity<RespuestaApi> getFactores(@PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.getFactorCorreccionInsulina(idPaciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/{idPaciente}/info_tarjeta/")
	private ResponseEntity<RespuestaApi> getInfoTarjeta(@PathVariable long idPaciente) {
		try {
			return new ResponseEntity<RespuestaApi>(pacienteServicio.getInfoTarjeta(idPaciente), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

}
