package com.glucome.controladores.paciente;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.MarcarPagosNotificadosRequest;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.PagoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/pago")
@CrossOrigin("*")
public class PagoControlador {

	@Autowired
	private PagoServicio pagoServicio;

	@GetMapping("/{idUsuario}/sin_notificar/")
	private ResponseEntity<RespuestaApi> getPagosSinNotificar(@PathVariable long idUsuario) {
		try {
			return new ResponseEntity<RespuestaApi>(pagoServicio.getPagosSinNotificar(idUsuario), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/marcar_notificados/", method = RequestMethod.POST, consumes = "application/json")
	private ResponseEntity<RespuestaApi> marcarPagosComoNotificados(@RequestBody MarcarPagosNotificadosRequest data) {
		try {
			return new ResponseEntity<RespuestaApi>(pagoServicio.marcarPagosComoNotificados(data.getIdsPagos()), HttpStatus.OK);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			return new ResponseEntity<RespuestaApi>(new RespuestaApi(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
