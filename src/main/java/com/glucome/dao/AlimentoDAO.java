package com.glucome.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.glucome.enumerador.MomentoAlimento;
import com.glucome.enumerador.ValorIndiceGlucemico;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Alimento;
import com.glucome.utils.UtilesString;

@Repository("alimentoDAO")
public class AlimentoDAO extends BaseDAO {

	public List<Alimento> getListado(ValorIndiceGlucemico tipo, List<MomentoAlimento> momentos) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Alimento> criteria = builder.createQuery(Alimento.class);
			Root<Alimento> alimentoRoot = criteria.from(Alimento.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<Alimento, MomentoAlimento> momentoJoin = alimentoRoot.join("momentos");

			if (momentos != null) {
				List<Predicate> condicionesOr = new ArrayList<>();
				for (MomentoAlimento momento : momentos) {
					condicionesOr.add(builder.equal(momentoJoin, momento));
				}
				condicionesWhere.add(builder.or(condicionesOr.toArray(new Predicate[] {})));
			}
			condicionesWhere.add(builder.equal(alimentoRoot.get("indiceGlucemico"), tipo));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(alimentoRoot).distinct(true);
			TypedQuery<Alimento> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<Alimento> getListado(String nombre) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Alimento> criteria = builder.createQuery(Alimento.class);
			Root<Alimento> alimentoRoot = criteria.from(Alimento.class);
			List<Predicate> condicionesWhere = new ArrayList<>();

			if (!UtilesString.isNullOrEmpty(nombre)) {
				condicionesWhere.add(builder.like(builder.lower(alimentoRoot.get("nombre")), "%" + nombre.toLowerCase() + "%"));
			}

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(alimentoRoot);
			TypedQuery<Alimento> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage());
		}
	}

	public Alimento get(long idAlimento) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			return (Alimento) session.get(Alimento.class, idAlimento);
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}

	}

}
