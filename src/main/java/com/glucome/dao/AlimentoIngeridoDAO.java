package com.glucome.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.glucome.api.request.ListadoAlimentoIngeridoRequest;
import com.glucome.enumerador.MomentoAlimento;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Alimento;
import com.glucome.modelo.AlimentoIngerido;
import com.glucome.modelo.Paciente;
import com.glucome.utils.UtilesFecha;
import com.glucome.utils.UtilesString;

@Repository("AlimentoIngeridoDAO")
public class AlimentoIngeridoDAO extends BaseDAO {

	public List<AlimentoIngerido> getListado(ListadoAlimentoIngeridoRequest filtro, long idPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<AlimentoIngerido> criteria = builder.createQuery(AlimentoIngerido.class);
			Root<AlimentoIngerido> alimentoIngeridoRoot = criteria.from(AlimentoIngerido.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<AlimentoIngerido, Paciente> joinPaciente = alimentoIngeridoRoot.join("paciente", JoinType.INNER);
			condicionesWhere.add(builder.equal(joinPaciente.get("id"), idPaciente));

			if (!UtilesString.isNullOrEmpty(filtro.getFechaHoraDesde())) {
				condicionesWhere.add(builder.greaterThanOrEqualTo(alimentoIngeridoRoot.get("fechaHora"), UtilesFecha.getLocalDateTimeFromString(filtro.getFechaHoraDesde())));
			}

			if (!UtilesString.isNullOrEmpty(filtro.getFechaHoraHasta())) {
				condicionesWhere.add(builder.lessThanOrEqualTo(alimentoIngeridoRoot.get("fechaHora"), UtilesFecha.getLocalDateTimeFromString(filtro.getFechaHoraHasta())));
			}

			if (filtro.getPorcionesDesde() != null) {
				condicionesWhere.add(builder.greaterThanOrEqualTo(alimentoIngeridoRoot.get("porciones"), filtro.getPorcionesDesde()));
			}

			if (filtro.getPorcionesHasta() != null) {
				condicionesWhere.add(builder.lessThanOrEqualTo(alimentoIngeridoRoot.get("porciones"), filtro.getPorcionesHasta()));
			}

			if (filtro.getMomento() != null) {
				condicionesWhere.add(builder.lessThanOrEqualTo(alimentoIngeridoRoot.get("momento"), MomentoAlimento.get(filtro.getMomento())));
			}

			if (filtro.getAlimento() != null) {
				Join<AlimentoIngerido, Alimento> joinAlimento = alimentoIngeridoRoot.join("alimento", JoinType.INNER);
				condicionesWhere.add(builder.equal(joinAlimento.get("id"), filtro.getAlimento()));
			}

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(alimentoIngeridoRoot);
			criteria.orderBy(builder.desc(alimentoIngeridoRoot.get("fechaHora")));
			TypedQuery<AlimentoIngerido> query = session.createQuery(criteria);
			query.setFirstResult(filtro.getNumeroPagina() * filtro.getRegistrosPorPagina());
			query.setMaxResults(filtro.getRegistrosPorPagina());
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage());
		}
	}

}
