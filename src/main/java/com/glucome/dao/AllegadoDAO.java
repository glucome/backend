package com.glucome.dao;

import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Allegado;
import com.glucome.modelo.Paciente;
import com.glucome.utils.UtilesString;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository("AllegadoDAO")
public class AllegadoDAO extends BaseDAO {

	public Allegado get(long id) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			return (Allegado) session.get(Allegado.class, id);
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<Allegado> getListado(String nombre) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Allegado> criteria = builder.createQuery(Allegado.class);
			Root<Allegado> allegadoRoot = criteria.from(Allegado.class);
			List<Predicate> condicionesWhere = new ArrayList<>();

			if (!UtilesString.isNullOrEmpty(nombre)) {
				condicionesWhere.add(builder.like(builder.lower(allegadoRoot.get("nombre")), "%" + nombre.toLowerCase() + "%"));
			}

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(allegadoRoot);
			TypedQuery<Allegado> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<Allegado> getListado(long idPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Allegado> criteria = builder.createQuery(Allegado.class);
			Root<Paciente> pacienteRoot = criteria.from(Paciente.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<Paciente, Allegado> allegadoRoot = pacienteRoot.join("allegados", JoinType.INNER);
			condicionesWhere.add(builder.equal(pacienteRoot.get("id"), idPaciente));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(allegadoRoot);
			TypedQuery<Allegado> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

}
