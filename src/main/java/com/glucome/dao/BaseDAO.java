package com.glucome.dao;

import com.glucome.excepciones.PersistenciaException;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Métodos comunes para consulta/persistencia en la base de datos.
 */
@Repository("baseDAO")
public class BaseDAO {

    @PersistenceContext
    protected EntityManager entityManager;

    /**
     * Graba un objeto en la base de datos.
     *
     * @param objeto
     * @throws PersistenciaException
     */
    public void grabar(Object objeto) throws PersistenciaException {
        Session session = entityManager.unwrap(Session.class);
        try {
            session.beginTransaction();
            session.saveOrUpdate(objeto);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
            throw new PersistenciaException(e.getMessage());
        }
    }

    /**
     * Borra un objeto de la base de datos.
     *
     * @param objeto
     * @throws PersistenciaException
     * @throws PojoNoBorrableException
     */
    public void borrar(Object objeto) throws PersistenciaException {
        Session session = entityManager.unwrap(Session.class);
        try {
            session.beginTransaction();
            session.delete(objeto);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
            throw new PersistenciaException(e.getMessage());
        }
    }

    public void correrScript(String alimentos) throws PersistenciaException {
        Session session = entityManager.unwrap(Session.class);
        try {
            session.beginTransaction();
            NativeQuery query = session.createNativeQuery(alimentos);
            query.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
            throw new PersistenciaException(e.getMessage());
        }

    }

}