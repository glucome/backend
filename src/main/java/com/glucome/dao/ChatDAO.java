package com.glucome.dao;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Chat;
import com.glucome.modelo.Medico;
import com.glucome.modelo.Mensaje;
import com.glucome.modelo.Paciente;
import com.glucome.modelo.Usuario;
import com.glucome.wrapper.MensajeWrapper;
import com.glucome.wrapper.UsuarioChatWrapper;

@Repository("chatDAO")
public class ChatDAO extends BaseDAO {

	@Transactional
	public Chat get(long id) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			return (Chat) session.get(Chat.class, id);
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public Chat crearChatMedicoPaciente(Medico medico, Paciente paciente) throws PersistenciaException {
		Chat chat = this.getChat(medico, paciente);
		if (chat == null) {
			chat = new Chat();
			chat.getUsuarios().add(medico.getUsuario());
			chat.getUsuarios().add(paciente.getUsuario());
			this.grabar(chat);
		}

		return chat;
	}

	private Chat getChat(Medico medico, Paciente paciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Chat> criteria = builder.createQuery(Chat.class);
			Root<Usuario> root = criteria.from(Usuario.class);

			Join<Usuario, Chat> joinMedicoChats = root.join("chats");
			Join<Chat, Usuario> joinChatsUsuarios = joinMedicoChats.join("usuarios");
			List<Predicate> condicionesWhere = new ArrayList<>();
			condicionesWhere.add(builder.equal(root, medico.getUsuario()));
			condicionesWhere.add(builder.equal(joinChatsUsuarios, paciente.getUsuario()));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(joinMedicoChats);
			TypedQuery<Chat> query = session.createQuery(criteria);
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<MensajeWrapper> getMensajes(Long idChat) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<MensajeWrapper> criteria = builder.createQuery(MensajeWrapper.class);
			Root<Mensaje> root = criteria.from(Mensaje.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<Chat, Usuario> joinAutor = root.join("autor");
			Join<Chat, Usuario> joinDestinatario = root.join("destinatario");

			condicionesWhere.add(builder.equal(root.get("chat"), idChat));

			criteria.orderBy(builder.asc(root.get("fechaHora")));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.multiselect(root.get("cuerpo"), joinAutor.get("id"), joinDestinatario.get("id"), root.get("fechaHora"), root.get("fechaHoraVisto"));
			TypedQuery<MensajeWrapper> query = session.createQuery(criteria);
			return query.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage());
		}
	}

	@Transactional
	public Mensaje guardarMensaje(Usuario autor, Usuario destinatario, Chat chat, String cuerpo, LocalDateTime fechaHora) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);

		Mensaje mensaje = new Mensaje();
		mensaje.setAutor(autor);
		mensaje.setDestinatario(destinatario);
		mensaje.setChat(chat);
		mensaje.setCuerpo(cuerpo);
		mensaje.setFechaHora(fechaHora);
		mensaje.setFechaHoraVisto(null);
		session.save(mensaje);

		session.getTransaction().commit();

		return mensaje;
	}

	public List<UsuarioChatWrapper> getPacientesMedico(Long idUsuarioMedico) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<UsuarioChatWrapper> criteria = builder.createQuery(UsuarioChatWrapper.class);
			Root<Chat> root = criteria.from(Chat.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<Chat, Usuario> joinUsuarios = root.join("usuarios");
			Join<Usuario, Medico> joinPaciente = joinUsuarios.join("paciente");
			Join<Chat, Mensaje> joinMensaje = root.join("mensajes", JoinType.LEFT);

			Subquery<Chat> subquery = criteria.subquery(Chat.class);
			Root<Chat> rootChat = subquery.from(Chat.class);
			subquery.select(rootChat);
			Join<Chat, Usuario> joinUsuariosSubQuery = root.join("usuarios");
			subquery.where(builder.equal(joinUsuariosSubQuery.get("id"), idUsuarioMedico));

			condicionesWhere.add(root.in(subquery));

			Expression cantidadMensajesNoLeidos = builder.sum(builder.<Number>selectCase().when(builder.and(builder.notEqual(joinMensaje.get("autor"), idUsuarioMedico), builder.isNull(joinMensaje.get("fechaHoraVisto"))), 1L).otherwise(0L));

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.multiselect(joinUsuarios.get("id"), root.get("id"), joinPaciente.get("nombre"), joinUsuarios.get("imagen"), joinUsuarios.get("estado"), cantidadMensajesNoLeidos);
			criteria.groupBy(root.get("id"));
			TypedQuery<UsuarioChatWrapper> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<UsuarioChatWrapper> getMedicosPaciente(Long idUsuarioPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<UsuarioChatWrapper> criteria = builder.createQuery(UsuarioChatWrapper.class);
			Root<Chat> root = criteria.from(Chat.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<Chat, Usuario> joinUsuario = root.join("usuarios");
			Join<Usuario, Medico> joinMedico = joinUsuario.join("medico");
			Join<Chat, Mensaje> joinMensaje = root.join("mensajes", JoinType.LEFT);

			Subquery<Chat> subquery = criteria.subquery(Chat.class);
			Root<Chat> rootChat = subquery.from(Chat.class);
			subquery.select(rootChat);
			Join<Chat, Usuario> joinUsuariosSubQuery = root.join("usuarios");
			subquery.where(builder.equal(joinUsuariosSubQuery.get("id"), idUsuarioPaciente));

			condicionesWhere.add(root.in(subquery));

			Expression cantidadMensajesNoLeidos = builder.sum(builder.<Number>selectCase().when(builder.and(builder.notEqual(joinMensaje.get("autor"), idUsuarioPaciente), builder.isNull(joinMensaje.get("fechaHoraVisto"))), 1L).otherwise(0L));

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.multiselect(joinUsuario.get("id"), root.get("id"), joinMedico.get("nombre"), joinUsuario.get("imagen"), joinUsuario.get("estado"), cantidadMensajesNoLeidos);
			criteria.groupBy(root.get("id"));
			TypedQuery<UsuarioChatWrapper> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<Mensaje> getMensajesNoLeidos(Long idChat, Long idAutor) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Mensaje> criteria = builder.createQuery(Mensaje.class);
			Root<Mensaje> root = criteria.from(Mensaje.class);
			List<Predicate> condicionesWhere = new ArrayList<>();

			condicionesWhere.add(builder.equal(root.get("chat"), idChat));
			condicionesWhere.add(builder.notEqual(root.get("autor"), idAutor));
			condicionesWhere.add(builder.isNull(root.get("fechaHoraVisto")));

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(root);
			TypedQuery<Mensaje> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<Mensaje> marcarMensajesComoLeidos(Long idChat, Long idAutor, LocalDateTime fechaHoraVisto) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			session.beginTransaction();
			List<Mensaje> mensajesNoLeidos = getMensajesNoLeidos(idChat, idAutor);

			for (Mensaje mensaje : mensajesNoLeidos) {
				mensaje.setFechaHoraVisto(fechaHoraVisto);
				session.update(mensaje);
			}

			session.getTransaction().commit();
			session.close();

			return mensajesNoLeidos;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage());
		}
	}
}
