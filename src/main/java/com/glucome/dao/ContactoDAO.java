package com.glucome.dao;

import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Contacto;
import com.glucome.modelo.Paciente;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository("ContactoDAO")
public class ContactoDAO extends BaseDAO {

	public Contacto get(long idContacto) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			return (Contacto) session.get(Contacto.class, idContacto);
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}

	}

	public List<Contacto> getListado(long idPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Contacto> criteria = builder.createQuery(Contacto.class);
			Root<Paciente> pacienteRoot = criteria.from(Paciente.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<Paciente, Contacto> contactoRoot = pacienteRoot.join("contactos", JoinType.INNER);
			condicionesWhere.add(builder.equal(pacienteRoot.get("id"), idPaciente));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(contactoRoot);
			TypedQuery<Contacto> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

}
