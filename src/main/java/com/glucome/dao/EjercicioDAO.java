package com.glucome.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.glucome.api.request.ListadoEjercicioRequest;
import com.glucome.enumerador.TipoEjercicio;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Ejercicio;
import com.glucome.modelo.Paciente;
import com.glucome.utils.UtilesFecha;
import com.glucome.utils.UtilesString;

@Repository("EjercicioDAO")
public class EjercicioDAO extends BaseDAO {

	public List<Ejercicio> getListado(ListadoEjercicioRequest filtro, long idPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Ejercicio> criteria = builder.createQuery(Ejercicio.class);
			Root<Ejercicio> ejercicioRoot = criteria.from(Ejercicio.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<Ejercicio, Paciente> joinPaciente = ejercicioRoot.join("paciente", JoinType.INNER);
			condicionesWhere.add(builder.equal(joinPaciente.get("id"), idPaciente));

			if (!UtilesString.isNullOrEmpty(filtro.getFechaHoraDesde())) {
				condicionesWhere.add(builder.greaterThanOrEqualTo(ejercicioRoot.get("fechaHora"), UtilesFecha.getLocalDateTimeFromString(filtro.getFechaHoraDesde())));
			}

			if (!UtilesString.isNullOrEmpty(filtro.getFechaHoraHasta())) {
				condicionesWhere.add(builder.lessThanOrEqualTo(ejercicioRoot.get("fechaHora"), UtilesFecha.getLocalDateTimeFromString(filtro.getFechaHoraHasta())));
			}

			if (filtro.getMinutosDesde() != null) {
				condicionesWhere.add(builder.greaterThanOrEqualTo(ejercicioRoot.get("minutos"), filtro.getMinutosDesde()));
			}

			if (filtro.getMinutosHasta() != null) {
				condicionesWhere.add(builder.lessThanOrEqualTo(ejercicioRoot.get("minutos"), filtro.getMinutosHasta()));
			}

			if (filtro.getTipo() != null) {
				condicionesWhere.add(builder.equal(ejercicioRoot.get("tipo"), TipoEjercicio.get(filtro.getTipo())));
			}

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(ejercicioRoot);
			criteria.orderBy(builder.desc(ejercicioRoot.get("fechaHora")));
			TypedQuery<Ejercicio> query = session.createQuery(criteria);
			query.setFirstResult(filtro.getNumeroPagina() * filtro.getRegistrosPorPagina());
			query.setMaxResults(filtro.getRegistrosPorPagina());
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage());
		}
	}

}
