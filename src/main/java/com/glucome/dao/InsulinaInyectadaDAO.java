package com.glucome.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.glucome.api.request.ListadoInyeccionInsulinaRequest;
import com.glucome.enumerador.TipoInsulina;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.InsulinaInyectada;
import com.glucome.modelo.Paciente;
import com.glucome.utils.UtilesFecha;
import com.glucome.utils.UtilesString;

@Repository("InsulinaInyectadaDAO")
public class InsulinaInyectadaDAO extends BaseDAO {

	public List<InsulinaInyectada> getListado(ListadoInyeccionInsulinaRequest filtro, long idPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<InsulinaInyectada> criteria = builder.createQuery(InsulinaInyectada.class);
			Root<InsulinaInyectada> insulinaInyectadaDAO = criteria.from(InsulinaInyectada.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<InsulinaInyectada, Paciente> joinPaciente = insulinaInyectadaDAO.join("paciente", JoinType.INNER);
			condicionesWhere.add(builder.equal(joinPaciente.get("id"), idPaciente));

			if (!UtilesString.isNullOrEmpty(filtro.getFechaHoraDesde())) {
				condicionesWhere.add(builder.greaterThanOrEqualTo(insulinaInyectadaDAO.get("fechaHora"), UtilesFecha.getLocalDateTimeFromString(filtro.getFechaHoraDesde())));
			}

			if (!UtilesString.isNullOrEmpty(filtro.getFechaHoraHasta())) {
				condicionesWhere.add(builder.lessThanOrEqualTo(insulinaInyectadaDAO.get("fechaHora"), UtilesFecha.getLocalDateTimeFromString(filtro.getFechaHoraHasta())));
			}

			if (filtro.getUnidadesDesde() != null) {
				condicionesWhere.add(builder.greaterThanOrEqualTo(insulinaInyectadaDAO.get("unidades"), filtro.getUnidadesDesde()));
			}

			if (filtro.getUnidadesHasta() != null) {
				condicionesWhere.add(builder.lessThanOrEqualTo(insulinaInyectadaDAO.get("unidades"), filtro.getUnidadesHasta()));
			}

			if (filtro.getTipo() != null) {
				condicionesWhere.add(builder.equal(insulinaInyectadaDAO.get("tipo"), TipoInsulina.getById(filtro.getTipo())));
			}

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(insulinaInyectadaDAO);
			criteria.orderBy(builder.desc(insulinaInyectadaDAO.get("fechaHora")));
			TypedQuery<InsulinaInyectada> query = session.createQuery(criteria);
			query.setFirstResult(filtro.getNumeroPagina() * filtro.getRegistrosPorPagina());
			query.setMaxResults(filtro.getRegistrosPorPagina());
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage());
		}
	}

}
