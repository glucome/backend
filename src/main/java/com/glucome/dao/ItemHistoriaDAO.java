package com.glucome.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.glucome.api.request.ListadoItemHistoriaRequest;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.ItemHistoriaClinica;
import com.glucome.modelo.Medico;
import com.glucome.modelo.Paciente;
import com.glucome.utils.UtilesFecha;
import com.glucome.utils.UtilesString;

@Repository("ItemHistoriaDAO")
public class ItemHistoriaDAO extends BaseDAO {

	public List<ItemHistoriaClinica> getListado(ListadoItemHistoriaRequest filtro, long idPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ItemHistoriaClinica> criteria = builder.createQuery(ItemHistoriaClinica.class);
			Root<ItemHistoriaClinica> itemHistoriaRoot = criteria.from(ItemHistoriaClinica.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<ItemHistoriaClinica, Paciente> joinPaciente = itemHistoriaRoot.join("paciente", JoinType.INNER);
			Join<ItemHistoriaClinica, Medico> joinMedico = itemHistoriaRoot.join("medico", JoinType.INNER);
			condicionesWhere.add(builder.equal(joinPaciente.get("id"), idPaciente));
			condicionesWhere.add(builder.equal(joinMedico.get("id"), filtro.getMedico()));

			aplicarFiltrosHistoria(filtro, builder, itemHistoriaRoot, condicionesWhere, joinMedico);

			criteria.orderBy(builder.desc(itemHistoriaRoot.get("fecha")));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(itemHistoriaRoot);
			TypedQuery<ItemHistoriaClinica> query = session.createQuery(criteria);
			query.setFirstResult(filtro.getNumeroPagina() * filtro.getRegistrosPorPagina());
			query.setMaxResults(filtro.getRegistrosPorPagina());
			return query.getResultList();
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	private void aplicarFiltrosHistoria(ListadoItemHistoriaRequest filtro, CriteriaBuilder builder, Root<ItemHistoriaClinica> itemHistoriaRoot, List<Predicate> condicionesWhere, Join<ItemHistoriaClinica, Medico> joinMedico) {

		condicionesWhere.add(builder.equal(joinMedico.get("id"), filtro.getMedico()));
		if (!UtilesString.isNullOrEmpty(filtro.getFechaDesde())) {
			condicionesWhere.add(builder.greaterThanOrEqualTo(itemHistoriaRoot.get("fecha"), UtilesFecha.getLocalDateFromString(filtro.getFechaDesde())));
		}

		if (!UtilesString.isNullOrEmpty(filtro.getFechaHasta())) {
			condicionesWhere.add(builder.lessThanOrEqualTo(itemHistoriaRoot.get("fecha"), UtilesFecha.getLocalDateFromString(filtro.getFechaHasta())));
		}

		if (filtro.getDescripcion() != null) {
			condicionesWhere.add(builder.like(itemHistoriaRoot.get("descripcion"), "%" + filtro.getDescripcion() + "%"));
		}
	}

	public ItemHistoriaClinica get(long id) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			return (ItemHistoriaClinica) session.get(ItemHistoriaClinica.class, id);
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<ItemHistoriaClinica> getListadoSinPaginar(long idPaciente, ListadoItemHistoriaRequest filtro) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ItemHistoriaClinica> criteria = builder.createQuery(ItemHistoriaClinica.class);
			Root<ItemHistoriaClinica> itemHistoriaRoot = criteria.from(ItemHistoriaClinica.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<ItemHistoriaClinica, Paciente> joinPaciente = itemHistoriaRoot.join("paciente", JoinType.INNER);
			Join<ItemHistoriaClinica, Medico> joinMedico = itemHistoriaRoot.join("medico", JoinType.INNER);
			condicionesWhere.add(builder.equal(joinPaciente.get("id"), idPaciente));

			aplicarFiltrosHistoria(filtro, builder, itemHistoriaRoot, condicionesWhere, joinMedico);

			criteria.orderBy(builder.desc(itemHistoriaRoot.get("fecha")));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(itemHistoriaRoot);
			TypedQuery<ItemHistoriaClinica> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

}
