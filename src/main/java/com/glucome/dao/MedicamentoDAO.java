package com.glucome.dao;

import com.glucome.api.request.ListadoMedicamentoTomadosRequest;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Medicamento;
import com.glucome.modelo.MedicamentoIngerido;
import com.glucome.modelo.Paciente;
import com.glucome.utils.UtilesFecha;
import com.glucome.utils.UtilesString;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository("MedicamentoDAO")
public class MedicamentoDAO extends BaseDAO {

	public List<Medicamento> getListado(String nombre, long paciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Medicamento> criteria = builder.createQuery(Medicamento.class);
			Root<Medicamento> medicamentoRoot = criteria.from(Medicamento.class);
			Join<Medicamento, Paciente> pacienteRoot = medicamentoRoot.join("pacientes", JoinType.LEFT);
			List<Predicate> condicionesWhere = new ArrayList<>();
			condicionesWhere.add(builder.or(builder.isNull(pacienteRoot), builder.notEqual(pacienteRoot.get("id"), paciente)));
			if (!UtilesString.isNullOrEmpty(nombre)) {
				Predicate droga = builder.like(builder.lower(medicamentoRoot.get("droga")), "%" + nombre.toLowerCase() + "%");
				Predicate marca = builder.like(builder.lower(medicamentoRoot.get("marca")), "%" + nombre.toLowerCase() + "%");
				Predicate presentacion = builder.like(builder.lower(medicamentoRoot.get("presentacion")), "%" + nombre.toLowerCase() + "%");
				Predicate laboratorio = builder.like(builder.lower(medicamentoRoot.get("laboratorio")), "%" + nombre.toLowerCase() + "%");

				condicionesWhere.add(builder.or(droga, marca, presentacion, laboratorio));
			}

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(medicamentoRoot);
			TypedQuery<Medicamento> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<Medicamento> getListado(long idPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Medicamento> criteria = builder.createQuery(Medicamento.class);
			Root<Paciente> pacienteRoot = criteria.from(Paciente.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<Paciente, Medicamento> medicamentoRoot = pacienteRoot.join("medicamentos", JoinType.INNER);
			condicionesWhere.add(builder.equal(pacienteRoot.get("id"), idPaciente));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(medicamentoRoot);
			TypedQuery<Medicamento> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public Medicamento get(long idMedicamento) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			return (Medicamento) session.get(Medicamento.class, idMedicamento);
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}

	}

	public MedicamentoIngerido getIngerido(long idMedicamento) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			return (MedicamentoIngerido) session.get(MedicamentoIngerido.class, idMedicamento);
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<MedicamentoIngerido> getListadoIngeridos(ListadoMedicamentoTomadosRequest filtro, long idPaciente, boolean pagina) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<MedicamentoIngerido> criteria = builder.createQuery(MedicamentoIngerido.class);
			Root<MedicamentoIngerido> medicionRoot = criteria.from(MedicamentoIngerido.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<MedicamentoIngerido, Paciente> joinPaciente = medicionRoot.join("paciente", JoinType.INNER);
			condicionesWhere.add(builder.equal(joinPaciente.get("id"), idPaciente));

			if (!UtilesString.isNullOrEmpty(filtro.getFechaHoraDesde())) {
				condicionesWhere.add(builder.greaterThanOrEqualTo(medicionRoot.get("fechaHora"), UtilesFecha.getLocalDateTimeFromString(filtro.getFechaHoraDesde())));
			}

			if (!UtilesString.isNullOrEmpty(filtro.getFechaHoraHasta())) {
				condicionesWhere.add(builder.lessThanOrEqualTo(medicionRoot.get("fechaHora"), UtilesFecha.getLocalDateTimeFromString(filtro.getFechaHoraHasta())));
			}

			if (filtro.getMedicamento() != null) {
				Join<MedicamentoIngerido, Medicamento> joinMedicamento = medicionRoot.join("medicamento", JoinType.INNER);
				Predicate droga = builder.like(joinMedicamento.get("droga"), "%" + filtro.getMedicamento() + "%");
				Predicate marca = builder.like(joinMedicamento.get("marca"), "%" + filtro.getMedicamento() + "%");
				Predicate presentacion = builder.like(joinMedicamento.get("presentacion"), "%" + filtro.getMedicamento() + "%");
				Predicate laboratorio = builder.like(joinMedicamento.get("laboratorio"), "%" + filtro.getMedicamento() + "%");

				condicionesWhere.add(builder.or(droga, marca, presentacion, laboratorio));
			}

			if (filtro.getCantidadDesde() != null) {
				condicionesWhere.add(builder.greaterThanOrEqualTo(medicionRoot.get("cantidadIngerida"), filtro.getCantidadDesde()));
			}

			if (filtro.getCantidadHasta() != null) {
				condicionesWhere.add(builder.lessThanOrEqualTo(medicionRoot.get("cantidadIngerida"), filtro.getCantidadHasta()));
			}

			criteria.orderBy(builder.desc(medicionRoot.get("fechaHora")));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(medicionRoot);
			TypedQuery<MedicamentoIngerido> query = session.createQuery(criteria);
			if (pagina) {
				query.setFirstResult(filtro.getNumeroPagina() * filtro.getRegistrosPorPagina());
				query.setMaxResults(filtro.getRegistrosPorPagina());
			}
			return query.getResultList();
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

}
