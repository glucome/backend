package com.glucome.dao;

import com.glucome.api.request.ListadoMedicionRequest;
import com.glucome.enumerador.MomentoMedicion;
import com.glucome.enumerador.TipoMedicion;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Medicion;
import com.glucome.modelo.Paciente;
import com.glucome.utils.Constantes;
import com.glucome.utils.UtilesFecha;
import com.glucome.utils.UtilesString;
import com.glucome.wrapper.HemoglobinaGlicosiladaAnualWrapper;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static java.time.temporal.TemporalAdjusters.*;

@Repository("MedicionDAO")
public class MedicionDAO extends BaseDAO {

	private final int CANTIDAD_ULTIMAS_MEDICIONES = 10;

	public List<Medicion> getListado(ListadoMedicionRequest filtro, long idPaciente, boolean pagina) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Medicion> criteria = builder.createQuery(Medicion.class);
			Root<Medicion> medicionRoot = criteria.from(Medicion.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<Medicion, Paciente> joinPaciente = medicionRoot.join("paciente", JoinType.INNER);
			condicionesWhere.add(builder.equal(joinPaciente.get("id"), idPaciente));

			if (!UtilesString.isNullOrEmpty(filtro.getFechaHoraDesde())) {
				condicionesWhere.add(builder.greaterThanOrEqualTo(medicionRoot.get("fechaHora"), UtilesFecha.getLocalDateTimeFromString(filtro.getFechaHoraDesde())));
			}

			if (!UtilesString.isNullOrEmpty(filtro.getFechaHoraHasta())) {
				condicionesWhere.add(builder.lessThanOrEqualTo(medicionRoot.get("fechaHora"), UtilesFecha.getLocalDateTimeFromString(filtro.getFechaHoraHasta())));
			}

			if (filtro.getValorDesde() != null) {
				condicionesWhere.add(builder.greaterThanOrEqualTo(medicionRoot.get("valor"), filtro.getValorDesde()));
			}

			if (filtro.getValorHasta() != null) {
				condicionesWhere.add(builder.lessThanOrEqualTo(medicionRoot.get("valor"), filtro.getValorHasta()));
			}

			if (filtro.getTipo() != null) {
				condicionesWhere.add(builder.equal(medicionRoot.get("tipo"), TipoMedicion.getById(filtro.getTipo())));
			}

			if (filtro.getMomento() != null) {
				condicionesWhere.add(builder.equal(medicionRoot.get("momento"), MomentoMedicion.getById(filtro.getMomento())));
			}

			criteria.orderBy(builder.desc(medicionRoot.get("fechaHora")));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(medicionRoot);
			TypedQuery<Medicion> query = session.createQuery(criteria);
			if (pagina) {
				query.setFirstResult(filtro.getNumeroPagina() * filtro.getRegistrosPorPagina());
				query.setMaxResults(filtro.getRegistrosPorPagina());
			}
			return query.getResultList();
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<Medicion> getListadoSinFiltro(long idPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Medicion> criteria = builder.createQuery(Medicion.class);
			Root<Medicion> medicionRoot = criteria.from(Medicion.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<Medicion, Paciente> joinPaciente = medicionRoot.join("paciente", JoinType.INNER);
			condicionesWhere.add(builder.equal(joinPaciente.get("id"), idPaciente));

			criteria.orderBy(builder.desc(medicionRoot.get("fechaHora")));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(medicionRoot);
			TypedQuery<Medicion> query = session.createQuery(criteria);
			List<Medicion> mediciones = query.getResultList();
			return mediciones;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public Medicion getUltimaMedicion(long idPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Medicion> criteria = builder.createQuery(Medicion.class);
			Root<Medicion> medicionRoot = criteria.from(Medicion.class);
			List<Predicate> condicionesWhere = new ArrayList<>();

			Join<Medicion, Paciente> joinPaciente = medicionRoot.join("paciente", JoinType.INNER);
			condicionesWhere.add(builder.equal(joinPaciente.get("id"), idPaciente));
			condicionesWhere.add(builder.lessThanOrEqualTo(medicionRoot.get("fechaHora"), LocalDateTime.now()));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(medicionRoot);
			criteria.orderBy(builder.desc(medicionRoot.get("fechaHora")));
			TypedQuery<Medicion> query = session.createQuery(criteria);
			query.setMaxResults(1);
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}

	}

	public List<Medicion> getUltimasMediciones(long idPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Medicion> criteria = builder.createQuery(Medicion.class);
			Root<Medicion> medicionRoot = criteria.from(Medicion.class);
			List<Predicate> condicionesWhere = new ArrayList<>();

			Join<Medicion, Paciente> joinPaciente = medicionRoot.join("paciente", JoinType.INNER);
			condicionesWhere.add(builder.equal(joinPaciente.get("id"), idPaciente));
			condicionesWhere.add(builder.lessThanOrEqualTo(medicionRoot.get("fechaHora"), LocalDateTime.now()));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(medicionRoot);
			criteria.orderBy(builder.desc(medicionRoot.get("fechaHora")));
			TypedQuery<Medicion> query = session.createQuery(criteria);
			query.setMaxResults(CANTIDAD_ULTIMAS_MEDICIONES);
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public double getGlucosaPromedioUltimos3Meses(long idPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Double> criteria = builder.createQuery(Double.class);
			Root<Medicion> medicionRoot = criteria.from(Medicion.class);
			List<Predicate> condicionesWhere = new ArrayList<>();

			Join<Medicion, Paciente> joinPaciente = medicionRoot.join("paciente", JoinType.INNER);
			condicionesWhere.add(builder.equal(joinPaciente.get("id"), idPaciente));
			condicionesWhere.add(builder.greaterThanOrEqualTo(medicionRoot.get("fechaHora"), LocalDateTime.now().minusMonths(Constantes.MESES_HEMOGLOBINA_GLICOSILADA)));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));

			Expression<Double> promedio = builder.avg(medicionRoot.get("valor"));
			criteria.multiselect(builder.selectCase().when(promedio.isNotNull(), promedio).otherwise(0));
			TypedQuery<Double> query = session.createQuery(criteria);
			query.setMaxResults(1);
			return query.getSingleResult();
		} catch (NoResultException e) {
			return 0;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public HemoglobinaGlicosiladaAnualWrapper getHemoglobinaGlicosiladaAnual(long idPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		final LocalDateTime primerDiaAnio = LocalDate.now().with(firstDayOfYear()).atStartOfDay();
		Month mesActual = LocalDate.now().getMonth();
		int trimestreActual = mesActual.equals(Month.JANUARY) || mesActual.equals(Month.FEBRUARY) ? 0 : -1;

		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<HemoglobinaGlicosiladaAnualWrapper> criteria = builder.createQuery(HemoglobinaGlicosiladaAnualWrapper.class);
			Root<Medicion> medicionRoot = criteria.from(Medicion.class);
			List<Predicate> condicionesWhere = new ArrayList<>();

			Join<Medicion, Paciente> joinPaciente = medicionRoot.join("paciente", JoinType.INNER);
			condicionesWhere.add(builder.equal(joinPaciente.get("id"), idPaciente));
			condicionesWhere.add(builder.greaterThanOrEqualTo(medicionRoot.get("fechaHora"), primerDiaAnio));
			condicionesWhere.add(builder.lessThanOrEqualTo(medicionRoot.get("fechaHora"), LocalDate.now().with(lastDayOfYear()).atStartOfDay()));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));

			Path<LocalDateTime> fechaHoraMedicion = medicionRoot.get("fechaHora");
			List<Selection<?>> promedios = new ArrayList<>();

			for (int i = 0; i < 10; i++) {
				LocalDateTime inicio = primerDiaAnio.plusMonths(i);
				LocalDateTime fin = primerDiaAnio.plusMonths(i + 2).with(lastDayOfMonth()).plusDays(1).minusSeconds(1);

				if(trimestreActual == -1 && mesActual.equals(fin.getMonth())) {
					trimestreActual = i;
				}

				promedios.add(getExpression3Meses(builder, medicionRoot, fechaHoraMedicion, inicio, fin));
			}

			criteria.multiselect(promedios);
			TypedQuery<HemoglobinaGlicosiladaAnualWrapper> query = session.createQuery(criteria);
			HemoglobinaGlicosiladaAnualWrapper hemoglobinaGlicosiladaAnual = query.getSingleResult();
			hemoglobinaGlicosiladaAnual.setTrimestreActual(trimestreActual);
			return hemoglobinaGlicosiladaAnual;
		} catch (NoResultException e) {
			return new HemoglobinaGlicosiladaAnualWrapper();
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	private static Expression<Number> getExpression3Meses(CriteriaBuilder builder, Root<Medicion> medicionRoot, Expression<LocalDateTime> fechaHoraMedicion, LocalDateTime inicio, LocalDateTime fin) {
		Expression<Number> suma = builder.coalesce(builder.sum(builder.<Double>selectCase().when(builder.between(fechaHoraMedicion, inicio, fin), medicionRoot.get("valor")).otherwise((double) 0)), 0);
		Expression<Long> cantidad = builder.coalesce(builder.sum(builder.<Long>selectCase().when(builder.between(fechaHoraMedicion, inicio, fin), 1L).otherwise(0L)), 0L);
		Expression<Number> promedio = builder.coalesce(builder.quot(suma, cantidad), 0L);
		Expression<Number> hemoglobinaGlicosilada = builder.sum(promedio, Constantes.PRIMER_TERMINO_HEMOGLOBINA_GLICOSILADA);
		hemoglobinaGlicosilada = builder.quot(hemoglobinaGlicosilada, Constantes.SEGUNDO_TERMINO_HEMOGLOBINA_GLICOSILADA);
		return hemoglobinaGlicosilada;
	}
}
