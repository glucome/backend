package com.glucome.dao;

import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Medico;
import com.glucome.modelo.Paciente;
import com.glucome.modelo.Usuario;
import com.glucome.utils.UtilesString;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository("MedicoDAO")
public class MedicoDAO extends BaseDAO {

    public List<Medico> getListado(String nombre) throws PersistenciaException {
        Session session = entityManager.unwrap(Session.class);
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Medico> criteria = builder.createQuery(Medico.class);
            Root<Medico> medicoRoot = criteria.from(Medico.class);
            List<Predicate> condicionesWhere = new ArrayList<>();

            if (!UtilesString.isNullOrEmpty(nombre)) {
                condicionesWhere.add(builder.like(builder.lower(medicoRoot.get("nombre")), "%" + nombre.toLowerCase() + "%"));
            }

            criteria.where(condicionesWhere.toArray(new Predicate[]{}));
            criteria.select(medicoRoot);
            TypedQuery<Medico> query = session.createQuery(criteria);
            return query.getResultList();
        } catch (Exception e) {
            throw new PersistenciaException(e.getMessage());
        }
    }

    public Medico get(long id) throws PersistenciaException {
        Session session = entityManager.unwrap(Session.class);
        try {
            return (Medico) session.get(Medico.class, id);
        } catch (Exception e) {
            throw new PersistenciaException(e.getMessage());
        }
    }

    public List<Medico> getListado(long idPaciente) throws PersistenciaException {
        Session session = entityManager.unwrap(Session.class);
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Medico> criteria = builder.createQuery(Medico.class);
            Root<Paciente> pacienteRoot = criteria.from(Paciente.class);
            List<Predicate> condicionesWhere = new ArrayList<>();
            Join<Paciente, Medico> medicoRoot = pacienteRoot.join("medicos", JoinType.INNER);
            condicionesWhere.add(builder.equal(pacienteRoot.get("id"), idPaciente));
            criteria.where(condicionesWhere.toArray(new Predicate[]{}));
            criteria.select(medicoRoot);
            TypedQuery<Medico> query = session.createQuery(criteria);
            return query.getResultList();
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw new PersistenciaException(e.getMessage());
        }
    }

    public Medico getByPreferenceId(String preferenceId) throws PersistenciaException {
        Session session = entityManager.unwrap(Session.class);
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Medico> criteria = builder.createQuery(Medico.class);
            Root<Medico> medicoRoot = criteria.from(Medico.class);
            Join<Medico, Usuario> joinUsuario = medicoRoot.join("usuario");
            List<Predicate> condicionesWhere = new ArrayList<>();

           condicionesWhere.add(builder.like(joinUsuario.get("preferenceId"), preferenceId));

            criteria.where(condicionesWhere.toArray(new Predicate[]{}));
            criteria.select(medicoRoot);
            TypedQuery<Medico> query = session.createQuery(criteria);
            List<Medico> medico = query.getResultList();
            return !medico.isEmpty() ? medico.get(0) : null;
        } catch (Exception e) {
            throw new PersistenciaException(e.getMessage());
        }
    }

    public String getUbicacion(Long idMedico) throws PersistenciaException {
        Session session = entityManager.unwrap(Session.class);
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<String> criteria = builder.createQuery(String.class);
            Root<Medico> medicoRoot = criteria.from(Medico.class);
            List<Predicate> condicionesWhere = new ArrayList<>();

            condicionesWhere.add(builder.equal(medicoRoot.get("id"), idMedico));

            criteria.where(condicionesWhere.toArray(new Predicate[]{}));
            criteria.select(medicoRoot.get("ubicacion"));
            TypedQuery<String> query = session.createQuery(criteria);
            List<String> ubicacion = query.getResultList();
            return !ubicacion.isEmpty() ? ubicacion.get(0) : null;
        } catch (Exception e) {
            throw new PersistenciaException(e.getMessage());
        }
    }
}
