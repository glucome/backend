package com.glucome.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Allegado;
import com.glucome.modelo.FactorCorreccionInsulina;
import com.glucome.modelo.Medicion;
import com.glucome.modelo.Medico;
import com.glucome.modelo.Paciente;
import com.glucome.modelo.Usuario;
import com.glucome.utils.Constantes;
import com.glucome.wrapper.LogMedicionWrapper;

@Repository("PacienteDAO")
public class PacienteDAO extends BaseDAO {

	public Paciente get(long id) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			return (Paciente) session.get(Paciente.class, id);
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<Paciente> getListado(long idMedico) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Paciente> criteria = builder.createQuery(Paciente.class);
			Root<Paciente> pacienteRoot = criteria.from(Paciente.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<Paciente, Medico> medicoRoot = pacienteRoot.join("medicos", JoinType.INNER);
			condicionesWhere.add(builder.equal(medicoRoot.get("id"), idMedico));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(pacienteRoot);
			criteria.orderBy(builder.asc(pacienteRoot.get("nombre")));
			TypedQuery<Paciente> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<Paciente> getListadoDelAllegado(long idAllegado) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Paciente> criteria = builder.createQuery(Paciente.class);
			Root<Paciente> pacienteRoot = criteria.from(Paciente.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<Paciente, Allegado> allegadoRoot = pacienteRoot.join("allegados", JoinType.INNER);
			condicionesWhere.add(builder.equal(allegadoRoot.get("id"), idAllegado));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(pacienteRoot);
			TypedQuery<Paciente> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public FactorCorreccionInsulina getFactorInsulina(long idPaciente) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<FactorCorreccionInsulina> criteria = builder.createQuery(FactorCorreccionInsulina.class);
			Root<FactorCorreccionInsulina> root = criteria.from(FactorCorreccionInsulina.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<FactorCorreccionInsulina, Paciente> pacienteRoot = root.join("paciente", JoinType.INNER);
			condicionesWhere.add(builder.equal(pacienteRoot.get("id"), idPaciente));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(root);
			TypedQuery<FactorCorreccionInsulina> query = session.createQuery(criteria);

			List<FactorCorreccionInsulina> resultado = query.getResultList();
			return resultado != null && !resultado.isEmpty() ? resultado.get(0) : null;
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<LogMedicionWrapper> getLogMediciones(long idMedico) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<LogMedicionWrapper> criteria = builder.createQuery(LogMedicionWrapper.class);
			Root<Medicion> medicionRoot = criteria.from(Medicion.class);
			List<Predicate> condicionesWhere = new ArrayList<>();
			Join<Medicion, Paciente> pacienteRoot = medicionRoot.join("paciente", JoinType.INNER);
			Join<Paciente, Usuario> usuarioRoot = pacienteRoot.join("usuario", JoinType.INNER);
			Join<Paciente, Medico> medicoRoot = pacienteRoot.join("medicos", JoinType.INNER);
			condicionesWhere.add(builder.equal(medicoRoot.get("id"), idMedico));
			condicionesWhere.add(builder.or(builder.lessThan(medicionRoot.get("valor"), Constantes.VALOR_MINIMO_MEDICION), builder.greaterThan(medicionRoot.get("valor"), Constantes.VALOR_MAXIMO_MEDICION)));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.orderBy(builder.desc(medicionRoot.get("fechaHora")));
			criteria.multiselect(pacienteRoot.get("nombre"), pacienteRoot.get("id"), usuarioRoot.get("id"), medicionRoot.get("valor"), medicionRoot.get("momento"), medicionRoot.get("fechaHora"));
			TypedQuery<LogMedicionWrapper> query = session.createQuery(criteria);
			query.setMaxResults(Constantes.CANTIDAD_MEDICIONES_LOG);
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

}
