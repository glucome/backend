package com.glucome.dao;

import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Pago;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository("pagoDAO")
public class PagoDAO extends BaseDAO {

	public List<Pago> getPagosSinNotificar(long idUsuario) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Pago> criteria = builder.createQuery(Pago.class);
			Root<Pago> pagoRoot = criteria.from(Pago.class);
			List<Predicate> condicionesWhere = new ArrayList<>();

			condicionesWhere.add(builder.equal(pagoRoot.get("notificado"), false));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.orderBy(builder.desc(pagoRoot.get("fechaHora")));

			TypedQuery<Pago> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<Pago> getPagos(List<Long> idsPagos) throws PersistenciaException {
		if(idsPagos.isEmpty()) {
			return new ArrayList<Pago>();
		}

		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Pago> criteria = builder.createQuery(Pago.class);
			Root<Pago> pagoRoot = criteria.from(Pago.class);
			List<Predicate> condicionesWhere = new ArrayList<>();

			condicionesWhere.add(pagoRoot.get("id").in(idsPagos));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));

			TypedQuery<Pago> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage());
		}
	}

	public void marcarPagosComoNotificados(List<Pago> pagos) throws PersistenciaException {
		for(Pago pago : pagos) {
			pago.setNotificado(true);
			grabar(pago);
		}
	}
}
