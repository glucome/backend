package com.glucome.dao;

import com.glucome.wrapper.*;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("reporteDAO")
public class ReporteDAO extends BaseDAO {

	@Value("${spring.datasource.url}")
	private String MYSQL_URL;

	@Value("${spring.datasource.username}")
	private String MYSQL_USERNAME;

	@Value("${spring.datasource.password}")
	private String MYSQL_PWD;

	private static final SparkSession spark;

	static {
		spark = SparkSession.builder().master("local").appName("GlucoMe").getOrCreate();
	}

	public List<ReporteCantidadMedicionesWrapper> getReporteCantidadMediciones() {
		Dataset<Row> medicion = spark.read().format("jdbc").option("url", MYSQL_URL).option("user", MYSQL_USERNAME).option("password", MYSQL_PWD).option("dbtable", "(SELECT valor, sexo FROM medicion m INNER JOIN paciente p ON m.paciente_id = p.id) as medicion").load();

		Dataset<Row> analisis = medicion.groupBy("sexo", "valor").count();

		List<ReporteCantidadMedicionesWrapper> reporte = new ArrayList<>();
		for (Row row : analisis.collectAsList()) {
			int sexo = row.getInt(0);
			int valor = row.getInt(1);
			long cantidad = row.getLong(2);

			reporte.add(new ReporteCantidadMedicionesWrapper(sexo, valor, cantidad));
		}

		return reporte;
	}

	public List<ReportePorMomentoWrapper> getReportePorMomento() {
		Dataset<Row> medicion = spark.read().format("jdbc").option("url", MYSQL_URL).option("user", MYSQL_USERNAME).option("password", MYSQL_PWD).option("dbtable", "(SELECT sexo, valor, momento FROM medicion m INNER JOIN paciente p ON m.paciente_id = p.id) as medicion").load();

		Dataset<Row> analisis = medicion.groupBy("sexo", "momento").avg("valor").orderBy("momento");

		List<ReportePorMomentoWrapper> reporte = new ArrayList<>();
		for (Row row : analisis.collectAsList()) {
			int sexo = row.getInt(0);
			int momento = row.getInt(1);
			double promedio = row.getDouble(2);

			reporte.add(new ReportePorMomentoWrapper(sexo, momento, promedio));
		}

        return reporte;
    }

    public List<ReportePorRangoEdadWrapper> getReportePorRangoEdad() {
        Dataset<Row> medicion = spark.read()
                .format("jdbc")
                .option("url", MYSQL_URL)
                .option("user", MYSQL_USERNAME)
                .option("password", MYSQL_PWD)
                .option("dbtable",
                        "(SELECT sexo, valor, momento, " +
                                "CASE" +
                                "   WHEN YEAR(CURDATE()) - YEAR(fecha_nacimiento) BETWEEN 0 AND 9 THEN '0'" +
                                "   WHEN YEAR(CURDATE()) - YEAR(fecha_nacimiento) BETWEEN 10 AND 24 THEN '1'" +
                                "   WHEN YEAR(CURDATE()) - YEAR(fecha_nacimiento) BETWEEN 25 AND 34 THEN '2'" +
                                "   WHEN YEAR(CURDATE()) - YEAR(fecha_nacimiento) BETWEEN 35 AND 44 THEN '3'" +
                                "   WHEN YEAR(CURDATE()) - YEAR(fecha_nacimiento) BETWEEN 45 AND 54 THEN '4'" +
                                "   WHEN YEAR(CURDATE()) - YEAR(fecha_nacimiento) BETWEEN 55 AND 64 THEN '5'" +
                                "   WHEN YEAR(CURDATE()) - YEAR(fecha_nacimiento) BETWEEN 65 AND 74 THEN '6'" +
                                "   WHEN YEAR(CURDATE()) - YEAR(fecha_nacimiento) BETWEEN 75 AND 84 THEN '7'" +
                                "   WHEN YEAR(CURDATE()) - YEAR(fecha_nacimiento) BETWEEN 85 AND 94 THEN '8'" +
                                "   WHEN YEAR(CURDATE()) - YEAR(fecha_nacimiento) BETWEEN 95 AND 104 THEN '9'" +
                                "   WHEN YEAR(CURDATE()) - YEAR(fecha_nacimiento) BETWEEN 105 AND 114 THEN '10'" +
                                "   ELSE '10'" +
                                "END AS edad " +
                                "FROM medicion m INNER JOIN paciente p ON m.paciente_id = p.id" +
                        ") as medicion")
                .load();

        Dataset<Row> analisis = medicion.groupBy("sexo", "edad", "momento").avg("valor").orderBy("edad", "momento");

        List<ReportePorRangoEdadWrapper> reporte = new ArrayList<>();
        for(Row row : analisis.collectAsList()) {
            int sexo = row.getInt(0);
            String rangoEdad = row.getString(1);
            int momento = row.getInt(2);
            double promedio = row.getDouble(3);

            reporte.add(new ReportePorRangoEdadWrapper(sexo, momento, rangoEdad, promedio));
        }

        return reporte;
    }

	public List<ReportePorTipoDiabetesWrapper> getReportePorTipoDiabetes() {
		Dataset<Row> medicion = spark.read().format("jdbc").option("url", MYSQL_URL).option("user", MYSQL_USERNAME).option("password", MYSQL_PWD).option("dbtable", "(SELECT sexo, valor, momento, tipo_diabetes FROM medicion m INNER JOIN paciente p ON m.paciente_id = p.id) as medicion").load();

		Dataset<Row> analisis = medicion.groupBy("sexo", "tipo_diabetes", "momento").avg("valor").orderBy("momento");

		List<ReportePorTipoDiabetesWrapper> reporte = new ArrayList<>();
		for (Row row : analisis.collectAsList()) {
			int sexo = row.getInt(0);
			int tipoDiabetes = row.getInt(1);
			int momento = row.getInt(2);
			double promedio = row.getDouble(3);

			reporte.add(new ReportePorTipoDiabetesWrapper(sexo, tipoDiabetes, momento, promedio));
		}

		return reporte;
	}

	public List<ReportePorTipoMedicionWrapper> getReportePorTipoMedicion() {
		Dataset<Row> medicion = spark.read().format("jdbc").option("url", MYSQL_URL).option("user", MYSQL_USERNAME).option("password", MYSQL_PWD).option("dbtable", "(SELECT sexo, valor, momento, tipo FROM medicion m INNER JOIN paciente p ON m.paciente_id = p.id) as medicion").load();

		Dataset<Row> analisis = medicion.groupBy("sexo", "tipo", "momento").avg("valor").orderBy("momento");

		List<ReportePorTipoMedicionWrapper> reporte = new ArrayList<>();
		for (Row row : analisis.collectAsList()) {
			int sexo = row.getInt(0);
			int tipoMedicion = row.getInt(1);
			int momento = row.getInt(2);
			double promedio = row.getDouble(3);

			reporte.add(new ReportePorTipoMedicionWrapper(sexo, tipoMedicion, momento, promedio));
		}

		return reporte;
	}

	public List<ReporteRankingAlimentosWrapper> getReporteRankingAlimentos() {
		Dataset<Row> alimento = spark.read().format("jdbc").option("url", MYSQL_URL).option("user", MYSQL_USERNAME).option("password", MYSQL_PWD).option("dbtable", "(SELECT id, nombre, descripcion, imagen FROM alimento) as alimento").load();
		Dataset<Row> alimentoIngerido = spark.read().format("jdbc").option("url", MYSQL_URL).option("user", MYSQL_USERNAME).option("password", MYSQL_PWD).option("dbtable", "(SELECT alimento_id, momento FROM alimento_ingerido) as alimento_ingerido").load();

		Dataset<Row> ranking = alimentoIngerido.groupBy("momento", "alimento_id").count();
		Dataset<Row> result = ranking.join(alimento, ranking.col("alimento_id").equalTo(alimento.col("id")), "left");
		result.orderBy(ranking.col("momento"), functions.desc("count"));

		List<ReporteRankingAlimentosWrapper> reporte = new ArrayList<>();
		for (Row row : result.collectAsList()) {
			long id = row.getLong(1);
			String nombre = row.getString(4);
			String descripcion = row.getString(5);
			int momento = row.getInt(0);
			long vecesIngerido = row.getLong(2);

			reporte.add(new ReporteRankingAlimentosWrapper(id, nombre, descripcion, momento, vecesIngerido));
		}

		return reporte;
	}
}
