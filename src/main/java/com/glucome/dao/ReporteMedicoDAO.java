package com.glucome.dao;

import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.*;
import com.glucome.wrapper.*;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository("ReporteMedicoDAO")
public class ReporteMedicoDAO extends BaseDAO {


	public List<ReporteMedicoPorMomentoWrapper> getReportePorMomento(long idMedico) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ReporteMedicoPorMomentoWrapper> criteria = builder.createQuery(ReporteMedicoPorMomentoWrapper.class);
			Root<Medicion> medicion = criteria.from(Medicion.class);
			Join<Medicion, Paciente> pacienteJoin = medicion.join("paciente");
			Join<Paciente, Medico> medicoJoin = pacienteJoin.join("medicos");
			List<Predicate> condicionesWhere = new ArrayList<>();

			condicionesWhere.add(medicoJoin.get("id").in(idMedico));

			Expression momento = medicion.get("momento");
			Expression promedio = builder.avg(medicion.get("valor")).as(Double.class);

			criteria.multiselect(momento, promedio);
			criteria.groupBy(momento);
			criteria.orderBy(builder.asc(momento));

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			TypedQuery<ReporteMedicoPorMomentoWrapper> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<ReporteMedicoPorRangoEdadWrapper> getReportePorRangoEdad(long idMedico) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ReporteMedicoPorRangoEdadWrapper> criteria = builder.createQuery(ReporteMedicoPorRangoEdadWrapper.class);
			Root<Medicion> medicion = criteria.from(Medicion.class);
			Join<Medicion, Paciente> pacienteJoin = medicion.join("paciente");
			Join<Paciente, Medico> medicoJoin = pacienteJoin.join("medicos");
			List<Predicate> condicionesWhere = new ArrayList<>();

			condicionesWhere.add(medicoJoin.get("id").in(idMedico));

			Expression momento = medicion.get("momento");
			Expression promedio = builder.avg(medicion.get("valor")).as(Double.class);

			Expression fechaActual = builder.function("YEAR", Integer.class, builder.currentDate());
			Expression fechaNacimiento = builder.function("YEAR", Integer.class, pacienteJoin.get("fechaNacimiento"));
			Expression edad = builder.diff(fechaActual, fechaNacimiento);

			Expression rangoEdad = builder.selectCase().when(builder.between(edad, 0, 9), 0)
					.when(builder.between(edad, 10, 24), 1)
					.when(builder.between(edad, 25, 34), 2)
					.when(builder.between(edad, 35, 44), 3)
					.when(builder.between(edad, 45, 54), 4)
					.when(builder.between(edad, 55, 64), 5)
					.when(builder.between(edad, 65, 74), 6)
					.when(builder.between(edad, 75, 84), 7)
					.when(builder.between(edad, 85, 94), 8)
					.when(builder.between(edad, 95, 104), 9)
					.when(builder.between(edad, 105, 114), 10)
					.otherwise(10);

			criteria.multiselect(momento, rangoEdad, promedio);
			criteria.groupBy(edad, momento);
			criteria.orderBy(builder.asc(edad), builder.asc(momento));

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			TypedQuery<ReporteMedicoPorRangoEdadWrapper> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<ReporteMedicoPorTipoDiabetesWrapper> getReportePorTipoDiabetes(long idMedico) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ReporteMedicoPorTipoDiabetesWrapper> criteria = builder.createQuery(ReporteMedicoPorTipoDiabetesWrapper.class);
			Root<Medicion> medicion = criteria.from(Medicion.class);
			Join<Medicion, Paciente> pacienteJoin = medicion.join("paciente");
			Join<Paciente, Medico> medicoJoin = pacienteJoin.join("medicos");
			List<Predicate> condicionesWhere = new ArrayList<>();

			condicionesWhere.add(medicoJoin.get("id").in(idMedico));

			Expression tipoDiabetes = pacienteJoin.get("tipoDiabetes");
			Expression momento = medicion.get("momento");
			Expression promedio = builder.avg(medicion.get("valor")).as(Double.class);

			criteria.multiselect(tipoDiabetes, momento, promedio);
			criteria.groupBy(tipoDiabetes, momento);
			criteria.orderBy(builder.asc(momento));

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			TypedQuery<ReporteMedicoPorTipoDiabetesWrapper> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<ReporteMedicoPorTipoMedicionWrapper> getReportePorTipoMedicion(long idMedico) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ReporteMedicoPorTipoMedicionWrapper> criteria = builder.createQuery(ReporteMedicoPorTipoMedicionWrapper.class);
			Root<Medicion> medicion = criteria.from(Medicion.class);
			Join<Medicion, Paciente> pacienteJoin = medicion.join("paciente");
			Join<Paciente, Medico> medicoJoin = pacienteJoin.join("medicos");
			List<Predicate> condicionesWhere = new ArrayList<>();

			condicionesWhere.add(medicoJoin.get("id").in(idMedico));

			Expression tipoMedicion = medicion.get("tipo");
			Expression momento = medicion.get("momento");
			Expression promedio = builder.avg(medicion.get("valor")).as(Double.class);

			criteria.multiselect(tipoMedicion, momento, promedio);
			criteria.groupBy(tipoMedicion, momento);
			criteria.orderBy(builder.asc(momento));

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			TypedQuery<ReporteMedicoPorTipoMedicionWrapper> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<ReporteMedicoRankingAlimentosWrapper> getReporteRankingAlimentos(long idMedico) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ReporteMedicoRankingAlimentosWrapper> criteria = builder.createQuery(ReporteMedicoRankingAlimentosWrapper.class);
			Root<AlimentoIngerido> alimentoIngeridoRoot = criteria.from(AlimentoIngerido.class);
			Join<AlimentoIngerido, Alimento> alimentoJoin = alimentoIngeridoRoot.join("alimento");
			Join<AlimentoIngerido, Paciente> pacienteJoin = alimentoIngeridoRoot.join("paciente");
			Join<Paciente, Medico> medicoJoin = pacienteJoin.join("medicos");
			List<Predicate> condicionesWhere = new ArrayList<>();

			condicionesWhere.add(medicoJoin.get("id").in(idMedico));

			Expression nombre = alimentoJoin.get("nombre");
			Expression descripcion = alimentoJoin.get("descripcion");
			Expression imagen = alimentoJoin.get("imagen");
			Expression momento = alimentoIngeridoRoot.get("momento");
			Expression vecesIngerido = builder.count(alimentoJoin.get("id"));

			criteria.multiselect(nombre, descripcion, imagen, momento, vecesIngerido);
			criteria.groupBy(momento, alimentoJoin.get("id"));
			criteria.orderBy(builder.asc(momento), builder.desc(vecesIngerido));

			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			TypedQuery<ReporteMedicoRankingAlimentosWrapper> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}
}
