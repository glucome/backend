package com.glucome.dao;

import com.glucome.enumerador.EstadoSolicitud;
import com.glucome.enumerador.TipoSolicitud;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.*;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository("SolicitudDAO")
public class SolicitudDAO extends BaseDAO {

	public SolicitudVinculacion get(long id) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			return (SolicitudVinculacion) session.get(SolicitudVinculacion.class, id);
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public SolicitudVinculacion get(long idSolicitante, long idSolicitado, TipoSolicitud tipo) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {

			return tipo.equals(TipoSolicitud.MEDICO_PACIENTE) ? getSolicitudMedicoPaciente(idSolicitante, idSolicitado, session) : getSolicitudAllegadoPaciente(idSolicitante, idSolicitado, session);
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	private SolicitudVinculacionMedicoPaciente getSolicitudMedicoPaciente(long idSolicitante, long idSolicitado, Session session) {
		CriteriaBuilder builder = session.getCriteriaBuilder();
		TypedQuery<SolicitudVinculacionMedicoPaciente> query;
		CriteriaQuery<SolicitudVinculacionMedicoPaciente> criteria = builder.createQuery(SolicitudVinculacionMedicoPaciente.class);
		Root<SolicitudVinculacionMedicoPaciente> solicitudRoot = criteria.from(SolicitudVinculacionMedicoPaciente.class);
		Join<SolicitudVinculacionMedicoPaciente, Medico> medicoRoot = solicitudRoot.join("medico", JoinType.INNER);
		Join<SolicitudVinculacionMedicoPaciente, Paciente> pacienteRoot = solicitudRoot.join("paciente", JoinType.INNER);
		List<Predicate> condicionesWhere = new ArrayList<>();
		condicionesWhere.add(builder.equal(medicoRoot.get("id"), idSolicitado));
		condicionesWhere.add(builder.equal(pacienteRoot.get("id"), idSolicitante));
		criteria.where(condicionesWhere.toArray(new Predicate[] {}));
		criteria.select(solicitudRoot);
		query = session.createQuery(criteria);
		return query.getSingleResult();
	}

	private SolicitudVinculacionAllegadoPaciente getSolicitudAllegadoPaciente(long idSolicitante, long idSolicitado, Session session) {
		CriteriaBuilder builder = session.getCriteriaBuilder();
		TypedQuery<SolicitudVinculacionAllegadoPaciente> query;
		CriteriaQuery<SolicitudVinculacionAllegadoPaciente> criteria = builder.createQuery(SolicitudVinculacionAllegadoPaciente.class);
		Root<SolicitudVinculacionAllegadoPaciente> solicitudRoot = criteria.from(SolicitudVinculacionAllegadoPaciente.class);
		Join<SolicitudVinculacionAllegadoPaciente, Allegado> allegadoRoot = solicitudRoot.join("allegado", JoinType.INNER);
		Join<SolicitudVinculacionAllegadoPaciente, Paciente> pacienteRoot = solicitudRoot.join("paciente", JoinType.INNER);
		List<Predicate> condicionesWhere = new ArrayList<>();
		condicionesWhere.add(builder.equal(allegadoRoot.get("id"), idSolicitado));
		condicionesWhere.add(builder.equal(pacienteRoot.get("id"), idSolicitante));
		criteria.where(condicionesWhere.toArray(new Predicate[] {}));
		criteria.select(solicitudRoot);
		query = session.createQuery(criteria);
		return query.getSingleResult();
	}

	public List<SolicitudVinculacion> getListadoDeSolicitudesMedico(long idMedico) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<SolicitudVinculacion> criteria = builder.createQuery(SolicitudVinculacion.class);
			Root<SolicitudVinculacionMedicoPaciente> solicitudRoot = criteria.from(SolicitudVinculacionMedicoPaciente.class);
			Join<SolicitudVinculacionMedicoPaciente, Medico> medicoRoot = solicitudRoot.join("medico", JoinType.INNER);
			List<Predicate> condicionesWhere = new ArrayList<>();
			condicionesWhere.add(builder.equal(medicoRoot.get("id"), idMedico));
			condicionesWhere.add(builder.equal(solicitudRoot.get("estado"), EstadoSolicitud.PENDIENTE));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(solicitudRoot);
			TypedQuery<SolicitudVinculacion> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public List<SolicitudVinculacion> getListadoDeSolicitudesAllegado(long id) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<SolicitudVinculacion> criteria = builder.createQuery(SolicitudVinculacion.class);
			Root<SolicitudVinculacionAllegadoPaciente> solicitudRoot = criteria.from(SolicitudVinculacionAllegadoPaciente.class);
			Join<SolicitudVinculacionAllegadoPaciente, Allegado> medicoRoot = solicitudRoot.join("allegado", JoinType.INNER);
			List<Predicate> condicionesWhere = new ArrayList<>();
			condicionesWhere.add(builder.equal(medicoRoot.get("id"), id));
			condicionesWhere.add(builder.equal(solicitudRoot.get("estado"), EstadoSolicitud.PENDIENTE));
			criteria.where(condicionesWhere.toArray(new Predicate[] {}));
			criteria.select(solicitudRoot);
			TypedQuery<SolicitudVinculacion> query = session.createQuery(criteria);
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

}
