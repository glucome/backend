package com.glucome.dao;

import com.glucome.modelo.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<Usuario, Long> {

    Optional<Usuario> findByNombreUsuarioOrMail(String username, String mail);

    Optional<Usuario> findByNombreUsuario(String username);

    Optional<Usuario> findByMail(String mail);
}