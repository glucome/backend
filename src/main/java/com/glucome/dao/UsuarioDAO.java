package com.glucome.dao;

import com.glucome.enumerador.EstadoUsuario;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Usuario;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("UsuarioDAO")
public class UsuarioDAO extends BaseDAO {

	@Transactional
	public Usuario get(long id) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		try {
			return (Usuario) session.get(Usuario.class, id);
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage());
		}
	}

	public EstadoUsuario cambiarEstado(Usuario usuario, boolean online) throws PersistenciaException {
		Session session = entityManager.unwrap(Session.class);
		session.beginTransaction();

		usuario.setEstado(online ? EstadoUsuario.ONLINE : EstadoUsuario.OFFLINE);
		session.update(usuario);

		session.getTransaction().commit();

		return usuario.getEstado();
	}
}
