package com.glucome.enumerador;

import com.fasterxml.jackson.annotation.JsonValue;

public enum AccionInsulina {


    RAPIDA("Acción Rápida"),
    INTERMEDIA("Acción Intermedia"),
    PROLONGADA("Acción Prolongada");


    private final String nombre;

    private AccionInsulina(String nombre) {
        this.nombre = nombre;
    }

    public static AccionInsulina getById(int id) {
        for (AccionInsulina e : values()) {
            if (e.ordinal() == id) {
                return e;
            }
        }
        return null;
    }

    public String getNombre() {
        return nombre;
    }

    @JsonValue
    public int getId() {
        return ordinal();
    }


}
