package com.glucome.enumerador;

public enum EstadoPago {

    APROBADO("approved"),
    PENDIENTE("in_process"),
    RECHAZADDO("rejected");

    private String estadoMercadoPago;

    EstadoPago(String estadoMercadoPago) {
        this.estadoMercadoPago = estadoMercadoPago;
    }

    public String getEstadoMercadoPago() {
        return estadoMercadoPago;
    }

    public static EstadoPago getByEstadoMercadoPago(String estadoMercadoPago)  {
        for(EstadoPago estadoPago : values()) {
            if(estadoPago.getEstadoMercadoPago().equals(estadoMercadoPago)) {
                return estadoPago;
            }
        }

        return null;
    }
}
