package com.glucome.enumerador;

import com.fasterxml.jackson.annotation.JsonValue;

public enum EstadoSolicitud {
    PENDIENTE("Pendiente"), RECHAZADA("Rechazada"), ACEPTADA("Aceptada");

    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    private EstadoSolicitud(String descripcion) {
        this.descripcion = descripcion;
    }

    @JsonValue
    public int getId() {
        return ordinal();
    }

}
