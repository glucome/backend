package com.glucome.enumerador;

public enum EstadoUsuario {
    ONLINE,
    OFFLINE;
}
