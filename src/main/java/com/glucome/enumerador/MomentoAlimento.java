package com.glucome.enumerador;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonValue;

public enum MomentoAlimento {

	DESAYUNO(5, 11), ALMUERZO(12, 13), MERIENDA(14, 19), CENA(20, 24), OTRO(0, 5);

	private int horaInicio;
	private int horaFin;

	private MomentoAlimento(int horaInicio, int horaFin) {
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;

	}

	public int getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(int horaInicio) {
		this.horaInicio = horaInicio;
	}

	public int getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(int horaFin) {
		this.horaFin = horaFin;
	}

	public static List<MomentoAlimento> getMomentosActuales() {
		int horaActual = LocalDateTime.now().getHour();
		List<MomentoAlimento> momentosActuales = new ArrayList<>();
		for (MomentoAlimento momento : values()) {
			if (momento.horaInicio <= horaActual && momento.horaFin >= horaActual) {
				momentosActuales.add(momento);
			}
		}
		return momentosActuales;

	}

	@JsonValue
	public int getId() {
		return ordinal();
	}

	public static MomentoAlimento get(int id) {
		for (MomentoAlimento e : values()) {
			if (e.ordinal() == id) {
				return e;
			}
		}
		return MomentoAlimento.OTRO;
	}
}
