package com.glucome.enumerador;

import com.fasterxml.jackson.annotation.JsonValue;

public enum MomentoMedicion {

    ANTES_DESAYUNO("Antes del desayuno"),
    DESPUES_DESAYUNO("Después del desayuno"),
    ANTES_ALMUERZO("Antes del almuerzo"),
    DESPUES_ALMUERZO("Después del almuerzo"),
    ANTES_MERIENDA("Antes de la merienda"),
    DESPUES_MERIENDA("Después de la merienda"),
    ANTES_CENA("Antes de la cena"),
    DESPUES_CENA("Después de la cena"),
    NA("No aplica");

    private final String nombre;

    private MomentoMedicion(String nombre) {
        this.nombre = nombre;
    }

    public static MomentoMedicion getById(int id) {
        for (MomentoMedicion e : values()) {
            if (e.ordinal() == id) {
                return e;
            }
        }
        return MomentoMedicion.NA;
    }

    public String getNombre() {
        return nombre;
    }

    @JsonValue
    public int getId() {
        return ordinal();
    }

    public boolean esAntesDeComer() {
        return this.equals(ANTES_ALMUERZO) || this.equals(ANTES_CENA) || this.equals(ANTES_DESAYUNO) || this.equals(ANTES_MERIENDA);
    }


}
