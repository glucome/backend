package com.glucome.enumerador;

import com.glucome.utils.Constantes;
import org.springframework.http.HttpMethod;

public enum Permiso {

	REPORTE(HttpMethod.GET, Constantes.URL_BASE + "reporte/", Rol.MEDICO_PREMIUM.getNombreParaSecurityConfig()),
	SUSCRIPCION_MEDICO_GET(HttpMethod.GET, Constantes.URL_BASE + "suscripcion/medico/", Rol.MEDICO_PREMIUM.getNombreParaSecurityConfig(), Rol.MEDICO.getNombreParaSecurityConfig()),
	SUSCRIPCION_PACIENTE_GET(HttpMethod.GET, Constantes.URL_BASE + "suscripcion/paciente/", Rol.PACIENTE_PREMIUM.getNombreParaSecurityConfig(), Rol.PACIENTE.getNombreParaSecurityConfig()),
	SUSCRIPCION_MEDICO_POST(HttpMethod.POST, Constantes.URL_BASE + "suscripcion/medico/**", Rol.MEDICO_PREMIUM.getNombreParaSecurityConfig(), Rol.MEDICO.getNombreParaSecurityConfig()),
	SUSCRIPCION_PACIENTE_POST(HttpMethod.POST, Constantes.URL_BASE + "suscripcion/paciente/**", Rol.PACIENTE_PREMIUM.getNombreParaSecurityConfig(), Rol.PACIENTE.getNombreParaSecurityConfig());

	
	
	private HttpMethod metodo;

	private String url;

	private String[] roles;

	private Permiso(HttpMethod metodo, String url, String... roles) {
		this.metodo = metodo;
		this.url = url;
		this.roles = roles;
	}

	public HttpMethod getMetodo() {
		return metodo;
	}

	public void setMetodo(HttpMethod metodo) {
		this.metodo = metodo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String[] getRoles() {
		return roles;
	}

	public void setRoles(String[] roles) {
		this.roles = roles;
	}

}
