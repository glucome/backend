package com.glucome.enumerador;

import java.util.ArrayList;
import java.util.List;

public enum Plan {

	PACIENTE_BASICO("Paciente Básico", Rol.PACIENTE, 200.00), 
	MEDICO_BASICO("Médico Básico", Rol.MEDICO, 200.00), 
	ALLEGADO_BASICO("Allegado", Rol.ALLEGADO, 0.00), 
	PACIENTE_PREMIUM("Paciente premium", Rol.PACIENTE_PREMIUM, 500.00), 
	MEDICO_PREMIUM("Médico premium", Rol.MEDICO_PREMIUM, 500.00);

	private String nombre;

	private Rol rol;

	private double precio;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	private Plan(String nombre, Rol rol, double precio) {
		this.nombre = nombre;
		this.rol = rol;
		this.precio = precio;
	}

	public static List<Plan> get(TipoUsuario tipo) {
		List<Plan> planes = new ArrayList<>();
		List<Rol> roles = Rol.get(tipo);
		for (Plan plan : values()) {
			if (roles.contains(plan.getRol())) {
				planes.add(plan);
			}
		}
		return planes;
	}

	public static Plan get(long id) {
		for (Plan e : values()) {
			if (e.ordinal() == id) {
				return e;
			}
		}
		return null;
	}

}
