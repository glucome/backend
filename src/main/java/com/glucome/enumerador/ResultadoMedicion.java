package com.glucome.enumerador;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ResultadoMedicion {

    ALTA, MEDIA, BAJA;

    @JsonValue
    public int getId() {
        return ordinal();
    }
}
