package com.glucome.enumerador;

import java.util.ArrayList;
import java.util.List;

public enum Rol {

	PACIENTE("ROLE_PACIENTE_BASICO", TipoUsuario.PACIENTE, "PACIENTE_BASICO"), MEDICO("ROLE_MEDICO_BASICO", TipoUsuario.MEDICO, "MEDICO_BASICO"), ALLEGADO("ROLE_ALLEGADO", TipoUsuario.ALLEGADO, "ALLEGADO"), PACIENTE_PREMIUM("ROLE_PACIENTE_PREMIUM", TipoUsuario.PACIENTE, "PACIENTE_PREMIUM"), MEDICO_PREMIUM("ROLE_MEDICO_PREMIUM", TipoUsuario.MEDICO, "MEDICO_PREMIUM");

	private String nombre;

	private TipoUsuario tipoUsuario;

	private String nombreParaSecurityConfig;

	private Rol(String nombre, TipoUsuario tipoUsuario, String nombreParaSecurityConfig) {
		this.nombre = nombre;
		this.tipoUsuario = tipoUsuario;
		this.nombreParaSecurityConfig = nombreParaSecurityConfig;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreParaConfiguracion() {
		return tipoUsuario.getDescripcion();
	}

	public String getNombreParaSecurityConfig() {
		return nombreParaSecurityConfig;
	}

	public void setNombreParaSecurityConfig(String nombreParaSecurityConfig) {
		this.nombreParaSecurityConfig = nombreParaSecurityConfig;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public static List<Rol> get(TipoUsuario tipo) {
		List<Rol> roles = new ArrayList<>();
		for (Rol rol : values()) {
			if (rol.getTipoUsuario().ordinal() == tipo.ordinal()) {
				roles.add(rol);
			}
		}
		return roles;
	}

}
