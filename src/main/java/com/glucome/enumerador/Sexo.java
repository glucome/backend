package com.glucome.enumerador;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Pablo
 */
public enum Sexo {
    MASCULINO("Masculino"),
    FEMENINO("Femenino"),
    OTRO("Otro");

    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    private Sexo(String nombre) {
        this.nombre = nombre;
    }

    public static Sexo get(int sexo) {
        for (Sexo sexoEnum : values()) {
            if (sexoEnum.ordinal() == sexo) {
                return sexoEnum;
            }
        }

        return null;
    }

    @JsonValue
    public int getId() {
        return ordinal();
    }

}
