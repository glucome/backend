package com.glucome.enumerador;

import com.fasterxml.jackson.annotation.JsonValue;

public enum TipoDiabetes {

    TIPO_1("Tipo 1"),
    TIPO_2("Tipo 2"),
    GESTACIONAL("Gestacional"),
    NA("N/A");

    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    private TipoDiabetes(String nombre) {
        this.nombre = nombre;
    }

    public static TipoDiabetes get(int tipoDiabetes) {
        for (TipoDiabetes tipo : values()) {
            if (tipo.ordinal() == tipoDiabetes) {
                return tipo;
            }
        }

        return null;
    }

    @JsonValue
    public int getId() {
        return ordinal();
    }


}
