package com.glucome.enumerador;

import com.fasterxml.jackson.annotation.JsonValue;

public enum TipoEjercicio {

	CORRER("Correr"), CAMINAR("Caminar"), DEPORTE("Deporte"), OTRO("Otro");

	private String descripcion;

	private TipoEjercicio(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static TipoEjercicio get(int tipoDiabetes) {
		for (TipoEjercicio tipo : values()) {
			if (tipo.ordinal() == tipoDiabetes) {
				return tipo;
			}
		}
		return null;
	}

	@JsonValue
	public int getId() {
		return ordinal();
	}
}
