package com.glucome.enumerador;

import com.fasterxml.jackson.annotation.JsonValue;

public enum TipoInsulina {

    REGULAR_NORMAL("Regular / normal", AccionInsulina.RAPIDA), LYSPRO_ASPART_GLULISINA("Lyspro/ Aspart/ Glulisina", AccionInsulina.RAPIDA), NPH("NPH", AccionInsulina.INTERMEDIA), DETEMIR("Detemir", AccionInsulina.PROLONGADA), GLARGINA("Glargina", AccionInsulina.PROLONGADA), OTRO("Otro", null);

    private final String nombre;
    private final AccionInsulina accion;

    private TipoInsulina(String nombre, AccionInsulina accion) {
        this.nombre = nombre;
        this.accion = accion;
    }

    public static TipoInsulina getById(int id) {
        for (TipoInsulina e : values()) {
            if (e.ordinal() == id) {
                return e;
            }
        }
        return null;
    }

    public String getNombre() {
        return nombre;
    }

    public AccionInsulina getAccion() {
        return accion;
    }

    @JsonValue
    public int getId() {
        return ordinal();
    }

}
