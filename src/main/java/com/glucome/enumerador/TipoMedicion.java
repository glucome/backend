package com.glucome.enumerador;

import com.fasterxml.jackson.annotation.JsonValue;

public enum TipoMedicion {

    INVASIVA("Invasiva"),
    NO_INVASIVA("No Invasiva"),
    NA("No aplica");

    private final String nombre;

    private TipoMedicion(String nombre) {
        this.nombre = nombre;
    }

    public static TipoMedicion getById(int id) {
        for (TipoMedicion tipo : values()) {
            if (tipo.ordinal() == id) {
                return tipo;
            }
        }
        return TipoMedicion.NA;
    }

    public String getNombre() {
        return nombre;
    }


    @JsonValue
    public int getId() {
        return ordinal();
    }


}
