package com.glucome.enumerador;

import com.glucome.modelo.SolicitudVinculacion;
import com.glucome.modelo.SolicitudVinculacionAllegadoPaciente;
import com.glucome.modelo.SolicitudVinculacionMedicoPaciente;

public enum TipoSolicitud {

	MEDICO_PACIENTE(SolicitudVinculacionMedicoPaciente.class), ALLEGADO_PACIENTE(SolicitudVinculacionAllegadoPaciente.class);

	private Class<? extends SolicitudVinculacion> clase;

	private TipoSolicitud(Class<? extends SolicitudVinculacion> clase) {
		this.clase = clase;
	}

	public static TipoSolicitud get(int id) {
		for (TipoSolicitud tipo : values()) {
			if (tipo.ordinal() == id) {
				return tipo;
			}
		}

		return MEDICO_PACIENTE;
	}

	public Class<? extends SolicitudVinculacion> getClase() {
		return clase;
	}

	public void setClase(Class<? extends SolicitudVinculacion> clase) {
		this.clase = clase;
	}

}
