package com.glucome.enumerador;

public enum TipoUsuario {

	PACIENTE("Paciente"), MEDICO("Medico"), ALLEGADO("Allegado");

	private String descripcion;

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	private TipoUsuario(String descripcion) {
		this.descripcion = descripcion;
	}

}
