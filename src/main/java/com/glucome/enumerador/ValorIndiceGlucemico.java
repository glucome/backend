package com.glucome.enumerador;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ValorIndiceGlucemico {
	ALTO("Alto"), MEDIO("Medio"), BAJO("Bajo");

	private String descripcion;

	@JsonValue
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	private ValorIndiceGlucemico(String descripcion) {
		this.descripcion = descripcion;
	}

}
