package com.glucome.excepciones;

public class APIException extends Exception {

    private static final long serialVersionUID = 1L;

    public APIException(String mensaje) {
        super(mensaje);
    }

}
