package com.glucome.excepciones;

public class InsulinaException extends Exception {

    private static final long serialVersionUID = 1L;

    public InsulinaException(String mensaje) {
        super(mensaje);
    }

}
