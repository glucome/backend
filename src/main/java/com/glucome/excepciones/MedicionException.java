package com.glucome.excepciones;

public class MedicionException extends Exception {

    private static final long serialVersionUID = 1L;

    public MedicionException(String mensaje) {
        super(mensaje);
    }
}
