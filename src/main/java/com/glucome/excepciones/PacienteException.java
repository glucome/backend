package com.glucome.excepciones;

public class PacienteException extends Exception {

    private static final long serialVersionUID = 1L;

    public PacienteException(String mensaje) {
        super(mensaje);
    }


}