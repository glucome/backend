package com.glucome.excepciones;

import java.util.List;
import java.util.stream.Collectors;

public class ParametrosNoValidosException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public ParametrosNoValidosException(List<String> camposInvalidos) {
		super(getString(camposInvalidos));
	}

	private static String getString(List<String> camposInvalidos) {
		String mensajeError = "Los siguientes campos son inválidos: " + camposInvalidos.stream().map(s -> s.concat(", ")).collect(Collectors.joining());

		return mensajeError.substring(0, mensajeError.length() - 2) + ".";
	}

}
