package com.glucome.interfaces;

public interface RegistroUsuarioRequest {
    public String getPassword();

    public String getMail();

    public String getNombreUsuario();
}
