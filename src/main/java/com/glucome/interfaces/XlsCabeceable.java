package com.glucome.interfaces;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.Map;

public interface XlsCabeceable {

    public int llenarCabecera(Sheet hoja, Map<String, CellStyle> estilos);

}
