package com.glucome.interfaces;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;

import java.util.Map;

public interface XlsExportable {
    public void llenarRow(Row row, Map<String, CellStyle> estilos);
}
