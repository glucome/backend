package com.glucome.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.glucome.enumerador.MomentoAlimento;
import com.glucome.enumerador.ValorIndiceGlucemico;
import com.glucome.interfaces.Persistente;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "alimento")
@JsonIgnoreProperties(value = { "imagen" })
public class Alimento implements Persistente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(columnDefinition = "TEXT")
	private String nombre;

	private ValorIndiceGlucemico indiceGlucemico;

	@Column(columnDefinition = "TEXT")
	private String unidadReferencia;

	private double carbohidratosPorUnidad;

	@ElementCollection
	@CollectionTable(name = "alimento_momento")
	@Column(name = "momento")
	private List<MomentoAlimento> momentos;

	@Column(columnDefinition = "TEXT")
	private String descripcion;

	@Column(columnDefinition = "TEXT")
	private String imagen;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ValorIndiceGlucemico getIndiceGlucemico() {
		return indiceGlucemico;
	}

	public void setIndiceGlucemico(ValorIndiceGlucemico indiceGlucemico) {
		this.indiceGlucemico = indiceGlucemico;
	}

	public Alimento(long id, String nombre, ValorIndiceGlucemico indiceGlucemico) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.indiceGlucemico = indiceGlucemico;
	}

	public Alimento() {
		super();
	}

	public List<MomentoAlimento> getMomento() {
		return momentos;
	}

	public void setMomento(List<MomentoAlimento> momentos) {
		this.momentos = momentos;
	}

	public String getImagen() {
		return this.imagen;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getUnidadReferencia() {
		return unidadReferencia;
	}

	public void setUnidadReferencia(String unidadReferencia) {
		this.unidadReferencia = unidadReferencia;
	}

	public List<MomentoAlimento> getMomentos() {
		return momentos;
	}

	public void setMomentos(List<MomentoAlimento> momentos) {
		this.momentos = momentos;
	}

	public double getCarbohidratosPorUnidad() {
		return carbohidratosPorUnidad;
	}

	public void setCarbohidratosPorUnidad(double carbohidratosPorUnidad) {
		this.carbohidratosPorUnidad = carbohidratosPorUnidad;
	}

}
