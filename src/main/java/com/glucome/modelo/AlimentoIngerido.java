package com.glucome.modelo;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.glucome.api.request.AlimentoIngeridoRequest;
import com.glucome.enumerador.MomentoAlimento;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.interfaces.Persistente;
import com.glucome.utils.UtilesFecha;

@Entity
@Table(name = "alimento_ingerido")
@JsonIgnoreProperties(value = { "paciente" })
public class AlimentoIngerido implements Persistente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	private Paciente paciente;

	@Column(nullable = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = UtilesFecha.DD_MM_YYYY_HH_MM)
	private LocalDateTime fechaHora;

	private double porciones;

	@ManyToOne
	private Alimento alimento;

	private MomentoAlimento momento;

	private String observacion;

	private String imagen;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public LocalDateTime getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(LocalDateTime fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Alimento getAlimento() {
		return alimento;
	}

	public void setAlimento(Alimento alimento) {
		this.alimento = alimento;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public AlimentoIngerido(long id, Paciente paciente, LocalDateTime fechaHora, double porciones, Alimento alimento, String observacion, MomentoAlimento momento) {
		super();
		this.id = id;
		this.paciente = paciente;
		this.fechaHora = fechaHora;
		this.porciones = porciones;
		this.alimento = alimento;
		this.observacion = observacion;
		this.momento = momento;
	}

	public AlimentoIngerido() {
		super();
	}

	public AlimentoIngerido(AlimentoIngeridoRequest data, Paciente paciente, Alimento alimento, String imagen) throws ParametrosNoValidosException {
		List<String> camposInvalidos = new ArrayList<>();

		try {
			this.fechaHora = UtilesFecha.getLocalDateTimeFromString(data.getFechaHora());
			if (fechaHora == null) {
				camposInvalidos.add("Fecha y hora");
			}
		} catch (DateTimeParseException e) {
			camposInvalidos.add("Fecha y hora");
		}

		this.porciones = data.getPorciones();

		this.paciente = paciente;

		this.imagen = imagen;

		this.alimento = alimento;

		if (this.alimento == null) {
			camposInvalidos.add("Alimento");
		}
		this.observacion = data.getObservacion();

		if (paciente == null) {
			camposInvalidos.add("Paciente");
		}

		this.momento = MomentoAlimento.get(data.getMomento());

		if (momento == null) {
			camposInvalidos.add("Momento");
		}

		if (!camposInvalidos.isEmpty()) {
			throw new ParametrosNoValidosException(camposInvalidos);
		}
	}

	public MomentoAlimento getMomento() {
		return momento;
	}

	public void setMomento(MomentoAlimento momento) {
		this.momento = momento;
	}

	public double getPorciones() {
		return porciones;
	}

	public void setPorciones(double porciones) {
		this.porciones = porciones;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

}
