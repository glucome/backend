package com.glucome.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.glucome.api.request.RegistroAllegadoRequest;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.interfaces.Logueable;
import com.glucome.interfaces.Persistente;
import com.glucome.utils.UtilesString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "allegado")
@JsonIgnoreProperties(value = {"usuario"})
public class Allegado implements Persistente, Logueable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @OneToOne
    private Usuario usuario;

    public long getId() {
        return id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Allegado(long id, String nombre) {
        super();
        this.id = id;
        this.nombre = nombre;
    }

    public Allegado() {
    }
    
    public Allegado(RegistroAllegadoRequest data, Usuario usuario) throws ParametrosNoValidosException {
    	List<String> camposInvalidos = new ArrayList<>();
    	
    	this.usuario = usuario;
    	
    	this.id = data.getId();
    	
    	this.nombre = data.getNombre();
        if (UtilesString.isNullOrEmpty(nombre)) {
            camposInvalidos.add("nombre");
        }
        
        if (!camposInvalidos.isEmpty()) {
            throw new ParametrosNoValidosException(camposInvalidos);
        }
    }

    @Override
    public long getIdUsuario() {
        return this.usuario != null ? usuario.getId() : 0L;
    }
}
