package com.glucome.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.glucome.interfaces.Persistente;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "chat")
@JsonIgnoreProperties(value = {"usuarios", "mensajes"})
public class Chat implements Persistente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Usuario> usuarios = new ArrayList<>();

    @OneToMany(mappedBy = "chat")
    private List<Mensaje> mensajes = new ArrayList<>();

    public Chat() {
    }

    public Chat(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
}
