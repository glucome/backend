package com.glucome.modelo;

import com.glucome.api.request.ContactoRequest;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.interfaces.Persistente;
import com.glucome.utils.UtilesString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "contacto")
public class Contacto implements Persistente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String nombre;

	private String telefono;

	@ManyToOne
	private Paciente paciente;

	public Contacto(ContactoRequest data, Paciente paciente) throws ParametrosNoValidosException {
		List<String> camposInvalidos = new ArrayList<>();

		this.nombre = data.getNombre();

		if (UtilesString.isNullOrEmpty(nombre)) {
			camposInvalidos.add("Nombre");
		}

		this.telefono = data.getTelefono();

		if (UtilesString.isNullOrEmpty(telefono)) {
			camposInvalidos.add("Teléfono");
		}

		this.paciente = paciente;

		if (paciente == null) {
			camposInvalidos.add("Paciente");
		}

		if (!camposInvalidos.isEmpty()) {
			throw new ParametrosNoValidosException(camposInvalidos);
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Contacto() {
		super();
	}

}
