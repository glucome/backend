package com.glucome.modelo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.glucome.api.request.EjercicioRequest;
import com.glucome.enumerador.TipoEjercicio;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.interfaces.Persistente;
import com.glucome.utils.UtilesFecha;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ejercicio")
@JsonIgnoreProperties(value = { "paciente" })
public class Ejercicio implements Persistente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	private Paciente paciente;

	@Column(nullable = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = UtilesFecha.DD_MM_YYYY_HH_MM)
	private LocalDateTime fechaHora;

	private double minutos;

	private TipoEjercicio tipo;

	private String observacion;

	@Override
	public long getId() {
		return id;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public LocalDateTime getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(LocalDateTime fechaHora) {
		this.fechaHora = fechaHora;
	}

	public double getMinutos() {
		return minutos;
	}

	public void setMinutos(double minutos) {
		this.minutos = minutos;
	}

	public TipoEjercicio getTipo() {
		return tipo;
	}

	public void setTipo(TipoEjercicio tipo) {
		this.tipo = tipo;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Ejercicio(EjercicioRequest data, Paciente paciente) throws ParametrosNoValidosException {
		List<String> camposInvalidos = new ArrayList<>();

		try {
			this.fechaHora = UtilesFecha.getLocalDateTimeFromString(data.getFechaHora());
			if (fechaHora == null) {
				camposInvalidos.add("Fecha y hora");
			}
		} catch (DateTimeParseException e) {
			camposInvalidos.add("Fecha y hora");
		}

		this.minutos = data.getMinutos();

		this.paciente = paciente;

		this.tipo = TipoEjercicio.get(data.getTipo());

		this.observacion = data.getObservacion();

		if (paciente == null) {
			camposInvalidos.add("Paciente");
		}

		if (!camposInvalidos.isEmpty()) {
			throw new ParametrosNoValidosException(camposInvalidos);
		}
	}

	public Ejercicio() {
		super();
	}

}
