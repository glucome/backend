package com.glucome.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.glucome.api.request.FactorCorreccionInsulinaRequest;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.interfaces.Persistente;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "factor_correccion_insulina")
@JsonIgnoreProperties(value = {"paciente"})
public class FactorCorreccionInsulina implements Persistente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
	@OneToOne
	private Paciente paciente;
	
	private double factorCorreccionAzucar;
	
	private double factorCorreccionCHO;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public double getFactorCorreccionAzucar() {
		return factorCorreccionAzucar;
	}

	public void setFactorCorreccionAzucar(double factorCorreccionAzucar) {
		this.factorCorreccionAzucar = factorCorreccionAzucar;
	}

	public double getFactorCorreccionCHO() {
		return factorCorreccionCHO;
	}

	public void setFactorCorreccionCHO(double factorCorreccionCHO) {
		this.factorCorreccionCHO = factorCorreccionCHO;
	}
	
	public FactorCorreccionInsulina(FactorCorreccionInsulinaRequest data, Paciente paciente) throws ParametrosNoValidosException {
		List<String> camposInvalidos = new ArrayList<>();

		this.paciente = paciente;

		this.factorCorreccionAzucar = data.getFactorCorreccionAzucar();
		
		this.factorCorreccionCHO = data.getFactorCorreccionCHO();

		if (paciente == null) {
			camposInvalidos.add("Paciente");
		}

		if (!camposInvalidos.isEmpty()) {
			throw new ParametrosNoValidosException(camposInvalidos);
		}
	}
	
	public FactorCorreccionInsulina() {
		super();
	}
}
