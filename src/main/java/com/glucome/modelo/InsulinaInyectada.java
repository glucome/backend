package com.glucome.modelo;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.glucome.api.request.InsulinaInyectadaRequest;
import com.glucome.enumerador.TipoInsulina;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.interfaces.Persistente;
import com.glucome.utils.UtilesFecha;

//Inselina Inyuctada grrr
@Entity
@Table(name = "insulina_inyectada")
@JsonIgnoreProperties(value = { "paciente" })
public class InsulinaInyectada implements Persistente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	private Paciente paciente;

	@Column(nullable = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = UtilesFecha.DD_MM_YYYY_HH_MM)
	private LocalDateTime fechaHora;

	private double unidades;

	private TipoInsulina tipo;

	public InsulinaInyectada(InsulinaInyectadaRequest data, Paciente paciente) throws ParametrosNoValidosException {
		List<String> camposInvalidos = new ArrayList<>();

		try {
			this.fechaHora = UtilesFecha.getLocalDateTimeFromString(data.getFechaHora());
			if (fechaHora == null) {
				camposInvalidos.add("Fecha y hora");
			}
		} catch (DateTimeParseException e) {
			camposInvalidos.add("Fecha y hora");
		}

		this.unidades = data.getUnidades();

		this.paciente = paciente;

		this.tipo = TipoInsulina.getById(data.getTipo());

		if (paciente == null) {
			camposInvalidos.add("Paciente");
		}

		if (!camposInvalidos.isEmpty()) {
			throw new ParametrosNoValidosException(camposInvalidos);
		}
	}

	public InsulinaInyectada() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public LocalDateTime getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(LocalDateTime fechaHora) {
		this.fechaHora = fechaHora;
	}

	public double getUnidades() {
		return unidades;
	}

	public void setUnidades(double unidades) {
		this.unidades = unidades;
	}

	public TipoInsulina getTipo() {
		return tipo;
	}

	public void setTipo(TipoInsulina tipo) {
		this.tipo = tipo;
	}

}
