package com.glucome.modelo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.glucome.api.request.ItemHistoriaRequest;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.interfaces.Persistente;
import com.glucome.utils.UtilesFecha;
import com.glucome.utils.UtilesString;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "item_historia_clinica")
// @JsonIgnoreProperties(value = { "paciente" })
public class ItemHistoriaClinica implements Persistente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	private Paciente paciente;

	@ManyToOne
	private Medico medico;

	@Column(nullable = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = UtilesFecha.DD_MM_YYYY)
	private LocalDate fecha;

	@Column(columnDefinition = "TEXT")
	private String descripcion;

	public ItemHistoriaClinica(ItemHistoriaRequest data, Paciente paciente, Medico medico) throws ParametrosNoValidosException {
		List<String> camposInvalidos = new ArrayList<>();

		this.id = data.getId();

		this.paciente = paciente;
		if (paciente == null) {
			camposInvalidos.add("Paciente");
		}
		this.medico = medico;
		if (medico == null) {
			camposInvalidos.add("Médico");
		}

		try {
			this.fecha = UtilesFecha.getLocalDateFromString(data.getFecha());
			if (fecha == null) {
				camposInvalidos.add("Fecha");
			}
		} catch (DateTimeParseException e) {
			camposInvalidos.add("Fecha");
		}

		this.descripcion = data.getDescripcion();

		if (UtilesString.isNullOrEmpty(descripcion)) {
			camposInvalidos.add("Descripción");
		}

		if (!camposInvalidos.isEmpty()) {
			throw new ParametrosNoValidosException(camposInvalidos);
		}
	}

	@Override
	public long getId() {
		return id;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ItemHistoriaClinica() {
	}

	public String getFechaFormateada() {
		return UtilesFecha.getStringFromLocalDate(fecha);
	}

}
