package com.glucome.modelo;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "medicamento")
public class Medicamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String droga;

    private String marca;

    private String presentacion;

    private String laboratorio;

    @ManyToMany(mappedBy = "medicamentos")
    private List<Paciente> pacientes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDroga() {
        return droga;
    }

    public void setDroga(String droga) {
        this.droga = droga;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public String getLaboratorio() {
        return laboratorio;
    }

    public void setLaboratorio(String laboratorio) {
        this.laboratorio = laboratorio;
    }

    public Medicamento(long id, String droga, String marca, String presentacion, String laboratorio) {
        super();
        this.id = id;
        this.droga = droga;
        this.marca = marca;
        this.presentacion = presentacion;
        this.laboratorio = laboratorio;
    }

    public Medicamento() {
    }

}
