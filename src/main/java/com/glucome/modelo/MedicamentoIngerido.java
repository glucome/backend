package com.glucome.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.glucome.api.request.MedicamentoTomadoPacienteRequest;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.interfaces.Persistente;
import com.glucome.utils.UtilesFecha;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "medicamento_ingerido")
@JsonIgnoreProperties(value = { "paciente" })
public class MedicamentoIngerido implements Persistente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	private Medicamento medicamento;

	@ManyToOne
	private Paciente paciente;

	private LocalDateTime fechaHora;

	private double cantidadIngerida;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Medicamento getMedicamento() {
		return medicamento;
	}

	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public LocalDateTime getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(LocalDateTime fechaHora) {
		this.fechaHora = fechaHora;
	}

	public double getCantidadIngerida() {
		return cantidadIngerida;
	}

	public void setCantidadIngerida(double cantidadIngerida) {
		this.cantidadIngerida = cantidadIngerida;
	}

	public MedicamentoIngerido(long id, Medicamento medicamento, Paciente paciente, LocalDateTime fechaHora, double cantidadIngerida) {
		super();
		this.id = id;
		this.medicamento = medicamento;
		this.paciente = paciente;
		this.fechaHora = fechaHora;
		this.cantidadIngerida = cantidadIngerida;
	}

	public MedicamentoIngerido() {
	}

	public MedicamentoIngerido(MedicamentoTomadoPacienteRequest data, Medicamento medicamento, Paciente paciente) throws ParametrosNoValidosException {
		List<String> camposInvalidos = new ArrayList<>();

		try {
			this.fechaHora = UtilesFecha.getLocalDateTimeFromString(data.getFechaHora());
			if (fechaHora == null) {
				camposInvalidos.add("Fecha y hora");
			}
		} catch (DateTimeParseException e) {
			camposInvalidos.add("Fecha y hora");
		}

		this.paciente = paciente;

		if (paciente == null) {
			camposInvalidos.add("Paciente");
		}

		this.medicamento = medicamento;

		if (medicamento == null) {
			camposInvalidos.add("Medicamento");
		}

		this.cantidadIngerida = data.getCantidad();

		if (!camposInvalidos.isEmpty()) {
			throw new ParametrosNoValidosException(camposInvalidos);
		}
	}

}
