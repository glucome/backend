package com.glucome.modelo;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.glucome.api.request.MedicionRequest;
import com.glucome.enumerador.MomentoMedicion;
import com.glucome.enumerador.ResultadoMedicion;
import com.glucome.enumerador.TipoMedicion;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.interfaces.Persistente;
import com.glucome.interfaces.XlsExportable;
import com.glucome.utils.UtilesFecha;
import com.glucome.utils.XlsUtils;

@Entity
@Table(name = "medicion")
@JsonIgnoreProperties(value = { "paciente" })
public class Medicion implements Persistente, XlsExportable {

	private static final int VALOR_BAJO_ANTES_DE_COMER = 70;

	private static final int VALOR_MAXIMO_NORMAL_DESPUES_DE_COMER = 140;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = UtilesFecha.DD_MM_YYYY_HH_MM)
	private LocalDateTime fechaHora;

	private int valor;

	private TipoMedicion tipo;

	private MomentoMedicion momento;

	@ManyToOne
	private Paciente paciente;

	public Medicion(MedicionRequest data, Paciente paciente) throws ParametrosNoValidosException {
		List<String> camposInvalidos = new ArrayList<>();

		try {
			this.fechaHora = UtilesFecha.getLocalDateTimeFromString(data.getFechaHora());
			if (fechaHora == null) {
				camposInvalidos.add("Fecha y hora");
			}
		} catch (DateTimeParseException e) {
			camposInvalidos.add("Fecha y hora");
		}

		this.valor = data.getValor();
		this.paciente = paciente;

		this.momento = MomentoMedicion.getById(data.getMomento());

		this.tipo = TipoMedicion.getById(data.getTipo());

		if (paciente == null) {
			camposInvalidos.add("Paciente");
		}

		if (!camposInvalidos.isEmpty()) {
			throw new ParametrosNoValidosException(camposInvalidos);
		}
	}

	@Override
	public long getId() {
		return id;
	}

	public LocalDateTime getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(LocalDateTime fechaHora) {
		this.fechaHora = fechaHora;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Medicion() {
		super();
	}

	public TipoMedicion getTipo() {
		return tipo;
	}

	public void setTipo(TipoMedicion tipo) {
		this.tipo = tipo;
	}

	public MomentoMedicion getMomento() {
		return momento;
	}

	public void setMomento(MomentoMedicion momento) {
		this.momento = momento;
	}

	public ResultadoMedicion getResultadoMedicion() {

		if (valor < VALOR_BAJO_ANTES_DE_COMER) {
			return ResultadoMedicion.BAJA;
		}

		if (valor >= VALOR_BAJO_ANTES_DE_COMER && valor <= VALOR_MAXIMO_NORMAL_DESPUES_DE_COMER) {
			return ResultadoMedicion.MEDIA;
		}

		if (valor >= VALOR_MAXIMO_NORMAL_DESPUES_DE_COMER) {
			return ResultadoMedicion.ALTA;
		}

		return ResultadoMedicion.ALTA;

	}

	public boolean esAntesDeComer() {
		return this.momento.esAntesDeComer();
	}

	public static String[] getCabeceraXls() {
		return new String[] { "Valor", "Momento", "Fecha y Hora" };

	}

	@Override
	public void llenarRow(Row row, Map<String, CellStyle> estilos) {
		int columna = 0;

		Cell valor = row.createCell(columna++);
		valor.setCellValue(this.valor);
		valor.setCellType(CellType.NUMERIC);
		valor.setCellStyle(estilos.get(XlsUtils.NORMAL));

		Cell momento = row.createCell(columna++);
		momento.setCellValue(this.momento != null ? this.momento.getNombre() : "");
		momento.setCellStyle(estilos.get(XlsUtils.NORMAL));

		Cell fechaHora = row.createCell(columna++);
		fechaHora.setCellValue(this.fechaHora != null ? UtilesFecha.getString(this.fechaHora) : "");
		fechaHora.setCellStyle(estilos.get(XlsUtils.NORMAL));

	}

}
