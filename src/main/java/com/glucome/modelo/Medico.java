package com.glucome.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.glucome.api.request.RegistroMedicoRequest;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.interfaces.Logueable;
import com.glucome.interfaces.Persistente;
import com.glucome.utils.UtilesString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "medico")
@JsonIgnoreProperties(value = { "usuario" })
public class Medico implements Persistente, Logueable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String nombre;

	private String matricula;

	@Column(columnDefinition = "TEXT")
	private String descripcion;

	@OneToOne
	private Usuario usuario;

	private String ubicacion;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Medico(long id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public Medico() {
		super();
	}

	public Medico(RegistroMedicoRequest data, Usuario usuario) throws ParametrosNoValidosException {
		List<String> camposInvalidos = new ArrayList<>();
		this.usuario = usuario;

		this.id = data.getId();

		this.nombre = data.getNombre();
		if (UtilesString.isNullOrEmpty(nombre)) {
			camposInvalidos.add("nombre");
		}

		this.matricula = data.getMatricula();
		if (UtilesString.isNullOrEmpty(matricula)) {
			camposInvalidos.add("matrícula");
		}

		this.descripcion = data.getDescripcion();

		if (!camposInvalidos.isEmpty()) {
			throw new ParametrosNoValidosException(camposInvalidos);
		}
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public long getIdUsuario() {
		return this.usuario != null ? usuario.getId() : 0L;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUbicacion() { return ubicacion; }

	public void setUbicacion(String ubicacion) { this.ubicacion = ubicacion; }
}
