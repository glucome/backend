package com.glucome.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.glucome.interfaces.Persistente;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "mensaje")
@JsonIgnoreProperties(value = {"chat", "autor", "destinatario"})
public class Mensaje implements Persistente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne(fetch = FetchType.LAZY)
    private Usuario autor;

    @OneToOne(fetch = FetchType.LAZY)
    private Usuario destinatario;

    @ManyToOne(fetch = FetchType.LAZY)
    private Chat chat;

    @Lob
    private String cuerpo;

    private LocalDateTime fechaHora;

    private LocalDateTime fechaHoraVisto;

    public Mensaje() {
    }

    public Mensaje(Chat chat, String cuerpo, LocalDateTime fechaHora) {
        this.chat = chat;
        this.cuerpo = cuerpo;
        this.fechaHora = fechaHora;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Usuario getAutor() {
        return autor;
    }

    public void setAutor(Usuario autor) {
        this.autor = autor;
    }

    public Usuario getDestinatario() { return destinatario; }

    public void setDestinatario(Usuario destinatario) { this.destinatario = destinatario; }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public LocalDateTime getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(LocalDateTime fechaHora) {
        this.fechaHora = fechaHora;
    }

    public LocalDateTime getFechaHoraVisto() { return fechaHoraVisto; }

    public void setFechaHoraVisto(LocalDateTime fechaHoraVisto) { this.fechaHoraVisto = fechaHoraVisto; }
}
