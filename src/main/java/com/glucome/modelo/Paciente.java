package com.glucome.modelo;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.glucome.api.request.RegistroPacienteExternoRequest;
import com.glucome.api.request.RegistroPacienteRequest;
import com.glucome.enumerador.Sexo;
import com.glucome.enumerador.TipoDiabetes;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.interfaces.Logueable;
import com.glucome.interfaces.Persistente;
import com.glucome.interfaces.XlsCabeceable;
import com.glucome.utils.UtilesFecha;
import com.glucome.utils.UtilesString;
import com.glucome.utils.XlsUtils;

@Entity
@Table(name = "paciente")
@JsonIgnoreProperties(value = { "medicos", "allegados", "usuario", "medicamentos", "contactos" })
public class Paciente implements Persistente, Logueable, XlsCabeceable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "nombre", nullable = false)
	private String nombre;

	private double altura;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = UtilesFecha.DD_MM_YYYY)
	private LocalDate fechaNacimiento;

	private double peso;

	private Sexo sexo;

	private TipoDiabetes tipoDiabetes;

	@ManyToMany
	private List<Medico> medicos;

	@ManyToMany
	private List<Allegado> allegados;

	@ManyToMany
	private List<Medicamento> medicamentos;

	@OneToMany(mappedBy = "paciente")
	private List<Contacto> contactos;

	@OneToOne
	private Usuario usuario;

	@OneToOne(mappedBy = "paciente", orphanRemoval = true, cascade = CascadeType.ALL)
	private FactorCorreccionInsulina factorCorreccionInsulina;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Paciente(long id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public Paciente() {
		super();
	}

	public Paciente(RegistroPacienteRequest data, Usuario usuario) throws ParametrosNoValidosException {
		List<String> camposInvalidos = new ArrayList<>();
		this.usuario = usuario;

		this.id = data.getId();

		this.altura = data.getAltura();

		try {
			this.fechaNacimiento = UtilesFecha.getLocalDateFromString(data.getFechaNacimiento());
		} catch (DateTimeParseException e) {
			camposInvalidos.add("fecha de nacimiento");
		}
		this.nombre = data.getNombre();
		if (UtilesString.isNullOrEmpty(nombre)) {
			camposInvalidos.add("nombre");
		}

		this.peso = data.getPeso();

		this.sexo = Sexo.get(data.getSexo());

		if (sexo == null) {
			camposInvalidos.add("sexo");
		}

		this.tipoDiabetes = TipoDiabetes.get(data.getTipoDiabetes());

		if (tipoDiabetes == null) {
			camposInvalidos.add("tipo de diabetes");
		}

		if (!camposInvalidos.isEmpty()) {
			throw new ParametrosNoValidosException(camposInvalidos);
		}

	}

	public Paciente(RegistroPacienteExternoRequest data) throws ParametrosNoValidosException {
		List<String> camposInvalidos = new ArrayList<>();

		this.id = data.getId();

		this.altura = data.getAltura();

		try {
			this.fechaNacimiento = UtilesFecha.getLocalDateFromString(data.getFechaNacimiento());
		} catch (DateTimeParseException e) {
			camposInvalidos.add("fecha de nacimiento");
		}
		this.nombre = data.getNombre();
		if (UtilesString.isNullOrEmpty(nombre)) {
			camposInvalidos.add("nombre");
		}

		this.peso = data.getPeso();

		this.sexo = Sexo.get(data.getSexo());

		if (sexo == null) {
			camposInvalidos.add("sexo");
		}

		this.tipoDiabetes = TipoDiabetes.get(data.getTipoDiabetes());

		if (tipoDiabetes == null) {
			camposInvalidos.add("tipo de diabetes");
		}

		if (!camposInvalidos.isEmpty()) {
			throw new ParametrosNoValidosException(camposInvalidos);
		}
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public String getFechaNacimientoFormateada() {
		return UtilesFecha.getStringFromLocalDate(fechaNacimiento);
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public TipoDiabetes getTipoDiabetes() {
		return tipoDiabetes;
	}

	public void setTipoDiabetes(TipoDiabetes tipoDiabetes) {
		this.tipoDiabetes = tipoDiabetes;
	}

	public List<Medico> getMedicos() {
		return medicos;
	}

	public void setMedicos(List<Medico> medicos) {
		this.medicos = medicos;
	}

	public List<Allegado> getAllegados() {
		return allegados;
	}

	public void setAllegados(List<Allegado> allegados) {
		this.allegados = allegados;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public FactorCorreccionInsulina getFactorCorreccionInsulina() {
		return factorCorreccionInsulina;
	}

	public void setFactorCorreccionInsulina(FactorCorreccionInsulina factorCorreccionInsulina) {
		this.factorCorreccionInsulina = factorCorreccionInsulina;
	}

	public void agregarMedico(Medico medico) {
		if (medicos == null) {
			medicos = new ArrayList<>();
		}
		medicos.add(medico);
	}

	public void agregarMedicamento(Medicamento medicamento) {
		if (medicamentos == null) {
			medicamentos = new ArrayList<>();
		}
		medicamentos.add(medicamento);

	}

	@Override
	public long getIdUsuario() {
		return this.usuario != null ? usuario.getId() : 0L;
	}

	@Override
	public int llenarCabecera(Sheet hoja, Map<String, CellStyle> estilos) {
		int fila = 0;
		int columna = 0;
		Row row = hoja.createRow(fila++);

		Cell pacienteHeader = row.createCell(columna++);
		pacienteHeader.setCellValue("Paciente");
		pacienteHeader.setCellStyle(estilos.get(XlsUtils.CABECERA));

		Cell tipoDiabetesHeader = row.createCell(columna++);
		tipoDiabetesHeader.setCellValue("Tipo de diabetes");
		tipoDiabetesHeader.setCellStyle(estilos.get(XlsUtils.CABECERA));

		Cell fechaNacimientoHeader = row.createCell(columna++);
		fechaNacimientoHeader.setCellValue("Fecha de Nacimiento");
		fechaNacimientoHeader.setCellStyle(estilos.get(XlsUtils.CABECERA));

		Cell alturaHeader = row.createCell(columna++);
		alturaHeader.setCellValue("Altura");
		alturaHeader.setCellStyle(estilos.get(XlsUtils.CABECERA));

		Cell pesoHeader = row.createCell(columna++);
		pesoHeader.setCellValue("Peso");
		pesoHeader.setCellStyle(estilos.get(XlsUtils.CABECERA));

		Cell sexoHeader = row.createCell(columna++);
		sexoHeader.setCellValue("Sexo");
		sexoHeader.setCellStyle(estilos.get(XlsUtils.CABECERA));

		row = hoja.createRow(fila++);
		columna = 0;

		Cell nombre = row.createCell(columna++);
		nombre.setCellValue(this.getNombre());
		nombre.setCellStyle(estilos.get(XlsUtils.FONDO_GRIS));

		Cell tipoDiabetes = row.createCell(columna++);
		tipoDiabetes.setCellValue(this.tipoDiabetes != null ? this.tipoDiabetes.getNombre() : "");
		tipoDiabetes.setCellStyle(estilos.get(XlsUtils.FONDO_GRIS));

		Cell fechaNacimiento = row.createCell(columna++);
		fechaNacimiento.setCellValue(this.fechaNacimiento != null ? UtilesFecha.getStringFromLocalDate(this.fechaNacimiento) : "");
		fechaNacimiento.setCellType(CellType.STRING);
		fechaNacimiento.setCellStyle(estilos.get(XlsUtils.FONDO_GRIS));

		Cell altura = row.createCell(columna++);
		altura.setCellValue(this.getAltura());
		altura.setCellType(CellType.STRING);
		altura.setCellStyle(estilos.get(XlsUtils.FONDO_GRIS));

		Cell peso = row.createCell(columna++);
		peso.setCellValue(this.getPeso());
		peso.setCellType(CellType.STRING);
		peso.setCellStyle(estilos.get(XlsUtils.FONDO_GRIS));

		Cell sexo = row.createCell(columna++);
		sexo.setCellValue(this.sexo != null ? this.sexo.getNombre() : "");
		sexo.setCellStyle(estilos.get(XlsUtils.FONDO_GRIS));

		row = hoja.createRow(fila++);

		return fila;
	}

	public void agregarAllegado(Allegado allegado) {
		if (allegados == null) {
			allegados = new ArrayList<>();
		}
		allegados.add(allegado);
	}

	public boolean getExterno() {
		return this.usuario == null;
	}

	public List<Medicamento> getMedicamentos() {
		return medicamentos;
	}

	public void setMedicamentos(List<Medicamento> medicamentos) {
		this.medicamentos = medicamentos;
	}

	public List<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos(List<Contacto> contactos) {
		this.contactos = contactos;
	}

}
