package com.glucome.modelo;

import com.glucome.enumerador.EstadoPago;
import com.glucome.interfaces.Persistente;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "pago")
public class Pago implements Persistente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@OneToOne
	private Usuario usuario;

	private LocalDateTime fechaHora;

	private EstadoPago estado;

	private boolean notificado;

	public Pago() {
	}

	public Pago(Usuario usuario, LocalDateTime fechaHora, EstadoPago estado, boolean notificado) {
		this.usuario = usuario;
		this.fechaHora = fechaHora;
		this.estado = estado;
		this.notificado = notificado;
	}

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public LocalDateTime getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(LocalDateTime fechaHora) {
		this.fechaHora = fechaHora;
	}

	public EstadoPago getEstado() {
		return estado;
	}

	public void setEstado(EstadoPago estado) {
		this.estado = estado;
	}

	public boolean isNotificado() { return notificado; }

	public void setNotificado(boolean notificado) { this.notificado = notificado; }
}
