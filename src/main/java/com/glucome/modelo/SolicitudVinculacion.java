package com.glucome.modelo;

import com.glucome.enumerador.EstadoSolicitud;
import com.glucome.interfaces.Logueable;
import com.glucome.interfaces.Persistente;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class SolicitudVinculacion implements Persistente {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private EstadoSolicitud estado;

    @Override
    public long getId() {
        return this.id;
    }

    public EstadoSolicitud getEstado() {
        return estado;
    }

    public void setEstado(EstadoSolicitud estado) {
        this.estado = estado;
    }

    public void setId(long id) {
        this.id = id;
    }

    public abstract Logueable aceptarSolicitud();

}
