package com.glucome.modelo;

import com.glucome.enumerador.EstadoSolicitud;
import com.glucome.interfaces.Logueable;

import javax.persistence.*;

@Entity
@Table(name = "solicitud_vinculacion_allegado_paciente", uniqueConstraints = { @UniqueConstraint(columnNames = { "paciente_id", "allegado_id" }) })
public class SolicitudVinculacionAllegadoPaciente extends SolicitudVinculacion {

	@ManyToOne
	@JoinColumn(name = "paciente_id")
	private Paciente paciente;

	@ManyToOne
	@JoinColumn(name = "allegado_id")
	private Allegado allegado;

	public SolicitudVinculacionAllegadoPaciente(Paciente paciente, Allegado allegado) {
		this.paciente = paciente;
		this.allegado = allegado;
		this.setEstado(EstadoSolicitud.PENDIENTE);
	}

	public SolicitudVinculacionAllegadoPaciente() {
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	@Override
	public Logueable aceptarSolicitud() {
		this.setEstado(EstadoSolicitud.ACEPTADA);
		this.paciente.agregarAllegado(allegado);

		return paciente;
	}

	public long getIdUsuarioPaciente() {
		return this.paciente != null ? paciente.getUsuario().getId() : 0L;
	}

	public Allegado getAllegado() {
		return allegado;
	}

	public void setAllegado(Allegado allegado) {
		this.allegado = allegado;
	}

}
