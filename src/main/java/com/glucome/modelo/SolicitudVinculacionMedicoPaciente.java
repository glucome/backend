package com.glucome.modelo;

import com.glucome.enumerador.EstadoSolicitud;
import com.glucome.interfaces.Logueable;

import javax.persistence.*;

@Entity
@Table(name = "solicitud_vinculacion_medico_paciente", uniqueConstraints = {@UniqueConstraint(columnNames = {"paciente_id", "medico_id"})})
public class SolicitudVinculacionMedicoPaciente extends SolicitudVinculacion {

    @ManyToOne
    @JoinColumn(name = "paciente_id")
    private Paciente paciente;

    @ManyToOne
    @JoinColumn(name = "medico_id")
    private Medico medico;

    public SolicitudVinculacionMedicoPaciente(Paciente paciente, Medico medico) {
        this.paciente = paciente;
        this.medico = medico;
        this.setEstado(EstadoSolicitud.PENDIENTE);
    }

    public SolicitudVinculacionMedicoPaciente() {
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Logueable aceptarSolicitud() {
        this.setEstado(EstadoSolicitud.ACEPTADA);
        this.paciente.agregarMedico(medico);

        return paciente;
    }

    public long getIdUsuarioPaciente() {
        return this.paciente != null ? paciente.getUsuario().getId() : 0L;
    }

}
