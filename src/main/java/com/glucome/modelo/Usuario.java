package com.glucome.modelo;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.glucome.enumerador.EstadoUsuario;
import com.glucome.enumerador.Plan;
import com.glucome.enumerador.Rol;
import com.glucome.enumerador.TipoUsuario;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.interfaces.Logueable;
import com.glucome.interfaces.Persistente;
import com.glucome.interfaces.RegistroUsuarioRequest;
import com.glucome.utils.UtilesString;

@Entity
@Table(name = "usuario")
public class Usuario implements UserDetails, Persistente {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;

	@NotEmpty
	@Column(unique = true)
	private String nombreUsuario;

	private String tokenFirebase;

	@NotEmpty
	private String clave;

	@NotEmpty
	@Column(unique = true)
	private String mail;

	@OneToOne(mappedBy = "usuario")
	private Paciente paciente;

	@OneToOne(mappedBy = "usuario")
	private Medico medico;

	@OneToOne(mappedBy = "usuario")
	private Allegado allegado;

	@ElementCollection(fetch = FetchType.EAGER)
	private List<Rol> roles = new ArrayList<>();

	private String imagen;

	@ManyToMany(mappedBy = "usuarios")
	private List<Chat> chats;

	private EstadoUsuario estado;

	private Plan plan;

	public Usuario(RegistroUsuarioRequest data, PasswordEncoder passwordEncoder, Plan plan) throws ParametrosNoValidosException {

		List<String> camposInvalidos = new ArrayList<>();

		this.clave = passwordEncoder.encode(data.getPassword());

		if (UtilesString.isNullOrEmpty(clave)) {
			camposInvalidos.add("clave");
		}

		this.mail = data.getMail();

		if (!UtilesString.esMailValido(mail)) {
			camposInvalidos.add("mail");
		}

		this.nombreUsuario = data.getNombreUsuario();

		if (UtilesString.isNullOrEmpty(nombreUsuario)) {
			camposInvalidos.add("nombre de usuario");
		}

		this.plan = plan;

		this.roles = new ArrayList<>();
		roles.add(plan.getRol());

		if (!camposInvalidos.isEmpty()) {
			throw new ParametrosNoValidosException(camposInvalidos);
		}
	}

	public Usuario(Usuario data, PasswordEncoder passwordEncoder) throws ParametrosNoValidosException {

		List<String> camposInvalidos = new ArrayList<>();

		this.clave = passwordEncoder.encode(data.getPassword());

		if (UtilesString.isNullOrEmpty(clave)) {
			camposInvalidos.add("clave");
		}

		this.mail = data.getMail();

		if (!UtilesString.esMailValido(mail)) {
			camposInvalidos.add("mail");
		}

		this.nombreUsuario = data.getNombreUsuario();

		if (UtilesString.isNullOrEmpty(nombreUsuario)) {
			camposInvalidos.add("nombre de usuario");
		}

		this.plan = data.plan;

		this.roles = new ArrayList<>();
		roles.add(plan.getRol());

		if (!camposInvalidos.isEmpty()) {
			throw new ParametrosNoValidosException(camposInvalidos);
		}
	}

	public Usuario() {
		roles = new ArrayList<>();
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.getRoles().stream().map(SimpleGrantedAuthority::new).collect(toList());
	}

	@Override
	public String getPassword() {
		return this.clave;
	}

	@Override
	public String getUsername() {
		return this.nombreUsuario;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public List<String> getRoles() {

		List<String> roles = new ArrayList<>();
		for (Rol rol : this.roles) {
			roles.add(rol.getNombre());
		}
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public Allegado getAllegado() {
		return allegado;
	}

	public void setAllegado(Allegado allegado) {
		this.allegado = allegado;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getTokenFirebase() {
		return tokenFirebase;
	}

	public void setTokenFirebase(String tokenFirebase) {
		this.tokenFirebase = tokenFirebase;
	}

	public EstadoUsuario getEstado() {
		return estado;
	}

	public void setEstado(EstadoUsuario estado) {
		this.estado = estado;
	}

	public List<Chat> getChats() {
		return chats;
	}

	public void setChats(List<Chat> chats) {
		this.chats = chats;
	}

	public long getIdAsociado() {
		for (Rol rol : roles) {
			if (rol.getTipoUsuario().equals(TipoUsuario.PACIENTE)) {
				return this.paciente.getId();
			} else {
				if (rol.getTipoUsuario().equals(TipoUsuario.MEDICO)) {
					return this.medico.getId();
				} else {
					if (rol.getTipoUsuario().equals(TipoUsuario.ALLEGADO)) {
						return this.allegado.getId();
					}
				}
			}
		}

		return this.id;
	}

	public Logueable getLogueable() {
		for (Rol rol : roles) {
			if (rol.getTipoUsuario().equals(TipoUsuario.PACIENTE)) {
				return this.paciente;
			} else {
				if (rol.getTipoUsuario().equals(TipoUsuario.MEDICO)) {
					return this.medico;
				} else {
					if (rol.getTipoUsuario().equals(TipoUsuario.ALLEGADO)) {
						return this.allegado;
					}
				}
			}
		}

		return null;
	}

	public boolean esMedico() {
		for (Rol rol : roles) {
			if (rol.getTipoUsuario().equals(TipoUsuario.MEDICO)) {
				return true;
			}
		}
		return false;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getNombre() {
		return getLogueable().getNombre();
	}

}