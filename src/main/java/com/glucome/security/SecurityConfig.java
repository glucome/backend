package com.glucome.security;

import com.glucome.enumerador.Permiso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	JwtTokenProvider jwtTokenProvider;

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorizeRequests = http.httpBasic().disable().csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests();
        authorizeRequests =  authorizeRequests
				.antMatchers("/socket").permitAll()
				.antMatchers("/mercadopago_ipn").permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/api/v1/**").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/auth/signin/").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/auth/signin/medico/").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/auth/cambiarPass/").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/paciente/").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/paciente/**").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/medico/").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/medico/reporte/").permitAll()
				.antMatchers(HttpMethod.POST, "/api/v1/allegado/").permitAll()
				.antMatchers(HttpMethod.POST, "/api/v1/firebase/token").permitAll()
				.antMatchers(HttpMethod.POST, "/api/v1/pago/**").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/usuario/**").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/firebase/enviarNotificacionMedicion").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/firebase/enviarNotificacionMedicacion").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/imagen/**").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/existe_mail/{mail}/").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/existe_nombre_usuario/").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/inicializar/").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/poblar/").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/poblar_real/**").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/inicializar_prod/").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/firebase/").permitAll()
				.antMatchers(HttpMethod.GET,"/api/v1/descargar_historia_clinica/**").permitAll()
				.antMatchers(HttpMethod.GET,"/api/v1/descargar_tarjeta_aumentada/**").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/chat/").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/mercadopago/**").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/pago/**").permitAll()
				.antMatchers(HttpMethod.DELETE, "/api/v1/medico/").permitAll();
        

		for(Permiso permiso : Permiso.values()) {
			authorizeRequests = authorizeRequests.antMatchers(permiso.getMetodo(), permiso.getUrl()).hasAnyRole(permiso.getRoles());
		}


		authorizeRequests.anyRequest().authenticated().and().apply(new JwtConfigurer(jwtTokenProvider));

		// @formatter:on
	}

}