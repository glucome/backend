package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.response.ListadoAlimentoResponse;
import com.glucome.dao.AlimentoDAO;
import com.glucome.enumerador.MomentoAlimento;
import com.glucome.enumerador.ValorIndiceGlucemico;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Alimento;
import com.glucome.utils.Constantes;
import com.glucome.utils.UtilesString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AlimentoServicio extends BaseServicio {

	@Autowired
	private AlimentoDAO alimentoDAO;

	public List<Alimento> getAlimentosMedios(List<MomentoAlimento> momentos) throws PersistenciaException {
		return alimentoDAO.getListado(ValorIndiceGlucemico.MEDIO, momentos);
	}

	public List<Alimento> getAlimentosBajos(List<MomentoAlimento> momentos) throws PersistenciaException {
		return alimentoDAO.getListado(ValorIndiceGlucemico.BAJO, momentos);
	}

	public List<Alimento> getAlimentosAltos(List<MomentoAlimento> momentos) throws PersistenciaException {
		return alimentoDAO.getListado(ValorIndiceGlucemico.ALTO, momentos);
	}

	public Alimento get(long idAlimento) throws PersistenciaException {
		return alimentoDAO.get(idAlimento);
	}

	public RespuestaApi get(String nombre) throws PersistenciaException {
		List<Alimento> alimentos;
		if (UtilesString.isNullOrEmpty(nombre) || nombre.length() < Constantes.CANTIDAD_MINIMA_CARACTERES_BUSQUEDA) {
			alimentos = new ArrayList<>();
		} else {
			alimentos = alimentoDAO.getListado(nombre);
		}

		ListadoAlimentoResponse respuesta = new ListadoAlimentoResponse(alimentos);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

}
