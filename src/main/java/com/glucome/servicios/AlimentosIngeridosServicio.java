package com.glucome.servicios;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.AlimentoIngeridoRequest;
import com.glucome.api.request.ListadoAlimentoIngeridoRequest;
import com.glucome.api.response.AlimentoIngeridoResponse;
import com.glucome.api.response.ListadoAlimentoIngeridoResponse;
import com.glucome.dao.AlimentoDAO;
import com.glucome.dao.AlimentoIngeridoDAO;
import com.glucome.dao.PacienteDAO;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Alimento;
import com.glucome.modelo.AlimentoIngerido;
import com.glucome.modelo.Paciente;
import com.glucome.utils.Constantes;

@Service
public class AlimentosIngeridosServicio extends BaseServicio {

	@Autowired
	AlimentoIngeridoDAO alimentoIngeridoDAO;

	@Autowired
	PacienteDAO pacienteDAO;

	@Autowired
	AlimentoDAO alimentoDAO;

	@Autowired
	AlmacenamientoDeArchivosServicios archivosServicio;

	public RespuestaApi registrarAlimentoIngerido(long idPaciente, AlimentoIngeridoRequest data, MultipartFile file) throws APIException, PacienteException, ParametrosNoValidosException {
		Paciente paciente;
		try {
			paciente = pacienteDAO.get(idPaciente);
		} catch (PersistenciaException e) {
			throw new PacienteException("Error al recuperar el paciente de la base de datos");
		}

		String nombreArchivo;
		try {
			nombreArchivo = archivosServicio.guardarArchivo(file);
		} catch (IOException e1) {
			throw new PacienteException("Error al recuperar grabar la imagen del alimento");
		}

		Alimento alimento;
		try {
			alimento = alimentoDAO.get(data.getAlimento());
		} catch (PersistenciaException e) {
			throw new PacienteException("Error al recuperar el alimento de la base de datos");
		}
		AlimentoIngerido alimentoIngerido = new AlimentoIngerido(data, paciente, alimento, nombreArchivo);
		try {
			this.grabar(alimentoIngerido);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			throw new APIException("Error al grabar el alimento ingerido en la base de datos");
		}
		AlimentoIngeridoResponse medicionResponse = new AlimentoIngeridoResponse(alimentoIngerido.getId());
		return new RespuestaApi(medicionResponse, Constantes.OK);

	}

	public RespuestaApi getListado(ListadoAlimentoIngeridoRequest filtro, long idPaciente) throws APIException {
		List<AlimentoIngerido> alimentosIngeridos;
		try {
			alimentosIngeridos = alimentoIngeridoDAO.getListado(filtro, idPaciente);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			throw new APIException("Error al recuperar los alimentos ingeridos en la base de datos");
		}
		return new RespuestaApi(new ListadoAlimentoIngeridoResponse(alimentosIngeridos, alimentosIngeridos.size() < filtro.getRegistrosPorPagina()), Constantes.OK);
	}

}
