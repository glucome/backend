package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.RegistroAllegadoRequest;
import com.glucome.api.response.ListadoAllegadoResponse;
import com.glucome.dao.AllegadoDAO;
import com.glucome.enumerador.Plan;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Allegado;
import com.glucome.modelo.Usuario;
import com.glucome.utils.Constantes;
import com.glucome.utils.UtilesString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AllegadoServicio extends BaseServicio {

	@Autowired
	UsuarioServicio usuarioServicio;

	@Autowired
	AllegadoDAO allegadoDAO;

	@Autowired
	PasswordEncoder passwordEncoder;

	public RespuestaApi registrar(RegistroAllegadoRequest data) throws PacienteException, ParametrosNoValidosException, IOException {

		Usuario usuario = new Usuario(data, passwordEncoder, Plan.ALLEGADO_BASICO);
		Allegado allegado = new Allegado(data, usuario);

		try {
			usuarioServicio.grabar(usuario);
			usuario.setAllegado(allegado);
			this.grabar(allegado);
		} catch (PersistenciaException e) {
			throw new PacienteException("El usuario o mail ya existe, utilice otro");
		}
		return usuarioServicio.getToken(usuario);
	}

	public RespuestaApi get(String nombre) throws PersistenciaException {
		List<Allegado> allegados;
		if (UtilesString.isNullOrEmpty(nombre) || nombre.length() < Constantes.CANTIDAD_MINIMA_CARACTERES_BUSQUEDA) {
			allegados = new ArrayList<>();
		} else {
			allegados = allegadoDAO.getListado(nombre);
		}

		ListadoAllegadoResponse respuesta = new ListadoAllegadoResponse(allegados);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getListado(long idPaciente) throws PersistenciaException {
		List<Allegado> allegados = allegadoDAO.getListado(idPaciente);
		if (allegados == null) {
			allegados = new ArrayList<>();
		}
		ListadoAllegadoResponse respuesta = new ListadoAllegadoResponse(allegados);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}
}
