package com.glucome.servicios;

import com.glucome.utils.UtilesString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class AlmacenamientoDeArchivosServicios {

	@Value("${file.upload-dir:'C:/Imagenes'}")
	private String rutaImagenes;

	private Path pathCompletoImagen;

	public String guardarArchivo(MultipartFile file) throws IOException {
		if (file == null) {
			return null;
		}

		inicializarStorage();
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		// Check if the file's name contains invalid characters
		if (fileName.contains("..")) {
			throw new IOException("El nombre de archivo es inválido " + fileName);
		}

		String[] archivoConExtension = fileName.split("\\.");
		fileName = archivoConExtension[0].concat(String.valueOf(System.currentTimeMillis())) + "." + archivoConExtension[1];

		// Copy file to the target location (Replacing existing file with the same name)
		Path targetLocation = this.pathCompletoImagen.resolve(fileName);
		Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

		return fileName;

	}

	private void inicializarStorage() throws IOException {
		this.pathCompletoImagen = Paths.get(rutaImagenes).toAbsolutePath().normalize();

		try {
			Files.createDirectories(this.pathCompletoImagen);
		} catch (Exception ex) {
			throw new IOException("No se pudo crear la carpeta para guardar las imagenes", ex);
		}

	}

	public void borrarArchivo(String imagen) {
		if (UtilesString.isNullOrEmpty(imagen)) {
			return;
		}

		try {
			File file = new File(this.rutaImagenes + imagen);
			file.delete();
		} catch (Exception e) {
			System.out.println("Error al borrar imagen");
			return;
		}

	}
}
