package com.glucome.servicios;

import com.glucome.dao.BaseDAO;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.interfaces.Persistente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class BaseServicio {

    @Autowired
    BaseDAO baseDAO;

    public void grabar(Object objeto) throws PersistenciaException {
        baseDAO.grabar(objeto);
    }

    public void borrar(Persistente objeto) throws PersistenciaException, DataIntegrityViolationException {
        baseDAO.borrar(objeto);
    }
}
