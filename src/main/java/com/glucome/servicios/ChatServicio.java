package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.response.*;
import com.glucome.dao.ChatDAO;
import com.glucome.dao.MedicoDAO;
import com.glucome.dao.PacienteDAO;
import com.glucome.dao.UsuarioDAO;
import com.glucome.enumerador.EstadoUsuario;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Chat;
import com.glucome.modelo.Mensaje;
import com.glucome.modelo.Usuario;
import com.glucome.utils.Constantes;
import com.glucome.wrapper.MensajeWrapper;
import com.glucome.wrapper.UsuarioChatWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.List;

@Service
public class ChatServicio extends BaseServicio {

	@Autowired
	ChatDAO chatDAO;

	@Autowired
	UsuarioDAO usuarioDAO;

	@Autowired
	MedicoDAO medicoDAO;

	@Autowired
	PacienteDAO pacienteDAO;

	public RespuestaApi cambiarEstado(Long idUsuario, Boolean online) throws PersistenciaException {
		EstadoUsuario estado = usuarioDAO.cambiarEstado(usuarioDAO.get(idUsuario), online);
		EstadoUsuarioResponse respuesta = new EstadoUsuarioResponse(estado);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi crearChatMedicoPaciente(Long idUsuarioMedico, Long idUsuarioPaciente) throws PersistenciaException {
		Chat chat = chatDAO.crearChatMedicoPaciente(medicoDAO.get(idUsuarioMedico), pacienteDAO.get(idUsuarioPaciente));
		ChatResponse respuesta = new ChatResponse(chat);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getMensajes(Long idChat) throws PersistenciaException {
		List<MensajeWrapper> mensajes = chatDAO.getMensajes(idChat);
		ListadoMensajeWrapperResponse respuesta = new ListadoMensajeWrapperResponse(mensajes);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi guardarMensaje(Long idAutor, Long idDestinatario, Long idChat, String cuerpo, LocalDateTime fechaHora) throws PersistenciaException {
		Usuario autor = usuarioDAO.get(idAutor);
		Usuario destinatario = usuarioDAO.get(idDestinatario);
		Chat chat = chatDAO.get(idChat);
		Mensaje mensaje = chatDAO.guardarMensaje(autor, destinatario, chat, cuerpo, fechaHora);

		MensajeResponse respuesta = new MensajeResponse(mensaje);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi marcarMensajesComoLeidos(Long idChat, Long idAutor, String fechaHoraVisto) throws PersistenciaException, DateTimeParseException {
		List<Mensaje> mensajes = chatDAO.marcarMensajesComoLeidos(idChat, idAutor, LocalDateTime.parse(fechaHoraVisto));
		ListadoMensajeResponse respuesta = new ListadoMensajeResponse(mensajes);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getPacientesMedico(Long idUsuarioMedico) throws PersistenciaException {
		List<UsuarioChatWrapper> usuariosChatsWrapper = chatDAO.getPacientesMedico(idUsuarioMedico);
		ListadoUsuariosChatsResponse respuesta = new ListadoUsuariosChatsResponse(usuariosChatsWrapper);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getMedicosPaciente(Long idUsuarioPaciente) throws PersistenciaException {
		List<UsuarioChatWrapper> usuariosChatsWrapper = chatDAO.getMedicosPaciente(idUsuarioPaciente);
		ListadoUsuariosChatsResponse respuesta = new ListadoUsuariosChatsResponse(usuariosChatsWrapper);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}
}
