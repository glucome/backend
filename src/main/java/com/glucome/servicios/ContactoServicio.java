package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.ContactoRequest;
import com.glucome.api.response.ContactoResponse;
import com.glucome.api.response.ListadoContactoResponse;
import com.glucome.dao.ContactoDAO;
import com.glucome.dao.PacienteDAO;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Contacto;
import com.glucome.modelo.Paciente;
import com.glucome.utils.Constantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContactoServicio extends BaseServicio {

	@Autowired
	PacienteDAO pacienteDAO;

	@Autowired
	ContactoDAO contactoDAO;

	public RespuestaApi registrar(long idPaciente, ContactoRequest data) throws ParametrosNoValidosException, PersistenciaException {
		Paciente paciente = pacienteDAO.get(idPaciente);
		Contacto contacto = new Contacto(data, paciente);
		this.grabar(contacto);
		ContactoResponse response = new ContactoResponse(contacto.getId());
		RespuestaApi respuestaApi = new RespuestaApi(response, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi borrar(long idContacto) throws PersistenciaException {
		Contacto contacto = contactoDAO.get(idContacto);
		this.borrar(contacto);
		ContactoResponse respuesta = new ContactoResponse(contacto.getId());
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi get(long idPaciente) throws PersistenciaException {
		List<Contacto> contacto = contactoDAO.getListado(idPaciente);
		if (contacto == null) {
			contacto = new ArrayList<>();
		}
		ListadoContactoResponse respuesta = new ListadoContactoResponse(contacto);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

}
