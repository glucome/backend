package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.EjercicioRequest;
import com.glucome.api.request.ListadoEjercicioRequest;
import com.glucome.api.response.EjercicioResponse;
import com.glucome.api.response.ListadoEjercicioResponse;
import com.glucome.dao.EjercicioDAO;
import com.glucome.dao.PacienteDAO;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Ejercicio;
import com.glucome.modelo.Paciente;
import com.glucome.utils.Constantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EjercicioServicio extends BaseServicio {

	@Autowired
	EjercicioDAO ejercicioDAO;

	@Autowired
	PacienteDAO pacienteDAO;

	public RespuestaApi registrarEjercicio(long idPaciente, EjercicioRequest data) throws APIException, PacienteException, ParametrosNoValidosException {
		Paciente paciente;
		try {
			paciente = pacienteDAO.get(idPaciente);
		} catch (PersistenciaException e) {
			throw new PacienteException("Error al recuperar el paciente de la base de datos");
		}
		Ejercicio ejercicio = new Ejercicio(data, paciente);
		try {
			this.grabar(ejercicio);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			throw new APIException("Error al grabar el ejercicio en la base de datos");
		}
		EjercicioResponse medicionResponse = new EjercicioResponse(ejercicio.getId());
		return new RespuestaApi(medicionResponse, Constantes.OK);

	}

	public RespuestaApi getListado(ListadoEjercicioRequest filtro, long idPaciente) throws APIException {
		List<Ejercicio> ejercicios;
		try {
			ejercicios = ejercicioDAO.getListado(filtro, idPaciente);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			throw new APIException("Error al recuperar los ejercicios en la base de datos");
		}
		return new RespuestaApi(new ListadoEjercicioResponse(ejercicios, ejercicios.size() < filtro.getRegistrosPorPagina()), Constantes.OK);
	}

}
