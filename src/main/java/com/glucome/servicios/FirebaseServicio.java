package com.glucome.servicios;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.LoginRequest;
import com.glucome.api.response.FirebaseTokenResponse;
import com.glucome.api.response.LoginResponse;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Usuario;
import com.glucome.utils.Constantes;
import com.glucome.utils.HeaderRequestInterceptor;
import com.google.gson.JsonObject;

@Service
public class FirebaseServicio extends BaseServicio {

	private static final String FIREBASE_SERVER_KEY = "AIzaSyCmVKioPAfNV0cxES99FA_IMJWHy0geRhE";
	private static final String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";

	@Autowired
	UsuarioServicio usuarioServicio;

	@Async
	public CompletableFuture<String> enviarAlertaMedicionAUsuario(
			long idPaciente, long idAvisado, String valorMedicion, String ubicacion) 
					throws PersistenciaException, HttpClientErrorException {

		Usuario paciente = usuarioServicio.get(idPaciente);
		Usuario avisado = usuarioServicio.get(idAvisado);

		JsonObject aEnviar = new JsonObject();
		JsonObject noti = new JsonObject();
		JsonObject data = new JsonObject();

		String title = paciente.getNombre() + " tuvo una medicion de " + valorMedicion + "mg/dl";

		noti.addProperty("title", title);
		noti.addProperty("body", "Enviale un mensaje para preguntarle como se encuentra!");
		noti.addProperty("click_action", "com.glucomeandroidapp.TARGET_NOTIFICATION");

		data.addProperty("ubicacion", ubicacion);
		data.addProperty("mensaje", title);

		aEnviar.add("notification", noti);
		aEnviar.add("data", data);
		aEnviar.addProperty("to", avisado.getTokenFirebase());

		org.springframework.http.HttpEntity<String> request = 
				new org.springframework.http.HttpEntity<String>(aEnviar.toString());

		RestTemplate restTemplate = new RestTemplate();

		ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
		interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + FIREBASE_SERVER_KEY));
		interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
		restTemplate.setInterceptors(interceptors);

		String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, request, String.class);

		return CompletableFuture.completedFuture(firebaseResponse);
	}

	@Async
	public CompletableFuture<String> enviarAlertaMedicacionAUsuario(
			long idPaciente, long idAvisado, String hora) 
					throws PersistenciaException, HttpClientErrorException {

		Usuario paciente = usuarioServicio.get(idPaciente);
		Usuario avisado = usuarioServicio.get(idAvisado);

		JsonObject aEnviar = new JsonObject();
		JsonObject noti = new JsonObject();

		// TODO ver porque no deja mandar con tildes (Si las agregas solas acá en la
		// notificación llega el mensaje mal escrito)
		noti.addProperty("title", paciente.getNombre() + " no tomo su medicacion de las " + hora + "hrs");
		noti.addProperty("body", "Enviale un mensaje para preguntarle como se encuentra!");

		aEnviar.add("notification", noti);
		aEnviar.addProperty("to", avisado.getTokenFirebase());

		org.springframework.http.HttpEntity<String> request = 
				new org.springframework.http.HttpEntity<String>(aEnviar.toString());

		RestTemplate restTemplate = new RestTemplate();

		ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
		interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + FIREBASE_SERVER_KEY));
		interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
		restTemplate.setInterceptors(interceptors);

		String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, request, String.class);

		return CompletableFuture.completedFuture(firebaseResponse);
	}

	public RespuestaApi setearTokenFirebaseDeUsuario(LoginRequest user, String token) throws PersistenciaException {
		LoginResponse respuesta = new LoginResponse();
		try {
			Usuario usuario = usuarioServicio.getUsuario(user);
			usuario.setTokenFirebase(token);
			this.grabar(usuario);
			respuesta.setId(usuario.getId());
			respuesta.setUsuario(usuario.getNombreUsuario());
			respuesta.setAsociado(usuario.getLogueable());
			respuesta.setRoles(usuario.getRoles());
			return new RespuestaApi(respuesta, Constantes.OK);
		} catch (AuthenticationException e) {
			throw new BadCredentialsException("Usuario o clave inválida");
		}
	}

	public RespuestaApi obtenerTokenFirebaseDeUsuario(long usuarioId) throws PersistenciaException {
		Usuario usuario = usuarioServicio.get(usuarioId);
		String token = usuario.getTokenFirebase();
		FirebaseTokenResponse ftr = new FirebaseTokenResponse(token);
		return new RespuestaApi(ftr, Constantes.OK);
	}

}
