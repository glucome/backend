package com.glucome.servicios;

import com.glucome.api.Response;

public class HemoglobinaGlicosildaResponse implements Response {

	private double hemoglobinaGlicosilada;

	public HemoglobinaGlicosildaResponse(double hemoglobinaGlicosilada) {
		this.hemoglobinaGlicosilada = hemoglobinaGlicosilada;
	}

	public double getHemoglobinaGlicosilada() {
		return hemoglobinaGlicosilada;
	}

	public void setHemoglobinaGlicosilada(double hemoglobinaGlicosilada) {
		this.hemoglobinaGlicosilada = hemoglobinaGlicosilada;
	}

}
