package com.glucome.servicios;

import com.glucome.api.Response;
import com.glucome.wrapper.HemoglobinaGlicosiladaAnualWrapper;

public class HemoglobinaGlicosildaUltimoAnioResponse implements Response {

	private HemoglobinaGlicosiladaAnualWrapper hemoglobinaGlicosiladaAnual;

	public HemoglobinaGlicosildaUltimoAnioResponse(HemoglobinaGlicosiladaAnualWrapper hemoglobinaGlicosiladaAnual) {
		this.hemoglobinaGlicosiladaAnual = hemoglobinaGlicosiladaAnual;
	}

	public HemoglobinaGlicosiladaAnualWrapper getHemoglobinaGlicosiladaAnual() { return hemoglobinaGlicosiladaAnual; }

	public void setHemoglobinaGlicosiladaAnual(HemoglobinaGlicosiladaAnualWrapper hemoglobinaGlicosiladaAnual) { this.hemoglobinaGlicosiladaAnual = hemoglobinaGlicosiladaAnual; }
}
