package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.InsulinaInyectadaRequest;
import com.glucome.api.request.ListadoInyeccionInsulinaRequest;
import com.glucome.api.response.InsulinaInyecadaResponse;
import com.glucome.api.response.ListadoInyeccionInsulinaResponse;
import com.glucome.dao.InsulinaInyectadaDAO;
import com.glucome.dao.PacienteDAO;
import com.glucome.excepciones.InsulinaException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.InsulinaInyectada;
import com.glucome.modelo.Paciente;
import com.glucome.utils.Constantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InsulinaServicio extends BaseServicio {

    @Autowired
    InsulinaInyectadaDAO insulinaDAO;

    @Autowired
    PacienteDAO pacienteDAO;

    public RespuestaApi registrarInyeccion(long idPaciente, InsulinaInyectadaRequest data) throws InsulinaException, PacienteException, ParametrosNoValidosException {
        Paciente paciente;
        try {
            paciente = pacienteDAO.get(idPaciente);
        } catch (PersistenciaException e) {
            throw new PacienteException("Error al recuperar el paciente de la base de datos");
        }
        InsulinaInyectada insulinaInyectada = new InsulinaInyectada(data, paciente);
        try {
            this.grabar(insulinaInyectada);
        } catch (PersistenciaException e) {
            e.printStackTrace();
            throw new InsulinaException("Error al grabar la insulina inyectada en la base de datos");
        }
        InsulinaInyecadaResponse medicionResponse = new InsulinaInyecadaResponse(insulinaInyectada.getId());
        return new RespuestaApi(medicionResponse, Constantes.OK);

    }

    public RespuestaApi getListado(ListadoInyeccionInsulinaRequest filtro, long idPaciente) throws InsulinaException {
        List<InsulinaInyectada> inyecciones;
        try {
            inyecciones = insulinaDAO.getListado(filtro, idPaciente);
        } catch (PersistenciaException e) {
            e.printStackTrace();
            throw new InsulinaException("Error al recuperar las inyecciones en la base de datos");
        }
        return new RespuestaApi(new ListadoInyeccionInsulinaResponse(inyecciones, inyecciones.size() < filtro.getRegistrosPorPagina()), Constantes.OK);
    }

}
