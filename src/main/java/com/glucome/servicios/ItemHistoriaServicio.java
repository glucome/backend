package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.ItemHistoriaRequest;
import com.glucome.api.request.ListadoItemHistoriaRequest;
import com.glucome.api.response.ItemHistoriaResponse;
import com.glucome.api.response.ListadoItemHistoriaInsulinaResponse;
import com.glucome.dao.ItemHistoriaDAO;
import com.glucome.dao.MedicoDAO;
import com.glucome.dao.PacienteDAO;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.ItemHistoriaClinica;
import com.glucome.modelo.Medico;
import com.glucome.modelo.Paciente;
import com.glucome.utils.Constantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemHistoriaServicio extends BaseServicio {

	@Autowired
	PacienteDAO pacienteDAO;

	@Autowired
	MedicoDAO medicoDAO;

	@Autowired
	ItemHistoriaDAO itemHistoriaDAO;

	public RespuestaApi registrarItemHistoria(long idPaciente, ItemHistoriaRequest data) throws PacienteException, APIException, ParametrosNoValidosException {
		Paciente paciente;
		try {
			paciente = pacienteDAO.get(idPaciente);
		} catch (PersistenciaException e) {
			throw new PacienteException("Error al recuperar el paciente de la base de datos");
		}

		Medico medico;
		try {
			medico = medicoDAO.get(data.getMedico());
		} catch (PersistenciaException e1) {
			throw new PacienteException("Error al recuperar el médico de la base de datos");
		}
		ItemHistoriaClinica item;
		try {
			item = new ItemHistoriaClinica(data, paciente, medico);
			this.grabar(item);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			throw new APIException("Error al grabar la insulina inyectada en la base de datos");
		}
		ItemHistoriaResponse medicionResponse = new ItemHistoriaResponse(item.getId());
		return new RespuestaApi(medicionResponse, Constantes.OK);
	}

	public RespuestaApi getListado(ListadoItemHistoriaRequest data, long idPaciente) throws APIException {
		List<ItemHistoriaClinica> inyecciones;
		try {
			inyecciones = itemHistoriaDAO.getListado(data, idPaciente);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			throw new APIException("Error al recuperar la historia en la base de datos");
		}
		return new RespuestaApi(new ListadoItemHistoriaInsulinaResponse(inyecciones, inyecciones.size() < data.getRegistrosPorPagina()), Constantes.OK);
	}

	public RespuestaApi borrarItemHistoria(long id) throws APIException {
		ItemHistoriaClinica item;
		try {
			item = itemHistoriaDAO.get(id);
			this.borrar(item);
		} catch (PersistenciaException e) {
			throw new APIException("No se pudo borrar la entrada a la historia clínica, intente nuevamente mas tarde");
		}
		ItemHistoriaResponse respuesta = new ItemHistoriaResponse(item.getId());
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getItemHistoria(long itemId) throws PersistenciaException {
		ItemHistoriaClinica item = this.itemHistoriaDAO.get(itemId);
		ItemHistoriaResponse respuesta = new ItemHistoriaResponse(item.getId());
		respuesta.setItem(item);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;

	}

}
