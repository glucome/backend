package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.ListadoMedicamentoTomadosRequest;
import com.glucome.api.request.MedicamentoPacienteRequest;
import com.glucome.api.request.MedicamentoTomadoPacienteRequest;
import com.glucome.api.response.BooleanResponse;
import com.glucome.api.response.ListadoMedicamentoResponse;
import com.glucome.api.response.ListadoMedicamentosIngeridosResponse;
import com.glucome.api.response.MedicamentoTomadoResponse;
import com.glucome.dao.MedicamentoDAO;
import com.glucome.dao.PacienteDAO;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Medicamento;
import com.glucome.modelo.MedicamentoIngerido;
import com.glucome.modelo.Paciente;
import com.glucome.utils.Constantes;
import com.glucome.utils.UtilesString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MedicamentoServicio extends BaseServicio {

	@Autowired
	MedicamentoDAO medicamentoDAO;

	@Autowired
	PacienteDAO pacienteDAO;

	public RespuestaApi get(String nombre, long paciente) throws PersistenciaException {
		List<Medicamento> medicamentos;
		if (UtilesString.isNullOrEmpty(nombre) || nombre.length() < Constantes.CANTIDAD_MINIMA_CARACTERES_BUSQUEDA) {
			medicamentos = new ArrayList<>();
		} else {
			medicamentos = medicamentoDAO.getListado(nombre, paciente);
		}

		ListadoMedicamentoResponse respuesta = new ListadoMedicamentoResponse(medicamentos);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getListado(long idPaciente) throws PersistenciaException {
		List<Medicamento> medicamentos = medicamentoDAO.getListado(idPaciente);
		if (medicamentos == null) {
			medicamentos = new ArrayList<>();
		}
		ListadoMedicamentoResponse respuesta = new ListadoMedicamentoResponse(medicamentos);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi agregarMedicamento(MedicamentoPacienteRequest data) throws PersistenciaException {
		Medicamento medicamento = medicamentoDAO.get(data.getIdMedicamento());
		Paciente paciente = pacienteDAO.get(data.getIdPaciente());
		paciente.agregarMedicamento(medicamento);
		this.grabar(medicamento);
		BooleanResponse response = new BooleanResponse(true);
		RespuestaApi respuestaApi = new RespuestaApi(response, Constantes.OK);
		return respuestaApi;

	}

	public RespuestaApi agregarMedicamentoTomado(MedicamentoTomadoPacienteRequest data, long idPaciente) throws PersistenciaException, ParametrosNoValidosException {
		Medicamento medicamento = medicamentoDAO.get(data.getIdMedicamento());
		Paciente paciente = pacienteDAO.get(idPaciente);
		MedicamentoIngerido medicamentoTomado = new MedicamentoIngerido(data, medicamento, paciente);
		this.grabar(medicamentoTomado);
		MedicamentoTomadoResponse response = new MedicamentoTomadoResponse(medicamentoTomado.getId());
		RespuestaApi respuestaApi = new RespuestaApi(response, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi borradoMedicamento(long idMedicamento) throws APIException {
		MedicamentoIngerido medicamento;
		try {
			medicamento = medicamentoDAO.getIngerido(idMedicamento);
			this.borrar(medicamento);
		} catch (PersistenciaException e) {
			throw new APIException("No se pudo borrar el medicamento ingerido, intente nuevamente mas tarde");
		}
		MedicamentoTomadoResponse response = new MedicamentoTomadoResponse(medicamento.getId());
		RespuestaApi respuestaApi = new RespuestaApi(response, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getListadoIngeridos(ListadoMedicamentoTomadosRequest data, long idPaciente) throws APIException {
		List<MedicamentoIngerido> medicaciones;
		try {
			medicaciones = medicamentoDAO.getListadoIngeridos(data, idPaciente, true);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			throw new APIException("Error al recuperar las medicaciones en la base de datos");
		}
		return new RespuestaApi(new ListadoMedicamentosIngeridosResponse(medicaciones, medicaciones.size() < data.getRegistrosPorPagina()), Constantes.OK);
	}

}
