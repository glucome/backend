package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.ListadoMedicionRequest;
import com.glucome.api.request.MedicionRequest;
import com.glucome.api.response.ListadoMedicionesAppResponse;
import com.glucome.api.response.ListadoMedicionesResponse;
import com.glucome.api.response.MedicionPacienteResponse;
import com.glucome.api.response.MedicionResponse;
import com.glucome.dao.MedicionDAO;
import com.glucome.dao.PacienteDAO;
import com.glucome.excepciones.MedicionException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Medicion;
import com.glucome.modelo.Paciente;
import com.glucome.utils.Constantes;
import com.glucome.utils.XlsUtils;
import com.glucome.wrapper.HemoglobinaGlicosiladaAnualWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Service
public class MedicionServicio extends BaseServicio {

	@Autowired
	private MedicionDAO medicionDAO;

	@Autowired
	PacienteDAO pacienteDAO;

	public RespuestaApi getListado(ListadoMedicionRequest filtro, long idPaciente) throws MedicionException {
		List<Medicion> mediciones;
		try {
			mediciones = medicionDAO.getListado(filtro, idPaciente, true);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			throw new MedicionException("Error al recuperar las mediciones en la base de datos");
		}
		return new RespuestaApi(new ListadoMedicionesResponse(mediciones, mediciones.size() < filtro.getRegistrosPorPagina()), Constantes.OK);
	}

	public RespuestaApi getListadoSinFiltro(long idPaciente) throws MedicionException {
		List<Medicion> mediciones;
		try {
			mediciones = medicionDAO.getListadoSinFiltro(idPaciente);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			throw new MedicionException("Error al recuperar las mediciones en la base de datos");
		}
		return new RespuestaApi(new ListadoMedicionesAppResponse(MedicionPacienteResponse.getFromMedicion(mediciones)), Constantes.OK);
	}

	public RespuestaApi registrarMedicion(long idPaciente, MedicionRequest data) throws PacienteException, MedicionException, ParametrosNoValidosException {
		Paciente paciente;
		try {
			paciente = pacienteDAO.get(idPaciente);
		} catch (PersistenciaException e) {
			throw new PacienteException("Error al recuperar el paciente de la base de datos");
		}
		Medicion medicion = new Medicion(data, paciente);
		try {
			this.grabar(medicion);
		} catch (PersistenciaException e) {
			throw new MedicionException("Error al grabar la medición en la base de datos");
		}
		MedicionResponse medicionResponse = new MedicionResponse(medicion.getId());
		return new RespuestaApi(medicionResponse, Constantes.OK);
	}

	public Medicion getUltimaMedicion(long idPaciente) throws PersistenciaException {
		return medicionDAO.getUltimaMedicion(idPaciente);
	}

	public List<Medicion> getUltimasMediciones(long idPaciente) throws PersistenciaException {
		List<Medicion> ultimasMediciones = medicionDAO.getUltimasMediciones(idPaciente);
		Collections.sort(ultimasMediciones, (x, y) -> x.getFechaHora().compareTo(y.getFechaHora()));
		return ultimasMediciones;
	}

	public void descargarMediciones(ListadoMedicionRequest filtro, HttpServletResponse response, long idPaciente) throws IOException, MedicionException {
		List<Medicion> mediciones;
		Paciente paciente = null;
		try {
			paciente = pacienteDAO.get(idPaciente);
			mediciones = medicionDAO.getListado(filtro, idPaciente, false);
			XlsUtils.descargarXls(response, "Mediciones", paciente, Medicion.getCabeceraXls(), mediciones);
		} catch (PersistenciaException e) {
			throw new MedicionException("Error al recuperar las mediciones en la base de datos");
		}
	}

	public double getHemoglobinaGlicosilada(long idPaciente) throws PersistenciaException {
		double glucosaPromedio = medicionDAO.getGlucosaPromedioUltimos3Meses(idPaciente);
		return (glucosaPromedio + Constantes.PRIMER_TERMINO_HEMOGLOBINA_GLICOSILADA) / Constantes.SEGUNDO_TERMINO_HEMOGLOBINA_GLICOSILADA;
	}

	public HemoglobinaGlicosiladaAnualWrapper getHemoglobinaGlicosiladaAnual(long idPaciente) throws PersistenciaException {
		HemoglobinaGlicosiladaAnualWrapper hemoglobinaGlicosiladaAnual = medicionDAO.getHemoglobinaGlicosiladaAnual(idPaciente);
		return hemoglobinaGlicosiladaAnual;
	}
}
