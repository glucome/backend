package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.RegistroMedicoRequest;
import com.glucome.api.response.BooleanResponse;
import com.glucome.api.response.ListadoMedicoResponse;
import com.glucome.api.response.UbicacionResponse;
import com.glucome.dao.MedicoDAO;
import com.glucome.enumerador.Plan;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Medico;
import com.glucome.modelo.Usuario;
import com.glucome.utils.Constantes;
import com.glucome.utils.UtilesString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MedicoServicio extends BaseServicio {

	@Autowired
	UsuarioServicio usuarioServicio;

	@Autowired
	MedicoDAO medicoDAO;

	@Autowired
	PasswordEncoder passwordEncoder;

	public RespuestaApi registrar(RegistroMedicoRequest data) throws PacienteException, ParametrosNoValidosException {

		Usuario usuario = new Usuario(data, passwordEncoder, Plan.MEDICO_BASICO);

		Medico medico = new Medico(data, usuario);
		try {
			usuarioServicio.grabar(usuario);
			usuario.setMedico(medico);
			this.grabar(medico);
		} catch (PersistenciaException e) {
			throw new PacienteException("El usuario o mail ya existe, utilice otro");
		}
		return usuarioServicio.getToken(usuario);
	}

	public RespuestaApi get(String nombre) throws PersistenciaException {
		List<Medico> medicos;
		if (UtilesString.isNullOrEmpty(nombre) || nombre.length() < Constantes.CANTIDAD_MINIMA_CARACTERES_BUSQUEDA) {
			medicos = new ArrayList<>();
		} else {
			medicos = medicoDAO.getListado(nombre);
		}

		ListadoMedicoResponse respuesta = new ListadoMedicoResponse(medicos);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getListado(long idPaciente) throws PersistenciaException {
		List<Medico> medicos = medicoDAO.getListado(idPaciente);
		if (medicos == null) {
			medicos = new ArrayList<>();
		}
		ListadoMedicoResponse respuesta = new ListadoMedicoResponse(medicos);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi editarMedico(RegistroMedicoRequest data) throws PersistenciaException, APIException {
		Medico medico = medicoDAO.get(data.getId());
		if (medico != null) {
			medico.setDescripcion(data.getDescripcion());
			medico.setMatricula(data.getMatricula());
			medico.setNombre(data.getNombre());
			this.grabar(medico);
			return usuarioServicio.getToken(medico.getUsuario());
		}
		throw new APIException("Médico inválido");
	}

	public Medico getByCollectorId(String preferenceId) throws PersistenciaException {
		return medicoDAO.getByPreferenceId(preferenceId);
	}

    public RespuestaApi getUbicacion(Long idMedico) throws PersistenciaException {
	    String ubicacion = medicoDAO.getUbicacion(idMedico);
        UbicacionResponse respuesta = new UbicacionResponse(ubicacion);
        RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
        return respuestaApi;
    }

	public RespuestaApi guardarUbicacion(Long idMedico, String ubicacion) throws PersistenciaException, APIException {
		Medico medico = medicoDAO.get(idMedico);
		if (medico != null) {
			medico.setUbicacion(ubicacion);
			this.grabar(medico);
			UbicacionResponse respuesta = new UbicacionResponse(medico.getUbicacion());
			return new RespuestaApi(respuesta, Constantes.OK);
		}

		throw new APIException("Médico inválido");
	}

	public RespuestaApi borrarUbicacion(Long idMedico) throws PersistenciaException, APIException {
		Medico medico = medicoDAO.get(idMedico);
		if (medico != null) {
			medico.setUbicacion(null);
			this.grabar(medico);
			BooleanResponse respuesta = new BooleanResponse(true);
			return new RespuestaApi(respuesta, Constantes.OK);
		}

		throw new APIException("Médico inválido");
	}
}
