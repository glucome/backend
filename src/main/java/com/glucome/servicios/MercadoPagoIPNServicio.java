package com.glucome.servicios;

import com.glucome.enumerador.EstadoPago;
import com.glucome.enumerador.Plan;
import com.glucome.enumerador.TipoUsuario;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mercadopago.MercadoPago;
import com.mercadopago.core.MPApiResponse;
import com.mercadopago.exceptions.MPException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class MercadoPagoIPNServicio extends BaseServicio {

	@Autowired
	PlanServicio planServicio;

	@Autowired
	PagoServicio pagoServicio;

	public HttpStatus ipn(String topic, String id) throws MPException, PersistenciaException, APIException, PacienteException, ParametrosNoValidosException {
		final String BASE_URL = MercadoPago.SDK.getBaseUrl();

		MPApiResponse MPPaymentResponse = MercadoPago.SDK.Get(BASE_URL + "/v1/payments/" + id + "?access_token=" + MercadoPago.SDK.getAccessToken());
		JsonObject payment = MPPaymentResponse.getJsonElementResponse().getAsJsonObject();

		MPPaymentResponse = MercadoPago.SDK.Get(BASE_URL + "/merchant_orders/" + payment.get("order").getAsJsonObject().get("id").getAsString() + "?access_token=" + MercadoPago.SDK.getAccessToken());
		JsonObject merchantOrder = MPPaymentResponse.getJsonElementResponse().getAsJsonObject();

		String[] externalReference = payment.get("external_reference").getAsString().split("-");
		String tipoUsuario = externalReference[0];
		Long idUsuario = Long.valueOf(externalReference[1]);

		BigDecimal total = BigDecimal.ZERO;
		for (JsonElement merchantOrderPayment : merchantOrder.get("payments").getAsJsonArray()) {
			if (merchantOrderPayment.getAsJsonObject().get("status").getAsString().equals(EstadoPago.APROBADO.getEstadoMercadoPago())) {
				total = total.add(merchantOrderPayment.getAsJsonObject().get("transaction_amount").getAsBigDecimal());
			}
		}

		int resultado = total.compareTo(merchantOrder.get("total_amount").getAsBigDecimal());
		if(resultado == 0 || resultado == 1) {
			if(tipoUsuario.equals(TipoUsuario.MEDICO.name())) {
				planServicio.setSuscripcionMedico(Plan.MEDICO_PREMIUM, idUsuario);
			} else if(tipoUsuario.equals(TipoUsuario.PACIENTE.name())){
				planServicio.setSuscripcionPaciente(Plan.PACIENTE_PREMIUM, idUsuario);
			}
		}

		pagoServicio.grabar(idUsuario, payment);

		return HttpStatus.OK;
	}
}
