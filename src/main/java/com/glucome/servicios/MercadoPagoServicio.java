package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.response.MercadoPagoInitPointResponse;
import com.glucome.enumerador.TipoUsuario;
import com.glucome.utils.Constantes;
import com.mercadopago.MercadoPago;
import com.mercadopago.exceptions.MPException;
import com.mercadopago.resources.Preference;
import com.mercadopago.resources.datastructures.preference.BackUrls;
import com.mercadopago.resources.datastructures.preference.Item;
import com.mercadopago.resources.datastructures.preference.Payer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MercadoPagoServicio extends BaseServicio {

	@Autowired
	UsuarioServicio usuarioServicio;

	@Value( "${mp.access-token}" )
	private String ACCESS_TOKEN;

	private final String TITLE = "Gluko";
	private final String CURRENCY_ID = "ARS";
	private final String DESCRIPTION = "Suscripción Premium";
	private final float UNIT_PRICE = 100;

	private final String SUCCESS_URL = "localhost:4200/subscription-success";
	private final String PENDING_URL = "localhost:4200/subscription-pending";
	private final String FAILURE_URL = "localhost:4200/subscription-failure";

	public RespuestaApi getInitPoint(Long idUsuario, TipoUsuario tipoUsuario) throws MPException {
		MercadoPago.SDK.setAccessToken(ACCESS_TOKEN);

		Preference preference = new Preference();

		Item item = new Item();
		item.setTitle(TITLE)
				.setCurrencyId(CURRENCY_ID)
				.setDescription(DESCRIPTION)
				.setQuantity(1)
				.setUnitPrice(UNIT_PRICE);
		preference.appendItem(item);

		preference.setPayer(new Payer());
		preference.setExternalReference(tipoUsuario.name() + "-" + idUsuario);
		preference.setBackUrls(new BackUrls(SUCCESS_URL, PENDING_URL, FAILURE_URL));
		preference.save();

		MercadoPagoInitPointResponse respuesta = new MercadoPagoInitPointResponse(preference.getInitPoint());
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}
}
