package com.glucome.servicios;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.StringTemplateGroup;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.glucome.api.request.ListadoItemHistoriaRequest;
import com.glucome.dao.ItemHistoriaDAO;
import com.glucome.dao.PacienteDAO;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.ItemHistoriaClinica;
import com.glucome.modelo.Paciente;
import com.glucome.utils.Constantes;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.Pipeline;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

@Service
public class PDFServicio {
	@Autowired
	PacienteDAO pacienteDAO;

	@Autowired
	UsuarioServicio usuarioServicio;

	@Autowired
	ItemHistoriaDAO itemHistoriaDAO;

	@Value("${file.upload-dir:'C:/Imagenes/'}")
	private String rutaImagenes;

	public void descargarPDFHistoriaClinica(HttpServletRequest request, HttpServletResponse response, long idMedico, long idPaciente, ListadoItemHistoriaRequest filtro) throws IOException {
		OutputStream os = null;
		try {

			Paciente paciente = pacienteDAO.get(idPaciente);
			List<ItemHistoriaClinica> entradas = itemHistoriaDAO.getListadoSinPaginar(idPaciente, filtro);
			File file = new File(rutaImagenes + "logo_nombre.png");
			StringTemplate page = getStringTemplate(Constantes.TEMPLATE_HISTORIA_CLINICA);
			page.setAttribute("paciente", paciente);
			page.setAttribute("entradas", entradas);
			page.setAttribute("rutaImagen", file.getAbsolutePath());
			String nombrePDF = "historia_" + paciente.getNombre().replace(" ", "_");

			os = imprimirPDF(response, page, nombrePDF);
		} catch (DocumentException | PersistenciaException e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				os.flush();
				os.close();
			}
		}

	}

	private OutputStream imprimirPDF(HttpServletResponse response, StringTemplate page, String nombrePDF) throws IOException, DocumentException {
		OutputStream os;
		String content = page.toString();

		final HtmlCleaner htmlCleaner = new HtmlCleaner();
		final TagNode tagNode = htmlCleaner.clean(content);
		content = htmlCleaner.getInnerHtml(tagNode);

		os = response.getOutputStream();

		Document document = null;

		document = new Document(PageSize.A4);

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		final PdfWriter writer = PdfWriter.getInstance(document, baos);
		document.open();

		final HtmlPipelineContext htmlContext = new HtmlPipelineContext();

		htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());

		final CSSResolver cssResolver = XMLWorkerHelper.getInstance().getDefaultCssResolver(true);
		final Pipeline<?> pipeline = new CssResolverPipeline(cssResolver, new HtmlPipeline(htmlContext, new PdfWriterPipeline(document, writer)));
		final XMLWorker worker = new XMLWorker(pipeline, true);
		final XMLParser parser = new XMLParser(worker);

		try {
			parser.parse(new StringReader(content));
		} catch (final Exception e) {
			e.printStackTrace();
		}

		document.close();
		response.setContentType("Content-Type: text/html; charset=UTF-8");
		response.addHeader("Content-Disposition", "attachment; filename=" + nombrePDF + ".pdf");
		response.setContentLength(baos.size());
		baos.writeTo(os);
		return os;
	}

	StringTemplate getStringTemplate(String nombreTemplate) {
		final StringTemplateGroup group = new StringTemplateGroup("Generators");
		return group.getInstanceOf(nombreTemplate);
	}

	public void descargarTarjetaAumentada(HttpServletRequest request, HttpServletResponse response, long idPaciente) throws IOException, WriterException {
		OutputStream os = null;
		try {

			Paciente paciente = pacienteDAO.get(idPaciente);
			File logoGluko = new File(rutaImagenes + "logo_nombre.png");
			File fotoPaciente = usuarioServicio.getImagenUsuario(paciente.getIdUsuario());

			StringTemplate page = getStringTemplate(Constantes.TEMPLATE_TARJETA_AUMENTADA);
			page.setAttribute("paciente", paciente);
			page.setAttribute("rutaImagen", logoGluko.getAbsolutePath());
			page.setAttribute("rutaImagenPaciente", fotoPaciente.getAbsolutePath());
			String filePath = rutaImagenes + "temp.png";

			File file = new File(filePath);
			generateQRCodeImage(String.valueOf(paciente.getId()), 400, 400, filePath);
			page.setAttribute("bytesQR", file.getAbsolutePath());
			String nombrePDF = "tarjeta_" + paciente.getNombre().replace(" ", "_");
			// file.delete();
			os = imprimirPDF(response, page, nombrePDF);
		} catch (DocumentException | PersistenciaException e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				os.flush();
				os.close();
			}
		}
	}

	private static void generateQRCodeImage(String text, int width, int height, String filePath) throws WriterException, IOException {
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

		Path path = FileSystems.getDefault().getPath(filePath);
		MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
	}

}
