package com.glucome.servicios;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.FactorCorreccionInsulinaRequest;
import com.glucome.api.request.RegistroPacienteExternoRequest;
import com.glucome.api.request.RegistroPacienteRequest;
import com.glucome.api.response.BooleanResponse;
import com.glucome.api.response.FactorCorreccionInsulinaResponse;
import com.glucome.api.response.IdResponse;
import com.glucome.api.response.InfoTarjetaResponse;
import com.glucome.api.response.ListadoPacientesResponse;
import com.glucome.api.response.LogMedicionesResponse;
import com.glucome.api.response.PacienteExternoResponse;
import com.glucome.api.response.UltimaMedicionResponse;
import com.glucome.api.response.UltimasMedicionesResponse;
import com.glucome.dao.AllegadoDAO;
import com.glucome.dao.MedicoDAO;
import com.glucome.dao.PacienteDAO;
import com.glucome.dao.SolicitudDAO;
import com.glucome.enumerador.Plan;
import com.glucome.enumerador.TipoSolicitud;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Allegado;
import com.glucome.modelo.FactorCorreccionInsulina;
import com.glucome.modelo.Medicion;
import com.glucome.modelo.Medico;
import com.glucome.modelo.Paciente;
import com.glucome.modelo.SolicitudVinculacion;
import com.glucome.modelo.Usuario;
import com.glucome.utils.Constantes;
import com.glucome.utils.UtilesNumeros;
import com.glucome.wrapper.HemoglobinaGlicosiladaAnualWrapper;
import com.glucome.wrapper.LogMedicionWrapper;

@Service
public class PacienteServicio extends BaseServicio {

	@Autowired
	UsuarioServicio usuarioServicio;

	@Autowired
	PacienteDAO pacienteDAO;

	@Autowired
	MedicoDAO medicoDAO;

	@Autowired
	AllegadoDAO allegadoDAO;

	@Autowired
	SolicitudDAO solicitudDAO;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	private MedicionServicio medicionServicio;

	public RespuestaApi registrar(RegistroPacienteRequest data) throws PacienteException, ParametrosNoValidosException, IOException {

		Usuario usuario = new Usuario(data, passwordEncoder, Plan.PACIENTE_BASICO);
		Paciente paciente = new Paciente(data, usuario);

		try {
			usuarioServicio.grabar(usuario);
			usuario.setPaciente(paciente);
			this.grabar(paciente);
		} catch (PersistenciaException e) {
			throw new PacienteException("El usuario o mail ya existe, utilice otro");
		}
		return usuarioServicio.getToken(usuario);
	}

	public RespuestaApi getListado(long idMedico) throws PersistenciaException {
		List<Paciente> pacientes = pacienteDAO.getListado(idMedico);
		if (pacientes == null) {
			pacientes = new ArrayList<>();
		}
		ListadoPacientesResponse respuesta = new ListadoPacientesResponse(pacientes);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getListadoDelAllegado(long idAllegado) throws PersistenciaException {
		List<Paciente> pacientes = pacienteDAO.getListadoDelAllegado(idAllegado);
		if (pacientes == null) {
			pacientes = new ArrayList<>();
		}
		ListadoPacientesResponse respuesta = new ListadoPacientesResponse(pacientes);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getUltimaMedicion(long idPaciente) throws PersistenciaException {
		Medicion medicion = medicionServicio.getUltimaMedicion(idPaciente);
		UltimaMedicionResponse respuesta = new UltimaMedicionResponse(medicion);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getUltimasMediciones(long idPaciente) throws PersistenciaException {
		List<Medicion> mediciones = medicionServicio.getUltimasMediciones(idPaciente);
		return new RespuestaApi(new UltimasMedicionesResponse(mediciones), Constantes.OK);
	}

	public RespuestaApi borrarMedico(long idPaciente, long idMedico) throws PersistenciaException, APIException {
		Paciente paciente = pacienteDAO.get(idPaciente);
		Medico medico = medicoDAO.get(idMedico);
		if (paciente != null && medico != null && paciente.getMedicos() != null) {
			boolean quito = paciente.getMedicos().remove(medico);
			if (quito) {
				this.grabar(paciente);
				SolicitudVinculacion solicitud = solicitudDAO.get(idPaciente, idMedico, TipoSolicitud.MEDICO_PACIENTE);
				solicitudDAO.borrar(solicitud);
				return new RespuestaApi(new BooleanResponse(quito), Constantes.OK);
			}
		}
		throw new APIException("No se pudo borrar al médico, intente nuevamente más tarde");
	}

	public RespuestaApi borrarAllegado(long idPaciente, long idAllegado) throws PersistenciaException, APIException {
		Paciente paciente = pacienteDAO.get(idPaciente);
		Allegado allegado = allegadoDAO.get(idAllegado);
		if (paciente != null && allegado != null && paciente.getAllegados() != null) {
			boolean quito = paciente.getAllegados().remove(allegado);
			if (quito) {
				this.grabar(paciente);
				SolicitudVinculacion solicitud = solicitudDAO.get(idPaciente, idAllegado, TipoSolicitud.ALLEGADO_PACIENTE);
				solicitudDAO.borrar(solicitud);
				return new RespuestaApi(new BooleanResponse(quito), Constantes.OK);
			}
		}
		throw new APIException("No se pudo borrar al médico, intente nuevamente más tarde");
	}

	public RespuestaApi registrar(long idMedico, RegistroPacienteExternoRequest data) throws APIException, ParametrosNoValidosException {

		Medico medico;
		try {
			medico = medicoDAO.get(idMedico);
		} catch (PersistenciaException e1) {
			throw new APIException("Error al grabar el paciente");
		}

		if (medico == null) {
			throw new APIException("Error al grabar el paciente, el médico es inválido");
		}
		Paciente paciente = new Paciente(data);
		paciente.agregarMedico(medico);
		try {
			this.grabar(paciente);
		} catch (PersistenciaException e) {
			throw new APIException("Error al grabar el paciente");
		}
		return new RespuestaApi(new PacienteExternoResponse(paciente.getId()), Constantes.OK);
	}

	public RespuestaApi borrar(long idPaciente) throws APIException {
		Paciente paciente;
		try {
			paciente = pacienteDAO.get(idPaciente);
			this.borrar(paciente);
		} catch (PersistenciaException e) {
			throw new APIException("Error al borrar el paciente en la base de datos intente nuevamente más tarde");
		}

		return new RespuestaApi(new BooleanResponse(true), Constantes.OK);
	}

	public RespuestaApi getHemoglobinaGlicosilada(long idPaciente) throws PersistenciaException {
		double hemoglobinaGlicosilada = medicionServicio.getHemoglobinaGlicosilada(idPaciente);
		HemoglobinaGlicosildaResponse respuesta = new HemoglobinaGlicosildaResponse(UtilesNumeros.redondear(hemoglobinaGlicosilada));
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getHemoglobinaGlicosiladaAnual(long idPaciente) throws PersistenciaException {
		HemoglobinaGlicosiladaAnualWrapper hemoglobinaGlicosiladaAnual = medicionServicio.getHemoglobinaGlicosiladaAnual(idPaciente);
		HemoglobinaGlicosildaUltimoAnioResponse respuesta = new HemoglobinaGlicosildaUltimoAnioResponse(hemoglobinaGlicosiladaAnual);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi setFactorCorreccionInsulina(long idPaciente, FactorCorreccionInsulinaRequest data) throws APIException, PacienteException, ParametrosNoValidosException {
		Paciente paciente;
		try {
			paciente = pacienteDAO.get(idPaciente);
		} catch (PersistenciaException e) {
			throw new PacienteException("Error al recuperar el paciente de la base de datos");
		}

		FactorCorreccionInsulina factorCorreccionInsulina = new FactorCorreccionInsulina(data, paciente);

		paciente.setFactorCorreccionInsulina(factorCorreccionInsulina);
		try {
			this.grabar(paciente);
		} catch (PersistenciaException e) {
			throw new APIException("Error al grabar el paciente");
		}

		IdResponse idResponse = new IdResponse(factorCorreccionInsulina.getId());
		return new RespuestaApi(idResponse, Constantes.OK);
	}

	public RespuestaApi getInfoTarjeta(long idPaciente) throws PersistenciaException {
		Paciente paciente = pacienteDAO.get(idPaciente);
		if (paciente == null) {
			paciente = new Paciente();
		}
		InfoTarjetaResponse respuesta = new InfoTarjetaResponse(paciente);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getFactorCorreccionInsulina(long idPaciente) throws PersistenciaException {
		FactorCorreccionInsulina factor = pacienteDAO.getFactorInsulina(idPaciente);
		FactorCorreccionInsulinaResponse respuesta;
		if (factor != null) {
			respuesta = new FactorCorreccionInsulinaResponse(factor);
		} else {
			respuesta = new FactorCorreccionInsulinaResponse();
		}
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getLogMediciones(long idMedico) throws PersistenciaException {
		List<LogMedicionWrapper> logMedicion = pacienteDAO.getLogMediciones(idMedico);
		LogMedicionesResponse respuesta = new LogMedicionesResponse(logMedicion);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi get(long idPaciente) throws PersistenciaException {
		return getInfoTarjeta(idPaciente);
	}

}
