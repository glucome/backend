package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.response.ListadoPagoResponse;
import com.glucome.dao.PagoDAO;
import com.glucome.dao.UsuarioDAO;
import com.glucome.enumerador.EstadoPago;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Pago;
import com.glucome.modelo.Usuario;
import com.glucome.utils.Constantes;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class PagoServicio extends BaseServicio {

	@Autowired
	UsuarioDAO usuarioDAO;

	@Autowired
	PagoDAO pagoDAO;

	public void grabar(long idUsuario, JsonObject payment) throws PersistenciaException {
		Usuario usuario;
		try {
			usuario = usuarioDAO.get(idUsuario);
		} catch (PersistenciaException e) {
			throw new PersistenciaException("Error al recuperar el usuario de la base de datos");
		}

		LocalDateTime fecha = LocalDateTime.parse(payment.get("date_last_updated").getAsString(), DateTimeFormatter.ISO_DATE_TIME);
		EstadoPago estado = EstadoPago.getByEstadoMercadoPago(payment.get("status").getAsString());

		Pago pago = new Pago(usuario, fecha, estado, false);

		try {
			this.grabar(pago);
		} catch (PersistenciaException e) {
			e.printStackTrace();
			throw new PersistenciaException("Error al grabar el pago en la base de datos");
		}
	}

	public RespuestaApi getPagosSinNotificar(long idUsuario) throws PersistenciaException {
		List<Pago> pagos = pagoDAO.getPagosSinNotificar(idUsuario);
		ListadoPagoResponse respuesta = new ListadoPagoResponse(pagos);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi marcarPagosComoNotificados(List<Long> idsPagos) throws PersistenciaException {
		List<Pago> pagos = pagoDAO.getPagos(idsPagos);
		pagoDAO.marcarPagosComoNotificados(pagos);
		ListadoPagoResponse respuesta = new ListadoPagoResponse(pagos);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}
}
