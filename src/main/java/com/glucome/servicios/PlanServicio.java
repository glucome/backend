package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.SuscripcionPlanRequest;
import com.glucome.api.response.BooleanResponse;
import com.glucome.api.response.ListadoPlanResponse;
import com.glucome.dao.MedicoDAO;
import com.glucome.dao.PacienteDAO;
import com.glucome.dao.UsuarioDAO;
import com.glucome.enumerador.Plan;
import com.glucome.enumerador.Rol;
import com.glucome.enumerador.TipoUsuario;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Medico;
import com.glucome.modelo.Paciente;
import com.glucome.modelo.Usuario;
import com.glucome.utils.Constantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlanServicio extends BaseServicio {

	@Autowired
	MedicoDAO medicoDAO;

	@Autowired
	PacienteDAO pacienteDAO;

	@Autowired
	UsuarioDAO usuarioDAO;

	public RespuestaApi getPlanes(TipoUsuario tipoUsuario) {
		List<Plan> plan = Plan.get(tipoUsuario);
		ListadoPlanResponse respuesta = new ListadoPlanResponse(plan);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi setSuscripcionMedico(SuscripcionPlanRequest data, long idMedico) throws PersistenciaException, APIException {
		Medico medico = medicoDAO.get(idMedico);
		if (medico == null) {
			throw new APIException("El médico es inválido");
		}
		Usuario usuario = medico.getUsuario();
		Plan plan = Plan.get(data.getId());
		if (plan == null) {
			throw new APIException("El plan es inválido");
		}
		suscribir(usuario, plan);
		return new RespuestaApi(new BooleanResponse(true), Constantes.OK);
	}

	public RespuestaApi setSuscripcionMedico(Plan plan, long idUsuario) throws PersistenciaException, APIException {
		Medico medico = usuarioDAO.get(idUsuario).getMedico();
		if (medico == null) {
			throw new APIException("El médico es inválido");
		}

		suscribir(medico.getUsuario(), plan);
		return new RespuestaApi(new BooleanResponse(true), Constantes.OK);
	}

	private void suscribir(Usuario usuario, Plan plan) throws PersistenciaException, APIException {
		Plan actual = usuario.getPlan();

		if (actual.getRol().getTipoUsuario().ordinal() == plan.getRol().getTipoUsuario().ordinal()) {
			List<Rol> roles = new ArrayList<>();
			roles.add(plan.getRol());
			usuario.setRoles(roles);
			usuario.setPlan(plan);
			this.grabar(usuario);
		} else {
			throw new APIException("El plan al que te deseas suscribir no es válido. Selecciona un plan acorde al tipo de usuario.");
		}
	}

	public RespuestaApi setSuscripcionPaciente(SuscripcionPlanRequest data, long idPaciente) throws PersistenciaException, APIException {
		Paciente paciente = pacienteDAO.get(idPaciente);
		if (paciente == null) {
			throw new APIException("El paciente es inválido");
		}
		Usuario usuario = paciente.getUsuario();
		Plan plan = Plan.get(data.getId());
		if (plan == null) {
			throw new APIException("El plan es inválido");
		}
		suscribir(usuario, plan);
		return new RespuestaApi(new BooleanResponse(true), Constantes.OK);
	}

	public RespuestaApi setSuscripcionPaciente(Plan plan, long idUsuario) throws PersistenciaException, APIException {
		Paciente paciente = usuarioDAO.get(idUsuario).getPaciente();
		if (paciente == null) {
			throw new APIException("El paciente es inválido");
		}

		suscribir(paciente.getUsuario(), plan);
		return new RespuestaApi(new BooleanResponse(true), Constantes.OK);
	}
}
