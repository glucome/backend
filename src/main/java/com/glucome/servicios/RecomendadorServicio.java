package com.glucome.servicios;

import java.util.Collections;
import java.util.List;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glucome.api.RespuestaApi;
import com.glucome.api.response.RecomendadorResponse;
import com.glucome.enumerador.MomentoAlimento;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Alimento;
import com.glucome.modelo.Medicion;
import com.glucome.utils.Constantes;

@Service
public class RecomendadorServicio extends BaseServicio {

	@Autowired
	private KieContainer kieContainer;

	@Autowired
	private MedicionServicio medicionServicio;

	@Autowired
	private AlimentoServicio alimentoServicio;

	public RespuestaApi getRecomendacion(long idPaciente) throws PersistenciaException, APIException {

		KieSession kieSession = kieContainer.newKieSession();

		RecomendadorResponse response = new RecomendadorResponse();
		Medicion medicion = medicionServicio.getUltimaMedicion(idPaciente);

		// if (medicion == null ||
		// medicion.getFechaHora().isBefore(LocalDateTime.now().minusHours(1))) {
		// throw new APIException("Para poder recomendarle alimentos, debe tener una
		// medición en la ultima hora");
		// }
		List<MomentoAlimento> momentosActuales = MomentoAlimento.getMomentosActuales();

		kieSession.insert(medicion);
		kieSession.insert(momentosActuales);
		kieSession.insert(alimentoServicio);
		kieSession.insert(response);
		kieSession.fireAllRules();

		List<Alimento> alimentos = response.getAlimentosRecomendados();
		if (alimentos != null && !alimentos.isEmpty()) {
			Collections.shuffle(alimentos);
			if (alimentos.size() > Constantes.CANTIDAD_COMIDAS_RECOMENDADAS) {
				alimentos = alimentos.subList(0, Constantes.CANTIDAD_COMIDAS_RECOMENDADAS);
			}
		}
		response.setAlimentosRecomendados(alimentos);

		return new RespuestaApi(response, Constantes.OK);

	}

}
