package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.response.*;
import com.glucome.dao.ReporteMedicoDAO;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.utils.Constantes;
import com.glucome.wrapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReporteMedicoServicio extends BaseServicio {


	@Autowired
	ReporteMedicoDAO reporteMedicoDAO;

	public RespuestaApi getReportePorMomento(long idMedico) throws PersistenciaException {
		List<ReporteMedicoPorMomentoWrapper> reporte = reporteMedicoDAO.getReportePorMomento(idMedico);
		ReporteMedicoPorMomentoResponse respuesta = new ReporteMedicoPorMomentoResponse(reporte);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}


	public RespuestaApi getReportePorRangoEdad(long idMedico) throws PersistenciaException {
		List<ReporteMedicoPorRangoEdadWrapper> reporte = reporteMedicoDAO.getReportePorRangoEdad(idMedico);
		ReporteMedicoPorRangoEdadResponse respuesta = new ReporteMedicoPorRangoEdadResponse(reporte);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}


	public RespuestaApi getReportePorTipoDiabetes(long idMedico) throws PersistenciaException {
		List<ReporteMedicoPorTipoDiabetesWrapper> reporte = reporteMedicoDAO.getReportePorTipoDiabetes(idMedico);
		ReporteMedicoPorTipoDiabetesResponse respuesta = new ReporteMedicoPorTipoDiabetesResponse(reporte);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getReportePorTipoMedicion(long idMedico) throws PersistenciaException {
		List<ReporteMedicoPorTipoMedicionWrapper> reporte = reporteMedicoDAO.getReportePorTipoMedicion(idMedico);
		ReporteMedicoPorTipoMedicionResponse respuesta = new ReporteMedicoPorTipoMedicionResponse(reporte);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getReporteRankingAlimentos(long idMedico) throws PersistenciaException {
		List<ReporteMedicoRankingAlimentosWrapper> reporte = reporteMedicoDAO.getReporteRankingAlimentos(idMedico);
		ReporteMedicoRankingAlimentosResponse respuesta = new ReporteMedicoRankingAlimentosResponse(reporte);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}
}
