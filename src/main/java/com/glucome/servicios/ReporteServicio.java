package com.glucome.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glucome.api.RespuestaApi;
import com.glucome.api.response.ReporteCantidadMedicionesResponse;
import com.glucome.api.response.ReportePorMomentoResponse;
import com.glucome.api.response.ReportePorRangoEdadResponse;
import com.glucome.api.response.ReportePorTipoDiabetesResponse;
import com.glucome.api.response.ReportePorTipoMedicionResponse;
import com.glucome.api.response.ReporteRankingAlimentosResponse;
import com.glucome.dao.ReporteDAO;
import com.glucome.utils.Constantes;
import com.glucome.wrapper.ReporteCantidadMedicionesWrapper;
import com.glucome.wrapper.ReportePorMomentoWrapper;
import com.glucome.wrapper.ReportePorRangoEdadWrapper;
import com.glucome.wrapper.ReportePorTipoDiabetesWrapper;
import com.glucome.wrapper.ReportePorTipoMedicionWrapper;
import com.glucome.wrapper.ReporteRankingAlimentosWrapper;

@Service
public class ReporteServicio extends BaseServicio {

	@Autowired
	private ReporteDAO reporteDAO;

	public RespuestaApi getReporteCantidadMediciones() {
		List<ReporteCantidadMedicionesWrapper> reporte = reporteDAO.getReporteCantidadMediciones();
		return new RespuestaApi(new ReporteCantidadMedicionesResponse(reporte), Constantes.OK);
	}

	public RespuestaApi getReportePorMomento() {
		List<ReportePorMomentoWrapper> reporte = reporteDAO.getReportePorMomento();
		return new RespuestaApi(new ReportePorMomentoResponse(reporte), Constantes.OK);
	}

	public RespuestaApi getReportePorRangoEdad() {
		List<ReportePorRangoEdadWrapper> reporte = reporteDAO.getReportePorRangoEdad();
		return new RespuestaApi(new ReportePorRangoEdadResponse(reporte), Constantes.OK);
	}

	public RespuestaApi getReportePorTipoDiabetes() {
		List<ReportePorTipoDiabetesWrapper> reporte = reporteDAO.getReportePorTipoDiabetes();
		return new RespuestaApi(new ReportePorTipoDiabetesResponse(reporte), Constantes.OK);
	}

	public RespuestaApi getReportePorTipoMedicion() {
		List<ReportePorTipoMedicionWrapper> reporte = reporteDAO.getReportePorTipoMedicion();
		return new RespuestaApi(new ReportePorTipoMedicionResponse(reporte), Constantes.OK);
	}

	public RespuestaApi getReporteRankingAlimentos() {
		List<ReporteRankingAlimentosWrapper> reporte = reporteDAO.getReporteRankingAlimentos();
		return new RespuestaApi(new ReporteRankingAlimentosResponse(reporte), Constantes.OK);
	}
}
