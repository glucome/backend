package com.glucome.servicios;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.SolicitudRequest;
import com.glucome.api.response.ListadoSolicitudResponse;
import com.glucome.api.response.SolicitudResponse;
import com.glucome.dao.AllegadoDAO;
import com.glucome.dao.MedicoDAO;
import com.glucome.dao.PacienteDAO;
import com.glucome.dao.SolicitudDAO;
import com.glucome.enumerador.EstadoSolicitud;
import com.glucome.enumerador.TipoSolicitud;
import com.glucome.excepciones.APIException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.interfaces.Logueable;
import com.glucome.modelo.*;
import com.glucome.utils.Constantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SolicitudServicio extends BaseServicio {

	@Autowired
	PacienteDAO pacienteDAO;

	@Autowired
	MedicoDAO medicoDAO;

	@Autowired
	AllegadoDAO allegadoDAO;

	@Autowired
	SolicitudDAO solicitudDAO;

	public RespuestaApi agregarSolicitud(SolicitudRequest data) throws PersistenciaException {
		Paciente paciente = pacienteDAO.get(data.getIdSolicitante());
		Medico medico = medicoDAO.get(data.getIdSolicitado());
		SolicitudVinculacionMedicoPaciente solicitud = new SolicitudVinculacionMedicoPaciente(paciente, medico);
		this.grabar(solicitud);
		SolicitudResponse respuesta = new SolicitudResponse(solicitud);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi aceptarSolicitud(long id) throws PersistenciaException, APIException {
		SolicitudVinculacion solicitud = solicitudDAO.get(id);
		if (solicitud != null && solicitud.getEstado() == EstadoSolicitud.PENDIENTE) {
			Logueable logueable = solicitud.aceptarSolicitud();
			this.grabar(solicitud);
			this.grabar(logueable);
			SolicitudResponse respuesta = new SolicitudResponse(solicitud);
			RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
			return respuestaApi;
		}
		throw new APIException("La solicitud es inválida");
	}

	public RespuestaApi rechazarSolicitud(long id) throws PersistenciaException, APIException {
		SolicitudVinculacion solicitud = solicitudDAO.get(id);
		if (solicitud != null && solicitud.getEstado() == EstadoSolicitud.PENDIENTE) {
			solicitud.setEstado(EstadoSolicitud.RECHAZADA);
			this.borrar(solicitud);
			SolicitudResponse respuesta = new SolicitudResponse(solicitud);
			RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
			return respuestaApi;
		}
		throw new APIException("La solicitud es inválida");
	}

	public RespuestaApi getSolicitud(long idSolicitante, long idSolicitado, int tipoSolicitud) throws PersistenciaException {
		TipoSolicitud tipo = TipoSolicitud.get(tipoSolicitud);
		SolicitudVinculacion solicitud = solicitudDAO.get(idSolicitante, idSolicitado, tipo);
		SolicitudResponse respuesta = new SolicitudResponse(solicitud);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi getListado(long id, TipoSolicitud tipo) throws PersistenciaException {
		List<SolicitudVinculacion> solicitudes;
		if (tipo.equals(TipoSolicitud.MEDICO_PACIENTE)) {
			solicitudes = solicitudDAO.getListadoDeSolicitudesMedico(id);
		} else {
			solicitudes = solicitudDAO.getListadoDeSolicitudesAllegado(id);
		}

		ListadoSolicitudResponse respuesta = new ListadoSolicitudResponse(solicitudes);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi agregarSolicitudAllegado(SolicitudRequest data) throws PersistenciaException {
		Paciente paciente = pacienteDAO.get(data.getIdSolicitante());
		Allegado allegado = allegadoDAO.get(data.getIdSolicitado());
		SolicitudVinculacionAllegadoPaciente solicitud = new SolicitudVinculacionAllegadoPaciente(paciente, allegado);
		this.grabar(solicitud);
		SolicitudResponse respuesta = new SolicitudResponse(solicitud);
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

}
