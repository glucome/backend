package com.glucome.servicios;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.glucome.api.RespuestaApi;
import com.glucome.api.request.LoginRequest;
import com.glucome.api.response.BooleanResponse;
import com.glucome.api.response.LoginResponse;
import com.glucome.api.response.RolesResponse;
import com.glucome.dao.UserRepository;
import com.glucome.dao.UsuarioDAO;
import com.glucome.excepciones.PacienteException;
import com.glucome.excepciones.ParametrosNoValidosException;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.modelo.Usuario;
import com.glucome.security.JwtTokenProvider;
import com.glucome.utils.Constantes;
import com.glucome.utils.UtilesString;

@Service
public class UsuarioServicio extends BaseServicio {
	@Autowired
	UserRepository users;
	
	@Autowired
	UsuarioDAO usuarioDao;

	@Autowired
	JwtTokenProvider jwtTokenProvider;

	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	AlmacenamientoDeArchivosServicios archivosServicio;

	@Value("${file.upload-dir:'C:/Imagenes/'}")
	private String rutaImagenes;

	public RespuestaApi getToken(Usuario usuario) {
		String token = jwtTokenProvider.createToken(usuario.getNombreUsuario(), usuario.getRoles());
		LoginResponse respuesta = new LoginResponse();
		respuesta.setToken(token);
		respuesta.setId(usuario.getId());
		respuesta.setUsuario(usuario.getNombreUsuario());
		respuesta.setAsociado(usuario.getLogueable());
		respuesta.setRoles(usuario.getRoles());
		RespuestaApi respuestaApi = new RespuestaApi(respuesta, Constantes.OK);
		return respuestaApi;
	}

	public RespuestaApi logIn(LoginRequest data) throws BadCredentialsException {
		try {
			Usuario usuario = getUsuario(data);
			return this.getToken(usuario);
		} catch (AuthenticationException e) {
			throw new BadCredentialsException("Usuario o clave inválida");
		}
	}
	
	public Usuario getUsuarioSinAutenticar(LoginRequest data) throws BadCredentialsException {
		String username = data.getUsuario();
		Usuario usuario = this.users.findByMail(username).orElseThrow(() -> new BadCredentialsException("Usuario no existente"));
		return usuario;
	}

	public Usuario getUsuario(LoginRequest data) throws UsernameNotFoundException {
		String username = data.getUsuario();
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, data.getPassword()));
		Usuario usuario = this.users.findByNombreUsuarioOrMail(username, username).orElseThrow(() -> new UsernameNotFoundException("Username " + username + "not found"));
		return usuario;
	}

	public RespuestaApi existeMail(String mail) throws BadCredentialsException {
		boolean existe = this.users.findByMail(mail).isPresent();

		return new RespuestaApi(new BooleanResponse(existe), Constantes.OK);
	}

	public RespuestaApi existeNombreUsuario(String nombreUsuario) throws BadCredentialsException {
		boolean existe = this.users.findByNombreUsuario(nombreUsuario).isPresent();

		return new RespuestaApi(new BooleanResponse(existe), Constantes.OK);
	}
	
	public RespuestaApi cambiarPass(LoginRequest data) throws ParametrosNoValidosException, PersistenciaException {

		Usuario usuarioExistente = getUsuarioSinAutenticar(data);
		usuarioExistente.setClave(passwordEncoder.encode(data.getPassword()));
		usuarioDao.grabar(usuarioExistente);
		return new RespuestaApi(new BooleanResponse(true), Constantes.OK);
	}

	public RespuestaApi logInMedico(LoginRequest data) throws BadCredentialsException {
		try {
			Usuario usuario = getUsuario(data);
			if (!usuario.esMedico()) {
				throw new BadCredentialsException("Usuario inválido");
			}
			return getToken(usuario);
		} catch (AuthenticationException e) {
			throw new BadCredentialsException("Usuario o clave inválida");
		}
	}

	public Usuario get(long idUsuario) throws PersistenciaException {
		return users.findById(idUsuario).orElseThrow(() -> new PersistenciaException("Usuario: " + idUsuario + " no existente"));
	}

	public RespuestaApi setImagen(long idUsuario, MultipartFile file) throws PersistenciaException, IOException {
		String nombreArchivo = archivosServicio.guardarArchivo(file);
		Usuario usuario = get(idUsuario);
		archivosServicio.borrarArchivo(usuario.getImagen());
		usuario.setImagen(nombreArchivo);
		this.grabar(usuario);
		return new RespuestaApi(new BooleanResponse(true), Constantes.OK);
	}

	public RespuestaApi getRoles(long idUsuario) throws PersistenciaException {
		List<String> roles = this.get(idUsuario).getRoles();
		return new RespuestaApi(new RolesResponse(roles), Constantes.OK);
	}

	public File getImagenUsuario(long idUsuario) {
		Usuario usuario;
		File file;
		try {
			usuario = this.get(idUsuario);
			if (UtilesString.isNullOrEmpty(usuario.getImagen())) {
				throw new PersistenciaException("Imagen vacía");
			}
			file = new File(rutaImagenes + usuario.getImagen());
		} catch (PersistenciaException e) {
			file = new File(rutaImagenes + "defecto.png");
		}
		return file;
	}
}