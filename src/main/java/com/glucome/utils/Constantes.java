package com.glucome.utils;

public class Constantes {

	public static final String OK = "ok";
	public static final int CANTIDAD_MINIMA_CARACTERES_BUSQUEDA = 2;
	public static final int PAGINA_INICIAL = 0;
	public static final int REGISTROS_POR_PAGINA_POR_DEFECTO = 50;
	public static final String URL_BASE = "/api/v1/";
	public static final long MESES_HEMOGLOBINA_GLICOSILADA = 3;
	public static final double PRIMER_TERMINO_HEMOGLOBINA_GLICOSILADA = 46.7;
	public static final double SEGUNDO_TERMINO_HEMOGLOBINA_GLICOSILADA = 28.7;
	public static final int CANTIDAD_DECIMALES = 2;
	public static final String FORMATO_REDONDEADO = "#.##";
	public static final int CANTIDAD_COMIDAS_RECOMENDADAS = 10;
	public static final int CANTIDAD_MEDICIONES_LOG = 5;
	public static final int VALOR_MINIMO_MEDICION = 70;
	public static final int VALOR_MAXIMO_MEDICION = 140;
	public static final String RUTA_RELATIVA_IMAGENES_ALIMENTOS = "alimentos/";
	public static final String TEMPLATE_HISTORIA_CLINICA = "historia_clinica";
	public static final String TEMPLATE_TARJETA_AUMENTADA = "tarjeta_aumentada";
}
