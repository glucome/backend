package com.glucome.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * @author Pablo
 */
public class UtilesFecha {
    public static final String DD_MM_YYYY_HH_MM = "dd/MM/yyyy HH:mm";
    public static final String DD_M_YYYY_HH_MM = "dd/M/yyyy HH:mm";
    public static final String DD_MM_YYYY = "dd/MM/yyyy";
    private static final DateTimeFormatter FORMATTER_FECHA = DateTimeFormatter.ofPattern(DD_MM_YYYY);
    private static final DateTimeFormatter FORMATTER_FECHA_HORA = DateTimeFormatter.ofPattern(DD_MM_YYYY_HH_MM);
    private static final DateTimeFormatter FORMATTER_FECHA_HORA_UN_CARACTER_MES = DateTimeFormatter.ofPattern(DD_M_YYYY_HH_MM);
    
    public static LocalDateTime millsToLocalDateTime(long millis) {
		Instant instant = Instant.ofEpochMilli(millis);
		return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

    public static LocalDate getLocalDateFromString(String fecha) throws DateTimeParseException {
        return LocalDate.parse(fecha, FORMATTER_FECHA);
    }

    public static String getStringFromLocalDate(LocalDate fecha) throws DateTimeParseException {
        if (fecha == null)
            return null;
        String fechaFormateada = fecha.format(FORMATTER_FECHA);
        return fechaFormateada;
    }

    public static LocalDateTime getLocalDateTimeFromString(String fechaHora) throws DateTimeParseException {
        try {
            return LocalDateTime.parse(fechaHora, FORMATTER_FECHA_HORA);
        } catch (DateTimeParseException e) {
            return LocalDateTime.parse(fechaHora, FORMATTER_FECHA_HORA_UN_CARACTER_MES);
        }
    }

    public static String getString(LocalDateTime fechaHora) {
        if (fechaHora == null)
            return null;
        String fechaFormateada = fechaHora.format(FORMATTER_FECHA_HORA);
        return fechaFormateada;
    }
}
