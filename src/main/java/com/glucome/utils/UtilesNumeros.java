package com.glucome.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class UtilesNumeros {

	public static double redondear(double numero) {
		BigDecimal bd = new BigDecimal(numero);
		bd = bd.setScale(Constantes.CANTIDAD_DECIMALES, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

}
