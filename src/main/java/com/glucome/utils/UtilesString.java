package com.glucome.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilesString {

	public static final Pattern EXPRESION_MAIL_VALIDO = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	public static boolean esMailValido(String emailStr) {
		if (isNullOrEmpty(emailStr)) {
			return false;
		}
		Matcher matcher = EXPRESION_MAIL_VALIDO.matcher(emailStr);
		return matcher.find();
	}

	public static boolean isNullOrEmpty(String cadena) {
		return cadena == null || cadena.equals("");
	}

	public static boolean esNumeroEntero(String cadena) {
		if (UtilesString.isNullOrEmpty(cadena)) {
			return false;
		}
		try {
			Long.parseLong(cadena);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
