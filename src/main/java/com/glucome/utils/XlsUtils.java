package com.glucome.utils;

import com.glucome.interfaces.XlsCabeceable;
import com.glucome.interfaces.XlsExportable;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Utils para creación de archivos xls
 *
 * @author Pablo
 */
public abstract class XlsUtils {

    public static final String FONDO_AZUL = "cell_blue";
    public static final String IDENTADA = "cell_indented";
    public static final String NORMAL_FECHA = "cell_normal_date";
    public static final String NORMAL_CENTRADA = "cell_normal_centered";
    public static final String NORMAL = "cell_normal";
    public static final String LETRA_AZUL_NEGRITA = "cell_h";
    public static final String FECHA_FONDO_GRIS = "cell_bg";
    public static final String NEGRITA_AZUL = "cell_bb";
    public static final String FONDO_GRIS = "cell_g";
    public static final String FECHA = "cell_b_date";
    public static final String CENTRADA = "cell_b_centered";
    public static final String CENTRADA_NEGRITA_SIN_BORDE = "cell_b_centered_without_border";
    public static final String NEGRITA = "cell_b";
    public static final String FECHA_CABECERA = "header_date";
    public static final String CABECERA_DATOS = "header";
    public static final String CABECERA = "header";

    /**
     * Crea un archivo xls de una sola hoja
     *
     * @param nombreHoja
     * @param cabecereable
     * @param cabecera
     * @param listado
     * @return
     */
    public static HSSFWorkbook crearXls(String nombreHoja, XlsCabeceable cabecereable, String[] cabecera, List<? extends XlsExportable> listado) {
        HSSFWorkbook libro = new HSSFWorkbook();
        Sheet hoja = libro.createSheet(nombreHoja);

        Map<String, CellStyle> estilos = crearEstilos(libro);

        hoja.setDisplayGridlines(false);
        hoja.setPrintGridlines(false);
        hoja.setFitToPage(true);
        hoja.setHorizontallyCenter(true);
        PrintSetup printSetup = hoja.getPrintSetup();
        printSetup.setLandscape(true);

        hoja.setAutobreaks(true);
        printSetup.setFitHeight((short) 1);
        printSetup.setFitWidth((short) 1);

        int filaActual = 0;

        if (cabecereable != null) {
            filaActual = cabecereable.llenarCabecera(hoja, estilos);
        }
        Row filaCabecera = hoja.createRow(filaActual++);
        filaCabecera.setHeightInPoints(12.75f);

        for (int i = 0; i < cabecera.length; i++) {
            Cell celdaCabecera = filaCabecera.createCell(i);
            celdaCabecera.setCellValue(cabecera[i]);
            celdaCabecera.setCellStyle(estilos.get(CABECERA_DATOS));
        }

        hoja.createFreezePane(0, filaActual);

        Row filaDatos = null;
        for (int i = filaActual; i < listado.size() + filaActual; i++) {
            XlsExportable dato = listado.get(i - filaActual);
            filaDatos = hoja.createRow(i);
            dato.llenarRow(filaDatos, estilos);
        }

        for (int i = 0; i < cabecera.length; i++) {
            hoja.autoSizeColumn(i);
        }

        return libro;

    }

    public static Map<String, CellStyle> crearEstilos(HSSFWorkbook wb) {
        Map<String, CellStyle> styles = new HashMap<>();
        DataFormat df = wb.createDataFormat();

        CellStyle style;
        Font headerFont = wb.createFont();
        headerFont.setBold(true);
        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillForegroundColor(IndexedColors.INDIGO.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFont(headerFont);
        styles.put(CABECERA_DATOS, style);

        headerFont = wb.createFont();
        headerFont.setBold(true);
        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFont(headerFont);
        styles.put(CABECERA, style);

        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFont(headerFont);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put(FECHA_CABECERA, style);

        Font font1 = wb.createFont();
        font1.setBold(true);
        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setFont(font1);
        styles.put(NEGRITA, style);

        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFont(font1);
        styles.put(CENTRADA, style);

        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.RIGHT);
        style.setFont(font1);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put(FECHA, style);

        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.RIGHT);
        style.setFont(font1);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put(FONDO_GRIS, style);

        Font font2 = wb.createFont();
        font2.setColor(IndexedColors.BLUE.getIndex());
        font2.setBold(true);
        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setFont(font2);
        styles.put(NEGRITA_AZUL, style);

        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.RIGHT);
        style.setFont(font1);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put(FECHA_FONDO_GRIS, style);

        Font font3 = wb.createFont();
        font3.setFontHeightInPoints((short) 14);
        font3.setColor(IndexedColors.DARK_BLUE.getIndex());
        font3.setBold(true);
        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setFont(font3);
        style.setWrapText(true);
        styles.put(LETRA_AZUL_NEGRITA, style);

        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setWrapText(true);
        styles.put(NORMAL, style);

        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setWrapText(true);
        styles.put(NORMAL_CENTRADA, style);

        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.RIGHT);
        style.setWrapText(true);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put(NORMAL_FECHA, style);

        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setIndention((short) 1);
        style.setWrapText(true);
        styles.put(IDENTADA, style);

        style = createBorderedStyle(wb);
        style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styles.put(FONDO_AZUL, style);

        style = wb.createCellStyle();
        font1 = wb.createFont();
        font1.setBold(true);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFont(font1);
        styles.put(CENTRADA_NEGRITA_SIN_BORDE, style);

        return styles;
    }

    private static CellStyle createBorderedStyle(Workbook wb) {
        BorderStyle thin = BorderStyle.THIN;
        short black = IndexedColors.BLACK.getIndex();

        CellStyle style = wb.createCellStyle();
        style.setBorderRight(thin);
        style.setRightBorderColor(black);
        style.setBorderBottom(thin);
        style.setBottomBorderColor(black);
        style.setBorderLeft(thin);
        style.setLeftBorderColor(black);
        style.setBorderTop(thin);
        style.setTopBorderColor(black);
        return style;
    }

    public static void descargarXls(HttpServletResponse response, String nombre, XlsCabeceable cabecereable, String[] cabecera, List<? extends XlsExportable> listado) throws IOException {

        HSSFWorkbook hojaXLS = XlsUtils.crearXls(nombre, cabecereable, cabecera, listado);
        response.setHeader("Content-disposition", "attachment;filename=" + nombre + ".xls");
        response.setHeader("charset", "UTF-8");
        response.setContentType("application/octet-stream");
        response.setStatus(HttpServletResponse.SC_OK);
        OutputStream outputStream = null;
        outputStream = response.getOutputStream();
        hojaXLS.write(outputStream);
        hojaXLS.close();
        outputStream.flush();
        outputStream.close();
        response.flushBuffer();

    }

}
