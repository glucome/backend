package com.glucome.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.glucome.excepciones.PersistenciaException;
import com.glucome.servicios.ChatServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;

@RestController
@RequestMapping("/socket")
@CrossOrigin("*")
public class SocketRest {

    @Autowired
    ChatServicio chatServicio;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @MessageMapping("/send/message")
    public Map<String, String> enviarMensaje(String message) throws PersistenciaException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> messageConverted = null;
        try {
            messageConverted = mapper.readValue(message, Map.class);
        } catch (IOException e) {
            messageConverted = null;
        }
        if (messageConverted != null) {
            if (messageConverted.containsKey("fromId") && messageConverted.containsKey("toId")) {
                Long idAutor = Long.valueOf(String.valueOf(messageConverted.get("fromId")));
                Long idDestinatario = Long.valueOf(String.valueOf(messageConverted.get("toId")));
                Long idChat = Long.valueOf(String.valueOf(messageConverted.get("chatId")));
                String cuerpo = String.valueOf(messageConverted.get("message"));
                LocalDateTime fechaHora = LocalDateTime.parse(String.valueOf(messageConverted.get("dateSent")));

                if(!idDestinatario.equals("")) {
                    this.simpMessagingTemplate.convertAndSend("/socket-publisher/" + idDestinatario, messageConverted);
                } else {
                    this.simpMessagingTemplate.convertAndSend("/socket-publisher", messageConverted);
                }

                chatServicio.guardarMensaje(idAutor, idDestinatario, idChat, cuerpo, fechaHora);
            }
        }
        return messageConverted;
    }
}
