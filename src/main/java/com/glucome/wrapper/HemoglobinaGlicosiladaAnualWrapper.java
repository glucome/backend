package com.glucome.wrapper;

public class HemoglobinaGlicosiladaAnualWrapper {

    private double eneroMarzo;
    private double febreroAbril;
    private double marzoMayo;
    private double abrilJunio;
    private double mayoJulio;
    private double junioAgosto;
    private double julioSeptiembre;
    private double agostoOctubre;
    private double septiembreNoviembre;
    private double octubreDiciembre;
    private int trimestreActual;

    public HemoglobinaGlicosiladaAnualWrapper() {
    }

    public HemoglobinaGlicosiladaAnualWrapper(double eneroMarzo, double febreroAbril, double marzoMayo, double abrilJunio, double mayoJulio, double junioAgosto, double julioSeptiembre, double agostoOctubre, double septiembreNoviembre, double octubreDiciembre) {
        this.eneroMarzo = eneroMarzo;
        this.febreroAbril = febreroAbril;
        this.marzoMayo = marzoMayo;
        this.abrilJunio = abrilJunio;
        this.mayoJulio = mayoJulio;
        this.junioAgosto = junioAgosto;
        this.julioSeptiembre = julioSeptiembre;
        this.agostoOctubre = agostoOctubre;
        this.septiembreNoviembre = septiembreNoviembre;
        this.octubreDiciembre = octubreDiciembre;
    }

    public double getEneroMarzo() {
        return eneroMarzo;
    }

    public void setEneroMarzo(double eneroMarzo) {
        this.eneroMarzo = eneroMarzo;
    }

    public double getFebreroAbril() {
        return febreroAbril;
    }

    public void setFebreroAbril(double febreroAbril) {
        this.febreroAbril = febreroAbril;
    }

    public double getMarzoMayo() {
        return marzoMayo;
    }

    public void setMarzoMayo(double marzoMayo) {
        this.marzoMayo = marzoMayo;
    }

    public double getAbrilJunio() {
        return abrilJunio;
    }

    public void setAbrilJunio(double abrilJunio) {
        this.abrilJunio = abrilJunio;
    }

    public double getMayoJulio() {
        return mayoJulio;
    }

    public void setMayoJulio(double mayoJulio) {
        this.mayoJulio = mayoJulio;
    }

    public double getJunioAgosto() {
        return junioAgosto;
    }

    public void setJunioAgosto(double junioAgosto) {
        this.junioAgosto = junioAgosto;
    }

    public double getJulioSeptiembre() {
        return julioSeptiembre;
    }

    public void setJulioSeptiembre(double julioSeptiembre) {
        this.julioSeptiembre = julioSeptiembre;
    }

    public double getAgostoOctubre() {
        return agostoOctubre;
    }

    public void setAgostoOctubre(double agostoOctubre) {
        this.agostoOctubre = agostoOctubre;
    }

    public double getSeptiembreNoviembre() {
        return septiembreNoviembre;
    }

    public void setSeptiembreNoviembre(double septiembreNoviembre) {
        this.septiembreNoviembre = septiembreNoviembre;
    }

    public double getOctubreDiciembre() {
        return octubreDiciembre;
    }

    public void setOctubreDiciembre(double octubreDiciembre) {
        this.octubreDiciembre = octubreDiciembre;
    }

    public int getTrimestreActual() {
        return trimestreActual;
    }

    public void setTrimestreActual(int trimestreActual) {
        this.trimestreActual = trimestreActual;
    }
}
