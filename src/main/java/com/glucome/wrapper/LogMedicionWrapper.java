package com.glucome.wrapper;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.glucome.enumerador.MomentoMedicion;
import com.glucome.utils.UtilesFecha;

public class LogMedicionWrapper {

	private String nombrePaciente;

	private long idPaciente;

	private long idUsuario;

	private int medicion;

	private MomentoMedicion momentoMedicion;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = UtilesFecha.DD_MM_YYYY_HH_MM)
	private LocalDateTime fechaHora;

	public String getNombrePaciente() {
		return nombrePaciente;
	}

	public void setNombrePaciente(String nombrePaciente) {
		this.nombrePaciente = nombrePaciente;
	}

	public long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(long idPaciente) {
		this.idPaciente = idPaciente;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getMedicion() {
		return medicion;
	}

	public void setMedicion(int medicion) {
		this.medicion = medicion;
	}

	public MomentoMedicion getMomentoMedicion() {
		return momentoMedicion;
	}

	public void setMomentoMedicion(MomentoMedicion momentoMedicion) {
		this.momentoMedicion = momentoMedicion;
	}

	public LocalDateTime getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(LocalDateTime fechaHora) {
		this.fechaHora = fechaHora;
	}

	public LogMedicionWrapper(String nombrePaciente, long idPaciente, long idUsuario, int medicion, MomentoMedicion momentoMedicion, LocalDateTime fechaHora) {
		super();
		this.nombrePaciente = nombrePaciente;
		this.idPaciente = idPaciente;
		this.idUsuario = idUsuario;
		this.medicion = medicion;
		this.momentoMedicion = momentoMedicion;
		this.fechaHora = fechaHora;
	}

}
