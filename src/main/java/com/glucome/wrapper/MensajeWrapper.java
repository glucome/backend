package com.glucome.wrapper;

import java.time.LocalDateTime;

public class MensajeWrapper {

    private String cuerpo;
    private Long autor;
    private Long destinatario;
    private LocalDateTime fechaHora;
    private LocalDateTime fechaHoraVisto;

    public MensajeWrapper(String cuerpo, Long autor, Long destinatario, LocalDateTime fechaHora, LocalDateTime fechaHoraVisto) {
        this.cuerpo = cuerpo;
        this.autor = autor;
        this.destinatario = destinatario;
        this.fechaHora = fechaHora;
        this.fechaHoraVisto = fechaHoraVisto;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public Long getAutor() {
        return autor;
    }

    public void setAutor(Long autor) {
        this.autor = autor;
    }

    public Long getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(Long destinatario) {
        this.destinatario = destinatario;
    }

    public LocalDateTime getFechaHora() { return fechaHora; }

    public void setFechaHora(LocalDateTime fechaHora) { this.fechaHora = fechaHora; }

    public LocalDateTime getFechaHoraVisto() { return fechaHoraVisto; }

    public void setFechaHoraVisto(LocalDateTime fechaHoraVisto) { this.fechaHoraVisto = fechaHoraVisto; }
}
