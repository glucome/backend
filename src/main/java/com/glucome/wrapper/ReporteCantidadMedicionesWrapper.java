package com.glucome.wrapper;

public class ReporteCantidadMedicionesWrapper {

    private int sexo;
    private int valor;
    private long cantidad;

    public ReporteCantidadMedicionesWrapper(int sexo, int valor, long cantidad) {
        this.sexo = sexo;
        this.valor = valor;
        this.cantidad = cantidad;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }
}
