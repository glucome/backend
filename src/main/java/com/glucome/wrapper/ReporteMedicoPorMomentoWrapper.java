package com.glucome.wrapper;

import com.glucome.enumerador.MomentoMedicion;

public class ReporteMedicoPorMomentoWrapper {

    private MomentoMedicion momento;
    private double promedio;

    public ReporteMedicoPorMomentoWrapper(MomentoMedicion momento, double promedio) {
        this.momento = momento;
        this.promedio = promedio;
    }

    public MomentoMedicion getMomento() {
        return momento;
    }

    public void setMomento(MomentoMedicion momento) {
        this.momento = momento;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }
}
