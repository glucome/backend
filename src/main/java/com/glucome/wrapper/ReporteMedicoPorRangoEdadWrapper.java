package com.glucome.wrapper;

import com.glucome.enumerador.MomentoMedicion;

public class ReporteMedicoPorRangoEdadWrapper {

    private MomentoMedicion momento;
    private int rangoEdad;
    private double promedio;

    public ReporteMedicoPorRangoEdadWrapper(MomentoMedicion momento, int rangoEdad, double promedio) {
        this.momento = momento;
        this.rangoEdad = rangoEdad;
        this.promedio = promedio;
    }

    public MomentoMedicion getMomento() {
        return momento;
    }

    public void setMomento(MomentoMedicion momento) {
        this.momento = momento;
    }

    public int getRangoEdad() {
        return rangoEdad;
    }

    public void setRangoEdad(int rangoEdad) {
        this.rangoEdad = rangoEdad;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }
}
