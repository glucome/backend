package com.glucome.wrapper;

import com.glucome.enumerador.MomentoMedicion;
import com.glucome.enumerador.TipoDiabetes;

public class ReporteMedicoPorTipoDiabetesWrapper {

    private TipoDiabetes tipoDiabetes;
    private MomentoMedicion momento;
    private double promedio;

    public ReporteMedicoPorTipoDiabetesWrapper(TipoDiabetes tipoDiabetes, MomentoMedicion momento, double promedio) {
        this.tipoDiabetes = tipoDiabetes;
        this.momento = momento;
        this.promedio = promedio;
    }

    public TipoDiabetes getTipoDiabetes() {
        return tipoDiabetes;
    }

    public void setTipoDiabetes(TipoDiabetes tipoDiabetes) {
        this.tipoDiabetes = tipoDiabetes;
    }

    public MomentoMedicion getMomento() {
        return momento;
    }

    public void setMomento(MomentoMedicion momento) {
        this.momento = momento;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }
}
