package com.glucome.wrapper;

import com.glucome.enumerador.MomentoMedicion;
import com.glucome.enumerador.TipoMedicion;

public class ReporteMedicoPorTipoMedicionWrapper {

    private TipoMedicion tipoMedicion;
    private MomentoMedicion momento;
    private double promedio;

    public ReporteMedicoPorTipoMedicionWrapper(TipoMedicion tipoMedicion, MomentoMedicion momento, double promedio) {
        this.tipoMedicion = tipoMedicion;
        this.momento = momento;
        this.promedio = promedio;
    }

    public TipoMedicion getTipoMedicion() {
        return tipoMedicion;
    }

    public void setTipoMedicion(TipoMedicion tipoMedicion) {
        this.tipoMedicion = tipoMedicion;
    }

    public MomentoMedicion getMomento() {
        return momento;
    }

    public void setMomento(MomentoMedicion momento) {
        this.momento = momento;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }
}
