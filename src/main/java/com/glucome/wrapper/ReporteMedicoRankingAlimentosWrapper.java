package com.glucome.wrapper;

import com.glucome.enumerador.MomentoAlimento;

public class ReporteMedicoRankingAlimentosWrapper {

    private String nombre;
    private String descripcion;
    private String imagen;
    private MomentoAlimento momento;
    private long vecesIngerido;

    public ReporteMedicoRankingAlimentosWrapper(String nombre, String descripcion, String imagen, MomentoAlimento momento, long vecesIngerido) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.momento = momento;
        this.vecesIngerido = vecesIngerido;
    }

    public String getNombre() { return nombre; }

    public void setNombre(String nombre) { this.nombre = nombre; }

    public String getDescripcion() {  return descripcion; }

    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }

    public String getImagen() { return imagen; }

    public void setImagen(String imagen) {  this.imagen = imagen; }

    public MomentoAlimento getMomento() { return momento; }

    public void setMomento(MomentoAlimento momento) { this.momento = momento; }

    public long getVecesIngerido() { return vecesIngerido; }

    public void setVecesIngerido(long vecesIngerido) { this.vecesIngerido = vecesIngerido; }
}
