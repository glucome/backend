package com.glucome.wrapper;

public class ReportePorMomentoWrapper {

    private int sexo;
    private int momento;
    private double promedio;

    public ReportePorMomentoWrapper(int sexo, int momento, double promedio) {
        this.sexo = sexo;
        this.momento = momento;
        this.promedio = promedio;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public int getMomento() { return momento; }

    public void setMomento(int momento) {
        this.momento = momento;
    }

    public double getPromedio() { return promedio; }

    public void setPromedio(double promedio) { this.promedio = promedio; }
}
