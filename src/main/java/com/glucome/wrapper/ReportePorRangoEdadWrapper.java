package com.glucome.wrapper;

public class ReportePorRangoEdadWrapper {

    private int sexo;
    private int momento;
    private String rangoEdad;
    private double promedio;

    public ReportePorRangoEdadWrapper(int sexo, int momento, String rangoEdad, double promedio) {
        this.sexo = sexo;
        this.momento = momento;
        this.rangoEdad = rangoEdad;
        this.promedio = promedio;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public int getMomento() {
        return momento;
    }

    public void setMomento(int momento) {
        this.momento = momento;
    }

    public String getRangoEdad() {
        return rangoEdad;
    }

    public void setRangoEdad(String rangoEdad) {
        this.rangoEdad = rangoEdad;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }
}
