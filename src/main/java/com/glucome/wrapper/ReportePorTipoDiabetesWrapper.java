package com.glucome.wrapper;

public class ReportePorTipoDiabetesWrapper {

    private int sexo;
    private int tipoDiabetes;
    private int momento;
    private double promedio;

    public ReportePorTipoDiabetesWrapper(int sexo, int tipoDiabetes, int momento, double promedio) {
        this.sexo = sexo;
        this.tipoDiabetes = tipoDiabetes;
        this.momento = momento;
        this.promedio = promedio;
    }

    public int getSexo() { return sexo; }

    public void setSexo(int sexo) { this.sexo = sexo; }

    public int getTipoDiabetes() {
        return tipoDiabetes;
    }

    public void setTipoDiabetes(int tipoDiabetes) {
        this.tipoDiabetes = tipoDiabetes;
    }

    public int getMomento() {
        return momento;
    }

    public void setMomento(int momento) {
        this.momento = momento;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }
}
