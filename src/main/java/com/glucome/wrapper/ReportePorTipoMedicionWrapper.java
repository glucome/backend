package com.glucome.wrapper;

public class ReportePorTipoMedicionWrapper {

    private int sexo;
    private int tipoMedicion;
    private int momento;
    private double promedio;

    public ReportePorTipoMedicionWrapper(int sexo, int tipoMedicion, int momento, double promedio) {
        this.sexo = sexo;
        this.tipoMedicion = tipoMedicion;
        this.momento = momento;
        this.promedio = promedio;
    }

    public int getSexo() { return sexo; }

    public void setSexo(int sexo) { this.sexo = sexo; }

    public int getTipoMedicion() {
        return tipoMedicion;
    }

    public void setTipoMedicion(int tipoMedicion) {
        this.tipoMedicion = tipoMedicion;
    }

    public int getMomento() {
        return momento;
    }

    public void setMomento(int momento) {
        this.momento = momento;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }
}
