package com.glucome.wrapper;

public class ReporteRankingAlimentosWrapper {

    private long id;
    private String nombre;
    private String descripcion;
    private int momento;
    private long vecesIngerido;

    public ReporteRankingAlimentosWrapper(long id, String nombre, String descripcion, int momento, long vecesIngerido) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.momento = momento;
        this.vecesIngerido = vecesIngerido;
    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getNombre() { return nombre; }

    public void setNombre(String nombre) { this.nombre = nombre; }

    public String getDescripcion() {  return descripcion; }

    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }

    public int getMomento() { return momento; }

    public void setMomento(int momento) { this.momento = momento; }

    public long getVecesIngerido() { return vecesIngerido; }

    public void setVecesIngerido(long vecesIngerido) { this.vecesIngerido = vecesIngerido; }
}
