package com.glucome.wrapper;

import com.glucome.enumerador.EstadoUsuario;

public class UsuarioChatWrapper {

    private Long idUsuario;
    private Long idChat;
    private String nombre;
    private String avatar;
    private EstadoUsuario estado;
    private Long cantidadMensajesNoLeidos;

    public UsuarioChatWrapper(Long idUsuario, Long idChat, String nombre, String avatar, EstadoUsuario estado, Long cantidadMensajesNoLeidos) {
        this.idUsuario = idUsuario;
        this.idChat = idChat;
        this.nombre = nombre;
        this.avatar = avatar;
        this.estado = estado;
        this.cantidadMensajesNoLeidos = cantidadMensajesNoLeidos;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdChat() {
        return idChat;
    }

    public void setIdChat(Long idChat) {
        this.idChat = idChat;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public EstadoUsuario getEstado() {
        return estado;
    }

    public void setEstado(EstadoUsuario estado) {
        this.estado = estado;
    }

    public Long getCantidadMensajesNoLeidos() { return cantidadMensajesNoLeidos; }

    public void setCantidadMensajesNoLeidos(Long cantidadMensajesNoLeidos) { this.cantidadMensajesNoLeidos = cantidadMensajesNoLeidos; }
}
