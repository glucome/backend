<html>

<body leftmargin='0' rightmargin='0' topmargin='0' bottommargin='0' style='font-family: Arial; font-size: 14.0pt; '>
    <div style="margin:10px"> 
          <table style='font-size:8.0pt; font-family:Arial;  border: 0.5px thick #000000; border-collapse: collapse; width: 1500pt;'>
            <tr>
              <td colspan='2' style='text-align:left; padding: .1pt .1pt .1pt .1pt;'>
                 <img src="$rutaImagen$"  height="200" >
              </td>
              <td colspan='4' style='text-align:right; padding: .1pt .1pt .1pt .1pt;'>
                  <p style='color: black;line-height: 12.0pt' >
                  	<b><span style='font-size:16.0pt;font-family:Arial;color:6397B6;' >$paciente.nombre$</span></b>
                  	<br>
                  	<span style='font-size:12.0pt;font-family:Arial;' ><b>Fecha de Nacimiento:</b> $paciente.fechaNacimientoFormateada$</span>
                  	<br>
                  	<span style='font-size:12.0pt;font-family:Arial;' ><b>Tipo de diabetes:</b> $paciente.tipoDiabetes.nombre$</span>
                  	<br>
                  	<span style='font-size:12.0pt;font-family:Arial;' ><b>Peso:</b> $paciente.peso$</span>
                  	<br>
                  	<span style='font-size:12.0pt;font-family:Arial;' ><b>Altura:</b> $paciente.altura$</span>
                  	<br>
	               </p>	
              </td>
            </tr>
            <tr>
              <td colspan='6' style='text-align:left; background-color:6397B6; margin:0px'>
                  <br/>
              </td>
            </tr>
        </table>
      <div>
          </div>
          <div style="margin:10px">
            <table border="1" style='font-size:8.0pt; font-family:Arial;'>
            <tr>
              <th align="center" style='font-size:12.0pt;font-family:Arial; width:10%;'>Fecha</th>
              <th align="center" style='font-size:12.0pt;font-family:Arial; width:90%;'>Descripción</th>
            </tr>
            $entradas: {entrada|
              <tr>
                  <td align="center" style='width:10%;'>$entrada.fechaFormateada$</td>
                  <td align="justify" style='width:90%; text-align:justify'>$entrada.descripcion$</td>
              </tr>
            }$
            
             		
        </table>
      <div>

          </div>
      <div><br/></div>
      </div>
</div>
</body>
</html>	