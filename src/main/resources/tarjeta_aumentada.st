<html>

<body leftmargin='0' rightmargin='0'  topmargin='0' bottommargin='0' style='font-family: Arial; font-size: 14.0pt; width: 90mm; height=50mm '>
    <div style="border:10px solid black;text-align:left;width:90mm; margin:10px"> 
          <table style='font-size:8.0pt; font-family:Arial;  border: 1px solid black;'>
            <tr>
              <td colspan='2' align='left' style='width:25mm; text-align:left; padding: .1pt .1pt .1pt .1pt;'>
                <img src="$rutaImagenPaciente$" align='top' style="border-radius: 50%;  display: inline-block;"  height="70" >
             </td>
              <td colspan='4' style='width:55mm; text-align:right; padding: .1pt .1pt .1pt .1pt;'>
                  <p style='color: black;line-height: 7.5pt' >
                  	<b><span style='font-size:12.0pt;font-family:Arial;color:6397B6;' >$paciente.nombre$</span></b>
                  	<br>
                  	<span style='font-size:7.5pt;font-family:Arial;' ><b>Fecha de Nacimiento:</b> $paciente.fechaNacimientoFormateada$</span>
                  	<br>
                  	<span style='font-size:7.5pt;font-family:Arial;' ><b>Tipo de diabetes:</b> $paciente.tipoDiabetes.nombre$</span>
                  	<br>
                  	<span style='font-size:7.5pt;font-family:Arial;' ><b>Peso:</b> $paciente.peso$</span>
                  	<br>
                  	<span style='font-size:7.5pt;font-family:Arial;' ><b>Altura:</b> $paciente.altura$</span>
                  	<br>
	               </p>	
              </td>
            </tr>
            <tr style="height:1px" >
            	  <td colspan='6' style='height:2mm;text-align:left;background-color:6397B6; margin:0px'>
              </td>
             </tr>
            <tr>
              <td colspan='2' style='width:20mm; text-align:left; padding: .1pt .1pt .1pt .1pt;' >
                    <img src="$bytesQR$" align='left'   height="100" >
              </td> 
 			  <td colspan='3' style="width:23mm; padding: .1pt .1pt .1pt .1pt" >
                   <p style='color: black;line-height: 7.5pt' >
                   	<span style='font-size:7.5pt;font-family:Arial;' >Si sos médico lee este QR con la Aplicación Gluko para casos de emergencia</span>
                   </p>
              </td> 
               <td colspan='1' style='width:7mm; text-align:right; padding: .1pt .1pt .1pt .1pt;'>
              	<div style="">
                	 <img  stlye="border-radius: 50%;  display: inline-block;" src="$rutaImagen$"  height="60" >
                 </div>
              </td> 
            </tr>
        </table>
	</div>
</body>
</html>	