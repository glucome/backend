package com.glucome;

import com.google.gson.JsonObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GlucomeApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InicializarTest {

    private static final String URL = "/api/v1/inicializar/";

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port = 8080;

    @Test
    public void inicializar() {
        Map<String, String> parametros = new HashMap<>();
        JsonObject jsonObject = UtilesTest.get(restTemplate, port, URL, parametros, null);
        assertTrue(jsonObject.getAsString().equals("Fin de inicialización"));
    }


}
