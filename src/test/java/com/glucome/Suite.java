package com.glucome;

import com.glucome.identificacion.LoginTest;
import com.glucome.identificacion.RegistroTest;
import com.glucome.paciente.MedicionTest;
import com.glucome.recomendador.RecomendadorTest;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class Suite {

    @Test
    public void run() {
        JUnitCore.runClasses(InicializarTest.class, LoginTest.class, RegistroTest.class, MedicionTest.class, RecomendadorTest.class);
    }

}