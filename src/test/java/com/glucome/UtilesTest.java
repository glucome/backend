package com.glucome;

import com.glucome.utils.UtilesString;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.Iterator;
import java.util.Map;

public class UtilesTest {

    private static String getRootUrl() {
        return "http://localhost:";
    }

    private static final String APPLICATION_JSON = "application/json";

    private static final String CONTENT_TYPE = "Content-Type";

    public static JsonObject navegar(TestRestTemplate restTemplate, int port, String url, Map<String, String> parametros, String token, HttpMethod metodo) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_TYPE, APPLICATION_JSON);

        if (!UtilesString.isNullOrEmpty(token)) {
            headers.add("Authorization", "Bearer " + token);
        }

        JsonObject request = new JsonObject();
        Iterator<String> iterator = parametros.keySet().iterator();
        while (iterator.hasNext()) {
            String clave = iterator.next();
            request.addProperty(clave, parametros.get(clave));
        }
        HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + port + url, metodo, entity, String.class);
        JsonParser jsonParser = new JsonParser();
        return (JsonObject) jsonParser.parse(response.getBody());
    }

    public static JsonObject post(TestRestTemplate restTemplate, int port, String url, Map<String, String> parametros, String token) {
        return navegar(restTemplate, port, url, parametros, token, HttpMethod.POST);
    }

    public static JsonObject get(TestRestTemplate restTemplate, int port, String url, Map<String, String> parametros, String token) {
        return navegar(restTemplate, port, url, parametros, token, HttpMethod.GET);
    }

}
