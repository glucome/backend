package com.glucome.identificacion;

import com.glucome.GlucomeApplication;
import com.glucome.UtilesTest;
import com.google.gson.JsonObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GlucomeApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LoginTest {

    private static final String RESPUESTA = "respuesta";

    private static final String CADENA_VACIA = "";

    private static final String CLAVE_INVALIDA = "noEsElPassword";

    public static final String CLAVE_VALIDA = "admin";

    public static final String USUARIO_VALIDO = "admin";

    public static final String USUARIO = "usuario";

    public static final String PASSWORD = "password";

    public static final String URL = "/api/v1/auth/signin/";

    public static final String TOKEN = "token";

    private static final String MAIL_VALIDO = "admin@glucome.com.ar";

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port = 8080;

    @Test
    public void loginCorrecto() {
        Map<String, String> parametros = new HashMap<>();
        parametros.put(PASSWORD, CLAVE_VALIDA);
        parametros.put(USUARIO, USUARIO_VALIDO);
        JsonObject jsonObject = UtilesTest.post(restTemplate, port, URL, parametros, null);
        assertFalse(jsonObject.get(RESPUESTA).getAsJsonObject().get(TOKEN).isJsonNull());
    }

    @Test
    public void loginCorrectoConMail() {
        Map<String, String> parametros = new HashMap<>();
        parametros.put(PASSWORD, CLAVE_VALIDA);
        parametros.put(USUARIO, MAIL_VALIDO);
        JsonObject jsonObject = UtilesTest.post(restTemplate, port, URL, parametros, null);
        assertFalse(jsonObject.get(RESPUESTA).getAsJsonObject().get(TOKEN).isJsonNull());
    }


    @Test
    public void loginIncorrectoConUsuarioVacio() {
        Map<String, String> parametros = new HashMap<>();
        parametros.put(PASSWORD, CADENA_VACIA);
        parametros.put(USUARIO, CADENA_VACIA);
        JsonObject jsonObject = UtilesTest.post(restTemplate, port, URL, parametros, null);
        assertTrue(jsonObject.get(RESPUESTA) == null || jsonObject.get(RESPUESTA).isJsonNull());
    }


    @Test
    public void loginIncorrectoSinParametros() {
        Map<String, String> parametros = new HashMap<>();
        JsonObject jsonObject = UtilesTest.post(restTemplate, port, URL, parametros, null);
        assertTrue(jsonObject.get(RESPUESTA) == null || jsonObject.get(RESPUESTA).isJsonNull());
    }

    @Test
    public void loginIncorrecto() {
        Map<String, String> parametros = new HashMap<>();
        parametros.put(PASSWORD, CLAVE_INVALIDA);
        parametros.put(USUARIO, USUARIO_VALIDO);
        JsonObject jsonObject = UtilesTest.post(restTemplate, port, URL, parametros, null);
        assertTrue(jsonObject.get(RESPUESTA) == null || jsonObject.get(RESPUESTA).isJsonNull());
    }
}
