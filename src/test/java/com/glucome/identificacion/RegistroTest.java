package com.glucome.identificacion;

import com.glucome.GlucomeApplication;
import com.glucome.UtilesTest;
import com.google.gson.JsonObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GlucomeApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RegistroTest {


    private static final String MENSAJE_SEXO = "sexo";

    private static final String MENSAJE_NOMBRE = "nombre";

    private static final String MENSAJE_NOMBRE_DE_USUARIO = "nombre de usuario";

    private static final String MESSAGE = "mensaje";

    private static final String URL = "/api/v1/paciente/";

    private static final String NUEVO_TIPO_DIABETES = "1";

    private static final String NUEVO_SEXO = "0";

    private static final String NUEVO_PESO = "99.53";

    private static final String NUEVA_FECHA_NACIMIENTO = "12/02/1977";

    private static final String NUEVA_ALTURA = "1.98";

    private static final String NUEVO_NOMBRE = "Nuevo";

    private static final String NUEVA_PASSWORD = "1234";

    private static final String NUEVO_MAIL = "nuevo@glucome.com";

    private static final String NUEVO_USUARIO = "nuevoUsuario";

    private static final String TIPO_DIABETES = "tipoDiabetes";

    private static final String SEXO = MENSAJE_SEXO;

    private static final String PESO = "peso";

    private static final String FECHA_NACIMIENTO = "fechaNacimiento";

    private static final String ALTURA = "altura";

    private static final String NOMBRE = RegistroTest.MENSAJE_NOMBRE;

    private static final String PASSWORD = "password";

    private static final String MAIL = "mail";

    private static final String NOMBRE_USUARIO = "nombreUsuario";

    private static final String TOKEN = "token";

    private static final CharSequence MENSAJE_MAIL = "mail";

    private static final String RESPUESTA = "respuesta";

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port = 8080;

    @Test
    public void registroCorrecto() {

        Map<String, String> parametros = new HashMap<>();
        parametros.put(NOMBRE_USUARIO, NUEVO_USUARIO);
        parametros.put(MAIL, NUEVO_MAIL);
        parametros.put(PASSWORD, NUEVA_PASSWORD);
        parametros.put(NOMBRE, NUEVO_NOMBRE);
        parametros.put(ALTURA, NUEVA_ALTURA);
        parametros.put(FECHA_NACIMIENTO, NUEVA_FECHA_NACIMIENTO);
        parametros.put(PESO, NUEVO_PESO);
        parametros.put(SEXO, NUEVO_SEXO);
        parametros.put(TIPO_DIABETES, NUEVO_TIPO_DIABETES);

        JsonObject jsonObject = UtilesTest.post(restTemplate, port, URL, parametros, null);
        assertFalse(jsonObject.get(RESPUESTA).getAsJsonObject().get(TOKEN).isJsonNull());
    }


    @Test
    public void registroNombreDeUsuarioFaltante() {

        Map<String, String> parametros = new HashMap<>();
        parametros.put(MAIL, NUEVO_MAIL);
        parametros.put(PASSWORD, NUEVA_PASSWORD);
        parametros.put(NOMBRE, NUEVO_NOMBRE);
        parametros.put(ALTURA, NUEVA_ALTURA);
        parametros.put(FECHA_NACIMIENTO, NUEVA_FECHA_NACIMIENTO);
        parametros.put(PESO, NUEVO_PESO);
        parametros.put(SEXO, NUEVO_SEXO);
        parametros.put(TIPO_DIABETES, NUEVO_TIPO_DIABETES);

        JsonObject jsonObject = UtilesTest.post(restTemplate, port, URL, parametros, null);
        assertTrue(jsonObject.get(RESPUESTA) == null || jsonObject.get(RESPUESTA).isJsonNull());
        assertTrue(jsonObject.get(MESSAGE).getAsString().contains(MENSAJE_NOMBRE_DE_USUARIO));
    }

    @Test
    public void registroFaltaMail() {

        Map<String, String> parametros = new HashMap<>();
        parametros.put(NOMBRE_USUARIO, NUEVO_USUARIO);
        parametros.put(PASSWORD, NUEVA_PASSWORD);
        parametros.put(NOMBRE, NUEVO_NOMBRE);
        parametros.put(ALTURA, NUEVA_ALTURA);
        parametros.put(FECHA_NACIMIENTO, NUEVA_FECHA_NACIMIENTO);
        parametros.put(PESO, NUEVO_PESO);
        parametros.put(SEXO, NUEVO_SEXO);
        parametros.put(TIPO_DIABETES, NUEVO_TIPO_DIABETES);

        JsonObject jsonObject = UtilesTest.post(restTemplate, port, URL, parametros, null);
        assertTrue(jsonObject.get(RESPUESTA) == null || jsonObject.get(RESPUESTA).isJsonNull());
        assertTrue(jsonObject.get(MESSAGE).getAsString().contains(MENSAJE_MAIL));
    }


    @Test
    public void registroFaltaNombre() {

        Map<String, String> parametros = new HashMap<>();
        parametros.put(NOMBRE_USUARIO, NUEVO_USUARIO);
        parametros.put(MAIL, NUEVO_MAIL);
        parametros.put(PASSWORD, NUEVA_PASSWORD);
        parametros.put(ALTURA, NUEVA_ALTURA);
        parametros.put(FECHA_NACIMIENTO, NUEVA_FECHA_NACIMIENTO);
        parametros.put(PESO, NUEVO_PESO);
        parametros.put(SEXO, NUEVO_SEXO);
        parametros.put(TIPO_DIABETES, NUEVO_TIPO_DIABETES);

        JsonObject jsonObject = UtilesTest.post(restTemplate, port, URL, parametros, null);
        assertTrue(jsonObject.get(RESPUESTA) == null || jsonObject.get(RESPUESTA).isJsonNull());
        assertTrue(jsonObject.get(MESSAGE).getAsString().contains(MENSAJE_NOMBRE));
    }

    @Test
    public void registroSinDatos() {

        Map<String, String> parametros = new HashMap<>();
        JsonObject jsonObject = UtilesTest.post(restTemplate, port, URL, parametros, null);
        assertTrue(jsonObject.get(RESPUESTA) == null || jsonObject.get(RESPUESTA).isJsonNull());
    }
}


