package com.glucome.paciente;

import com.glucome.GlucomeApplication;
import com.glucome.UtilesTest;
import com.glucome.identificacion.LoginTest;
import com.google.gson.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GlucomeApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InsulinaTest {

    private static final String TIPO = "tipo";

    private static final String URL_MEDICION = "/insulina/";

    private static final String URL_BASE = "/api/v1/paciente/";

    private static final String FECHA_CORRECTA = "27/02/2019 20:42";

    private static final String UNIDADES_CORRECTAS = "10";

    private static final String UNIDADES = "unidades";

    private static final String TIPO_MEDICION = "1";

    private static final String FECHA_HORA = "fechaHora";

    private static final String ID = "id";

    private static final String FECHA_INCORRECTA = "75/24/3211 42:32";

    private static final String RESPUESTA = "respuesta";

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port = 8080;

    private String token;

    private long idUsuario;

    @Before
    public void iniciarSesion() {
        Map<String, String> parametros = new HashMap<>();
        parametros.put(LoginTest.PASSWORD, LoginTest.CLAVE_VALIDA);
        parametros.put(LoginTest.USUARIO, LoginTest.USUARIO_VALIDO);
        JsonObject jsonObject = UtilesTest.post(restTemplate, port, LoginTest.URL, parametros, null);
        token = jsonObject.get(RESPUESTA).getAsJsonObject().get(LoginTest.TOKEN).getAsString();
        idUsuario = jsonObject.get(RESPUESTA).getAsJsonObject().get(ID).getAsLong();
    }

    @Test
    public void medicionCorrecta() {
        Map<String, String> parametros = new HashMap<>();
        parametros.put(FECHA_HORA, FECHA_CORRECTA);
        parametros.put(UNIDADES, UNIDADES_CORRECTAS);
        parametros.put(TIPO, TIPO_MEDICION);
        JsonObject jsonObject = UtilesTest.post(restTemplate, port, getUrl(), parametros, token);
        assertFalse(jsonObject.get(RESPUESTA).getAsJsonObject().get(ID).isJsonNull());
    }

    @Test
    public void medicionCorrectaSinToken() {
        Map<String, String> parametros = new HashMap<>();
        parametros.put(FECHA_HORA, FECHA_CORRECTA);
        parametros.put(UNIDADES, UNIDADES_CORRECTAS);
        parametros.put(TIPO, TIPO_MEDICION);
        JsonObject jsonObject = UtilesTest.post(restTemplate, port, getUrl(), parametros, null);
        assertTrue(jsonObject.get(RESPUESTA) == null || jsonObject.get(RESPUESTA).isJsonNull());
    }

    @Test
    public void medicionFechaIncorrecta() {
        Map<String, String> parametros = new HashMap<>();
        parametros.put(FECHA_HORA, FECHA_INCORRECTA);
        parametros.put(UNIDADES, UNIDADES_CORRECTAS);
        parametros.put(TIPO, TIPO_MEDICION);
        JsonObject jsonObject = UtilesTest.post(restTemplate, port, getUrl(), parametros, token);
        assertTrue(jsonObject.get(RESPUESTA) == null || jsonObject.get(RESPUESTA).isJsonNull());
    }

    private String getUrl() {
        return URL_BASE + (idUsuario) + URL_MEDICION;
    }

}