package com.glucome.recomendador;

import com.glucome.GlucomeApplication;
import com.glucome.UtilesTest;
import com.glucome.identificacion.LoginTest;
import com.google.gson.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GlucomeApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RecomendadorTest {

    private static final String URL_BASE = "/api/v1/recomendador/";

    private String token;

    private long idUsuario;

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port = 8080;

    private static final String ID = "id";

    private static final String RESPUESTA = "respuesta";

    @Before
    public void iniciarSesion() {
        Map<String, String> parametros = new HashMap<>();
        parametros.put(LoginTest.PASSWORD, LoginTest.CLAVE_VALIDA);
        parametros.put(LoginTest.USUARIO, LoginTest.USUARIO_VALIDO);
        JsonObject jsonObject = UtilesTest.post(restTemplate, port, LoginTest.URL, parametros, null);
        token = jsonObject.get(RESPUESTA).getAsJsonObject().get(LoginTest.TOKEN).getAsString();
        idUsuario = jsonObject.get(RESPUESTA).getAsJsonObject().get(ID).getAsLong();
    }


    @Test
    public void recomendacionCorrecta() {
        Map<String, String> parametros = new HashMap<>();
        JsonObject jsonObject = UtilesTest.get(restTemplate, port, getUrl(), parametros, token);
        assertFalse(jsonObject.get(RESPUESTA).getAsJsonObject().get("alimentosRecomendados").isJsonNull());
    }


    @Test
    public void usuarioIncorrecto() {
        Map<String, String> parametros = new HashMap<>();
        JsonObject jsonObject = UtilesTest.get(restTemplate, port, URL_BASE + (idUsuario * 4) + "/", parametros, token);
        assertTrue(jsonObject.get(RESPUESTA) == null || jsonObject.get(RESPUESTA).isJsonNull());
    }


    private String getUrl() {
        return URL_BASE + idUsuario + "/";
    }


}
